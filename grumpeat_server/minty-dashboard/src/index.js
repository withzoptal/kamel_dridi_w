import React, { Component }from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Switch, Redirect} from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import registerServiceWorker from './registerServiceWorker';
import Pages from './containers/Pages.jsx';
import Dash from './containers/Dash.jsx';
import {AppContext} from './containers/AppContext';


import './assets/css/bootstrap.min.css';
import './assets/sass/light-bootstrap-dashboard.css';
import './assets/css/demo.css';
import './assets/css/pe-icon-7-stroke.css';
import 'react-select/dist/react-select.css';

//TODO: extract PrivateRoute on a separate component
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>{
      return isAuthenticated() ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
              pathname: "/login",
              state: { from: props.location }
          }}
        />
      )
      }
    }
  />
);

function isAuthenticated(){
  const jwt = sessionStorage.getItem('grumpeat_jwt');
  if (jwt) {
    return true;
  }
  return false;
};

class MintyDashboard extends Component{

  constructor(props){
    super(props);

    this.handleError = (error) => {
      console.log('error****************', error)
      let errorMsg = '';
      let errorStatus = '';
      if (error.response) {
        errorStatus = error && error.response && error.response.status;
        errorMsg = error && error.response && error.response.data && error.response.data.error && error.response.data.error.message;
      } else {
        errorStatus = 'Error';
        errorMsg = 'The app encountred an error with the server';
      }

      this.setState({
        alert: (
          <SweetAlert
            danger
            title={errorStatus}
            confirmBtnBsStyle="danger"
            style={{display: "block",marginTop: "-100px"}}
            onConfirm={
              () => {
                this.setState({alert: null});
                this.setState({isLoading: false});
              }
            }
          >
            {errorMsg}
          </SweetAlert>
        )
      });
    }

    this.handleWaiting = () => {
      this.setState({
        alert: (
          <SweetAlert
            title="Waiting"
            style={{display: "block",marginTop: "-100px"}}
            showConfirm={false}
            onConfirm={prop => {}}
          >
            Waiting for the operation to end
          </SweetAlert>
        )
      });
    }

    this.setAlert = (param=null) => {
      this.setState({alert: param});
    }

    this.handleCorrectResponse = () => {
      this.setState({})
      this.setState({
        alert: (
          <SweetAlert
            success
            title="Success"
            confirmBtnBsStyle="info"
            style={{display: "block",marginTop: "-100px"}}
            onConfirm={
              () => {
                this.setState({alert: null});
              }
            }
          >
            Operation executed successfully
          </SweetAlert>
        )
      });
    }

    this.handleLogin = (param) => {
      const role = param.user && param.user.role || param.userId;
      sessionStorage.setItem('grumpeat_jwt',param.id);
      sessionStorage.setItem('grumpeat_role',role);
      this.setState({token: param.id, role});
    }

    this.handleLogout = () => {
      this.setState({token: ''});
      sessionStorage.removeItem('grumpeat_jwt');
    }

    this.state = {
      appName: 'Grumpeat',
      alert: null,
      baseUrl: 'http://3.0.175.106/api', 
      container: "grumpeat",
      googlePlaceKey: "AIzaSyCLtOx4tx1-E0kyi5XqVEVDLJKmNwKS3sI",
      token: sessionStorage.getItem('grumpeat_jwt') || '',
      role: sessionStorage.getItem('grumpeat_role') || '',
      handleError: this.handleError,
      handleWaiting: this.handleWaiting,
      setAlert: this.setAlert,
      handleCorrectResponse: this.handleCorrectResponse,
      handleLogin: this.handleLogin,
      handleLogout: this.handleLogout,
    }
  }


  render(){
    return (
      <AppContext.Provider value={this.state}>
        {this.state.alert}
        <HashRouter>
          <Switch>
          
            <Route name="Login" path="/login" component={Pages} />
            
             <PrivateRoute name="Home" path="/" component={Dash} />
            

          </Switch>
        </HashRouter>
      </AppContext.Provider>
    );
  }
}

//todo: the router systeme is the worst routing syste that I have ever seen, fix this shit and make a new one !
ReactDOM.render(<MintyDashboard/>, document.getElementById('root'));
registerServiceWorker();

