import LoginPage from '../views/Pages/LoginPage.jsx';
import LockScreenPage from '../views/Pages/LockScreenPage.jsx';

var pagesRoutes = [
    { path: "/login", name: "Login Page", mini: "LP", component: LoginPage },
    { path: "/lock-screen", name: "Lock Screen Page", mini: "LSP", component: LockScreenPage }
];

export default pagesRoutes;
