import Dashboard from '../views/Dashboard/Dashboard';
import NewUser from '../views/User/NewUser';
import UsersList from '../views/User/UsersList';
import NewQuality from '../views/Quality/NewQuality';
import QualitiesList from '../views/Quality/QualitiesList';
import ListC from '../views/Category/CategoriesList';
import NewCategory from '../views/Category/NewCategory';
import NewSpecialty from '../views/Speciality/NewSpecialty';
import SpecialtiesList from '../views/Speciality/SpecialtiesList';
import OrdersList from '../views/Order/OrdersList';
import NewRestaurant from '../views/Restaurant/NewRestaurant';
import RestaurantsList from '../views/Restaurant/RestaurantsList';
import ImportRestaurants from '../views/Restaurant/ImportRestaurants';
import NewNotification from '../views/Notification/NewNotification';
import NewPromotion from '../views/Promotion/NewPromotion';
import PromotionsList from '../views/Promotion/PromotionsList';
import NewSetting from '../views/GlobalSettings/newSetting';
import ModerationsList from '../views/Moderation/ModerationsList';

var dashRoutes = [
  {
    path: '/dashboard',
    link: '/dashboard',
    name: 'Dashboard',
    icon: 'pe-7s-graph',
    component: Dashboard
  },

  {
    collapse: true,
    path: '/users',
    name: 'Users',
    state: 'openUser',
    icon: 'pe-7s-users',
    views: [
      {
        path: '/users/new/:id?',
        link: '/users/new/',
        name: 'New User',
        mini: 'NU',
        component: NewUser
      },

      {
        path: '/users/',
        link: '/users/',
        name: 'Users List',
        mini: 'UL',
        component: UsersList
      }
    ]
  },

  {
    collapse: true,
    path: '/restaurant',
    name: 'Restaurants',
    state: 'openRestaurant',
    icon: 'pe-7s-home',
    views: [
      {
        path: '/restaurant/new/:id?',
        link: '/restaurant/new/',
        name: 'New Restaurants',
        mini: 'NR',
        component: NewRestaurant
      },

      {
        path: '/restaurant/import',
        link: '/restaurant/import',
        name: 'Import Restaurants',
        mini: 'IR',
        component: ImportRestaurants
      },

      {
        path: '/restaurant/',
        link: '/restaurant/',
        name: 'Restaurants List',
        mini: 'RL',
        component: RestaurantsList
      }
    ]
  },

  {
    collapse: true,
    path: '/promotion',
    name: 'Promotion',
    state: 'openPromotion',
    icon: 'pe-7s-ribbon',
    views: [
      {
        path: '/promotion/new/:id?',
        link: '/promotion/new/',
        name: 'New Promotion',
        mini: 'NP',
        component: NewPromotion
      },

      {
        path: '/promotion/',
        link: '/promotion/',
        name: 'Promotions List',
        mini: 'PL',
        component: PromotionsList
      }
    ]
  },

  {
    path: '/orders',
    link: '/orders',
    name: 'Orders',
    icon: 'pe-7s-note2',
    component: OrdersList
  },

  {
    path: '/notifications',
    link: '/notifications',
    name: 'Notifications',
    icon: 'pe-7s-paper-plane',
    component: NewNotification
  },

  {
    collapse: true,
    path: '/quality',
    name: 'quality',
    state: 'openQuality',
    icon: 'pe-7s-like2',
    views: [
      {
        path: '/quality/new/:id?',
        link: '/quality/new/',
        name: 'New Quality',
        mini: 'NQ',
        component: NewQuality
      },

      {
        path: '/quality/',
        link: '/quality/',
        name: 'Qualities List',
        mini: 'QL',
        component: QualitiesList
      }
    ]
  },
  {
    collapse: true,
    path: '/category',
    name: 'category',
    state: 'openCategory',
    icon: 'pe-7s-like2',
    views: [
       
      {
        path: '/category/new/:id?',
        link: '/category/new/',
        name: 'New Category',
        mini: 'NC',
        component: NewCategory
      },

      {
        path: '/category/',
        link: '/category/',
        name: 'Categories List',
        mini: 'CL',
        component:  ListC
      }
    ]
  },

  {
    path: '/moderations',
    link: '/moderations',
    name: 'Moderation',
    icon: 'pe-7s-hammer',
    component: ModerationsList
  },

  {
    path: '/settings',
    link: '/settings',
    name: 'Settings',
    icon: 'pe-7s-tools',
    component: NewSetting
  },

  { redirect: true, path: '/', pathTo: '/dashboard', name: 'Dashboard' }
];
export default dashRoutes;
