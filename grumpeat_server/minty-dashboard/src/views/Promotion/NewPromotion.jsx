import React, { Component } from 'react';
import{ Grid, Row, Col, FormGroup, ControlLabel, FormControl, Form, ButtonGroup, Button as Buttong, OverlayTrigger, Tooltip, HelpBlock } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Button from '../../components/CustomButton/CustomButton.jsx';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios';
import { withRouter } from 'react-router-dom'
import {AppContext} from "../../containers/AppContext";
import Datetime from 'react-datetime';
//import moment from 'moment';
import moment from 'moment-timezone';
import TimePicker from 'react-time-picker';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';

var  _ = require ('lodash');


var tzlookup = require("tz-lookup");
export class NewPromotion extends Component{
  constructor(props){
    super(props);

    this.state = {
      isLoading: false,
      alert: null,
      submitMessage: 'Submit',
      requestID: '',
      name: '',
      workingHourDays: ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
      selectedWorkingDays: [],
      lastSelectedDay: "",
      startAt: "",
      endAt: "",
      restaurantsOptions: [],
      restaurantsData: [],
      selectedRestaurant: "",
      email: '',
      test: '',
      dateRange: '',
    };

    this.handleDateInputChange = this.handleDateInputChange.bind(this);
    this.addPromTime = this.addPromTime.bind(this);   
    this.handleWorkingHours = _.debounce(this.handleWorkingHours, 100);
    this.handleWorkingDay = this.handleWorkingDay.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.populateForm = this.populateForm.bind(this);

    this.getDateFromString = this.getDateFromString.bind(this);
    this.getStringFromDate = this.getStringFromDate.bind(this);

    this.handleTimeBlur = this.handleTimeBlur.bind(this);
    this.handleTimeChange = _.debounce(this.handleTimeChange, 100);

    this.handleDateRange = this.handleDateRange.bind(this);

    if(this.props.match.params.id && (this.props.match.params.id !== ':id')){
      this.state.requestID = this.props.match.params.id;
    }
  }

   async componentWillMount(){
    
    //fill restau select options
    this.props.helper.handleWaiting();
    axios.get(`${this.props.helper.baseUrl}/Restaurants`, {
      params: {
        access_token: this.props.helper.token
      }
    }).then(res => {
      if (res.status === 200) {
        let restaurantsOptions = res.data.map(rest => ({label: (
        <span>
          {rest.name}  <span style={{width: "10px",display: "inline-block"}}></span>   {"("+rest.formatted_address+")"}
        </span>) , value: rest.id }));
        this.setState({ restaurantsOptions, restaurantsData: res.data });
        this.props.helper.setAlert(null);
      } else {
        throw (res.status);
      }
    }).catch(error => { this.props.helper.handleError(error); });





        

    if(this.state.requestID){

      axios.get(`${this.props.helper.baseUrl}/Promotions/${this.state.requestID}`, {
        params: {
          filter:{
            "include": "restaurant"
          },
          access_token: this.props.helper.token
        }
      }).then(res => {
        if (res.status === 200) {
          this.populateForm(res.data);
        } else {
          throw (res.status);
        }
      }).catch(error => { this.props.helper.handleError(error); });

    }

    
  } 

  getDateFromString(timeString){
    let [hours, minutes] =  timeString.split(':');
    let d = new Date();
    d.setMinutes(Number(minutes));
    d.setHours(Number(hours));

    return d;
  }

  getStringFromDate(date){
    let d = new Date(date);
    let hours = d.getHours() + "";
    let minutes = d.getMinutes() + "";

    if(hours.length == 1){
      hours = "0" + hours;
    }

    if(minutes.length == 1){
      minutes = "0" + minutes;
    }

    let result =  `${hours}:${minutes}`;
    return result;
  }

  handleDateRange(dates){
    let startAt = "", endAt = "";
    if(!dates){
      [startAt, endAt] = [null, null];
    }else{

      [startAt, endAt] = dates;
    }

    this.setState({startAt, endAt, dateRange: dates});
  }

  handleTimeBlur(name, index){
    let workingDays = this.state.selectedWorkingDays;
    let dayIndex = workingDays.findIndex(obj => obj.day === this.state.lastSelectedDay);
    let oldVal = workingDays[dayIndex].frames[index][name];

    workingDays[dayIndex].frames[index][name] = "";
    this.setState({selectedWorkingDays: workingDays}, () => {
      workingDays[dayIndex].frames[index][name] = oldVal;

      this.setState({selectedWorkingDays: workingDays}, () => {console.log('blur', this.state); console.groupEnd('blur');});
    });
    
    
  }

  handleTimeChange  (name, value)  {

      this.setState({[name]: value});
  }
  

  /*
  Old shit and I am glade that it's over
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  */

 addPromTime(){
     if(this.state.selectedWorkingDays.find(obj => obj.day === this.state.lastSelectedDay).frames.length < 3){
         let workingDays = this.state.selectedWorkingDays;
         this.state.selectedWorkingDays.find(obj => obj.day === this.state.lastSelectedDay).frames.push({"start": "", "end": "", "percentage": ""});
         this.setState({selectedWorkingDays: workingDays});
     }
 }

 handleWorkingDay(e, day, toDelete){
  e.stopPropagation();
    let sel = this.state.selectedWorkingDays; 

    if(sel.find(obj => obj.day === day)){
      //delete sel[day];
    }else {
      sel.push({
                  "day": day, 
                  "frames":[
                    {"start": "", "end": "", "percentage": ""}
                  ]
                });
    }

    if (sel.find(obj => obj.day === day) && toDelete) {
      //delete sel[day];
      sel.splice(sel.findIndex(obj => obj.day === day), 1);
    }

    this.setState({selectedWorkingDays: sel, lastSelectedDay: day});
 }

 handleWorkingHours(name, value, index){
   
   let workingDays = this.state.selectedWorkingDays;
   
     let dayIndex = workingDays.findIndex(obj => obj.day === this.state.lastSelectedDay);
     workingDays[dayIndex].frames[index][name] = value;

   
   this.setState({selectedWorkingDays: workingDays}, () =>console.log(this.state.selectedWorkingDays[dayIndex].frames[index]));
 }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    const baseObjectName =name.split('[')[0].split('.')[0];
    const baseObject = _.get(this.state, baseObjectName);
    const partToModify = name.replace(baseObjectName, '');

    if(partToModify){
      _.set(baseObject, partToModify, value);
      this.setState({[baseObjectName]: baseObject});
    }else{
      this.setState({[baseObjectName]: value});
    }
  }

  async handleSubmit(e){
    e.preventDefault();
    if((this.state.password !== this.state.confirmPassword) && (this.state.password || this.state.confirmPassword) ){
      return;
    }

    

    this.setState({ isLoading: true });
    this.props.helper.handleWaiting();

    let restaurant = this.state.restaurantsData.find(restau => restau.id === this.state.selectedRestaurant);

    let selectedWorkingDays =  this.state.selectedWorkingDays;

    selectedWorkingDays = selectedWorkingDays.map(wday => {
      return {
        /* frames: {
          ...wday.frames,
          "percentage": parseInt(wday.frames.percentage)
        }, */
        frames: wday.frames.map(frame => {
          console.log('frame ', frame);
          return {
            ...frame,

            start: this.getDateFromString(frame.start)  /* moment.tz(frame.start, tzlookup(restaurant.location.lat, restaurant.location.lng) ).to() */,

            end: this.getDateFromString(frame.end) /* moment.tz(frame.end, tzlookup(restaurant.location.lat, restaurant.location.lng) ).format() */,
            
            "percentage": parseInt(frame.percentage)
          }
        }),
        day:  this.state.workingHourDays.findIndex( whd => whd ===  wday.day) 
       };
    })

    
    let request = {
      name: this.state.name,
      start: this.state.startAt,
      end: this.state.endAt,
      restaurantId: this.state.selectedRestaurant,
      times: selectedWorkingDays
    };
    
    debugger;

   

      let requestURL    = "",
          restaurantURL = "";
      if(this.state.requestID){
        requestURL    = `${this.props.helper.baseUrl}/Promotions/${this.state.requestID}?access_token=${this.props.helper.token}`;
        restaurantURL = `${this.props.helper.baseUrl}/Restaurants/${this.state.selectedRestaurant}?access_token=${this.props.helper.token}`;
      }else{
        requestURL = `${this.props.helper.baseUrl}/Promotions?access_token=${this.props.helper.token}`;
        restaurantURL = `${this.props.helper.baseUrl}/Restaurants/${this.state.selectedRestaurant}?access_token=${this.props.helper.token}`;
      }

    try{
      //await (this.state.requestID ? axios.patch(requestURL, request) : axios.post(requestURL, request));
      if(this.state.requestID){
        await axios.patch(requestURL, request);
        await axios.patch(restaurantURL, {"email": this.state.email});
      }else{
          await axios.post(requestURL, request);
          await axios.patch(restaurantURL, {"email": this.state.email});
      }

      this.props.helper.setAlert(null);
      this.props.history.push('/promotion')
    }catch(error){
      this.setState({ isLoading: false });
      this.props.helper.handleError(error);
    }

    
  }

 
  handleDateInputChange(name, prop){
    if(prop){
      try{
        this.setState({ [name]: prop.toDate()});

      } catch (e) {
        let error = {}
        error.response = {};
        error.response.data = {};
        error.response.data.error = {}; 
        error.response.data.error.message = "Veuillez sélectionner une date";
        this.props.helper.handleError(error);
      }
    }else{
        this.setState({ [name]: null});
    }
}



  populateForm(prop){
    prop = prop ? prop : {};
    let submitMessage = prop ? 'update' : 'submit';
    let selectedWorkingDays = prop.times || [];
    selectedWorkingDays = selectedWorkingDays.map(day => {
        let frames = day.frames.map(frame => ({start: this.getStringFromDate(frame.start), end: this.getStringFromDate(frame.end), percentage: frame.percentage}));
        let dayName = this.state.workingHourDays[day.day];
        day.frames = frames;
        day.day = dayName;
        return day;
    });
    //let restaurant = this.state.restaurantsData.find(rest => rest.id === prop.restaurantId);
    this.setState({
      requestID: prop.id || '',
      submitMessage: submitMessage,
      isLoading: false,
      name: prop.name || '',
      startAt: new Date(prop.start) || "",
      endAt: new Date(prop.end) || "",
      dateRange: [new Date(prop.start), new Date(prop.end)],
      selectedRestaurant: prop.restaurantId || '',
      lastSelectedDay: prop.times[prop.times.length - 1] && prop.times[prop.times.length - 1].day || "",
      selectedWorkingDays,
      email:  prop.restaurant && prop.restaurant.email || ""
    });

    
    this.props.helper.setAlert(null);
  }

  

  render(){

    const {startAt ,endAt} = this.state;

    var yesterday = Datetime.moment().subtract(1, 'day');
    var validStartAt = function( current ){
      return current.isAfter( yesterday ) && (endAt && current.isBefore( endAt )  || true);
    };

    var validEndAt = function( current ){
      return current.isAfter( startAt );
    };

    const { isLoading } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="New Promotion"
                content={
                  <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>
                    
                    {/* <FormGroup>
                        <ControlLabel className="col-md-2">
                        test
                      </ControlLabel>
                      <Col md={9}>
                      <TimePicker
                          name="start"
                          value={this.state.test}
                          required={true}
                          onChange={(value) => {this.handleTimeChange('test', value)}}
                          onBlur={(event)=>this.handleTimeBlur('test', event)}
                          calendarIcon={null}
                        />
                      </Col>
                    </FormGroup> */}

                    <FormGroup>

                      <ControlLabel className="col-md-2">
                        Name
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="Baristas"
                          name="name"
                          value={this.state.name}
                          onChange={this.handleInputChange}
                          required={true}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Restaurant
                      </ControlLabel>
                      <Col md={9}>
                      <Select
                          name="restaurant"
                          value={this.state.selectedRestaurant}
                          options={this.state.restaurantsOptions}
                          onChange={(value) =>{this.setState({selectedRestaurant:value && value.value || ''})}}
                          required={true}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Email
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="email"
                          placeholder="name@domain.com"
                          name="email"
                          value={this.state.email}
                          onChange={this.handleInputChange}
                          required={true}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Start / End:
                      </ControlLabel>
                      <Col md={9}>
                        {/* <Datetime
                            utc
                            closeOnSelect
                            timeFormat={false}
                            inputProps={{placeholder:"Select the starting date", required:true}}
                            name="startAt"
                            value={this.state.startAt}
                            onChange={(value) => {this.handleDateInputChange("startAt", value)}}
                            required={true}
                            isValidDate={ validStartAt }
                        /> */}
                        <DateRangePicker
                          onChange={this.handleDateRange}
                          value={this.state.dateRange}
                          required={true}
                        />
                      </Col>
                      {/* <ControlLabel className="col-md-3">
                        End at:
                      </ControlLabel>
                      <Col md={3}>
                        
                        <Datetime
                            utc
                            closeOnSelect
                            timeFormat={false}
                            inputProps={{placeholder:"Select the ending date", required:true}}
                            name="endAt"
                            value={this.state.endAt}
                            onChange={(value) => {this.handleDateInputChange("endAt", value)}}
                            isValidDate={ startAt && validEndAt }
                        />
                        
                      </Col>*/}
                    </FormGroup> 
                    
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Timing: 
                      </ControlLabel>
                      <Col md={9}>
                        <ButtonGroup>
                            {
                                this.state.workingHourDays.map((day, i) => (
                                     <Button bsStyle={/* this.state.selectedWorkingDays[day] */  this.state.selectedWorkingDays.find(obj => obj.day === day) && "primary"}  fill key={i} onClick={(e)=>this.handleWorkingDay(e, day)} style={this.state.selectedWorkingDays.find(obj => obj.day === day) && { "paddingRight": "0" }}>
                                      <span style={{ "marginRight": "5px" }} className="text-capitalize">{day}</span>
                                      {this.state.selectedWorkingDays.find(obj => obj.day === day) &&
                                        <OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip">Delete Day</Tooltip>}>

                                          <i className="fa fa-times text-danger text-right" onClick={(e) => this.handleWorkingDay(e, day, true)}></i>

                                        </OverlayTrigger>
                                      }
                                     </Button>
                                ))
                            }
                           
                        </ButtonGroup>
                      </Col>
                    </FormGroup>
                            
                  { this.state.selectedWorkingDays.find(obj => obj.day === this.state.lastSelectedDay) && this.state.selectedWorkingDays.find(obj => obj.day === this.state.lastSelectedDay).frames.map((s, i) =>{
                  return <React.Fragment key={i}>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Start
                      </ControlLabel>
                      <Col md={2}>
                        {/* <Datetime
                            utc
                            closeOnSelect
                            dateFormat={false}
                            inputProps={{placeholder:"starting time", required:true}}
                            name="start"
                            value={s.start}
                            required={true}
                            onChange={(value) => {this.handleWorkingHours("start", value.toDate(), i)}}
                        /> */}
                        <TimePicker
                          name="start"
                          value={s.start}
                          required={true}
                          onChange={(value) => {this.handleWorkingHours("start", value, i)}}
                          onBlur={()=>this.handleTimeBlur('start', i)}
                          calendarIcon={null}
                          required={true}
                          maxTime={s.end}
                          step={30}
                        />
                        <HelpBlock>Enter time in GMT*</HelpBlock>
                      </Col>
                      <ControlLabel className="col-md-2">
                        End
                      </ControlLabel>
                      <Col md={2}>
                        {/* <Datetime
                            utc
                            closeOnSelect
                            dateFormat={false}
                            inputProps={{placeholder:"end time", required:true}}
                            name="end"
                            value={s.end}
                            required={true}
                            onChange={(value) => {this.handleWorkingHours("end", value.toDate(), i)}}
                        /> */}

                        <TimePicker
                          name="end"
                          value={s.end}
                          required={true}
                          onChange={(value) => {this.handleWorkingHours("end", value, i)}}
                          onBlur={()=>this.handleTimeBlur('end', i)}
                          calendarIcon={null}
                          required={true}
                          minTime={s.start}
                        />

                        <HelpBlock>Enter time in GMT*</HelpBlock>
                      </Col>
                      {i === 0 && 
                      <Col md={1} mdOffset={1}>
                        <Button simple bsStyle="info" onClick={()=>this.addPromTime()}><i className="fa fa-plus"></i></Button>
                    </Col>}
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Offer %
                      </ControlLabel>
                      <Col md={6}>
                        <FormControl
                          type="number"
                          placeholder=""
                          name="percentage"
                          value={s.percentage}
                          onChange={(e)=>this.handleWorkingHours(e.target.name, e.target.value, i)}
                          min={0}
                          max={100}
                          required={true}
                        />
                      </Col>
                    </FormGroup>
                    <br/>  
                    </React.Fragment>
                  })}
                    
                    <FormGroup>
                      <Col md={10} mdOffset={2}>
                        <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                          {isLoading ? 'processing...' : this.state.submitMessage}
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const UpdatedComponent = withRouter(NewPromotion);
export default props => (
  <AppContext.Consumer>
    { Helper => <UpdatedComponent {...props} helper={Helper} /> }
  </AppContext.Consumer>
);
