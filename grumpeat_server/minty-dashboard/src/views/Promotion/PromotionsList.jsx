import React, { Component } from 'react';
import { Grid, Row, Col, Pagination, InputGroup, FormGroup, FormControl } from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import { AppContext } from "../../containers/AppContext";
import _ from "lodash";
import PromotionRow from '../../components/Table/PromotionRow';

class PromotionsList extends Component {
    constructor(prop) {
        super(prop);

        this.state = {
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['Name', 'Restaurant', 'Country', 'Address', "Action"],
            "dataRows": [],
            paginationItems: [],
            searchKeyWord: ''
        };

        this.getItems = this.getItems.bind(this);
        this.getCount = this.getCount.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount() {
        this.getItems();
        this.getCount();
    }


    getCount(prop){

        if(prop){
          // get the count
          axios.get(`${this.props.helper.baseUrl}/Promotions?access_token=${this.props.helper.token}`, {
            params: {
              
              filter:{
                "where": {"and": [
                    {"status": 1},
                    {"or": [
                         {"name": {"like": `${prop}.*`, "options": "i" }},
                    ]}
                ]}
              },
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              const pageCount = Math.ceil(res.data.length/this.state.limit);
              this.setState({count: pageCount});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }else{
          // get the count
          axios.get(`${this.props.helper.baseUrl}/Promotions?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"status": 1}
                    ]}
                },
                //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              const pageCount = Math.ceil(res.data.length/this.state.limit);
              this.setState({count: pageCount});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }
    
      }


    

    getItems(prop){
        this.props.helper.handleWaiting();
        if(prop){
          // for the actual list with name
          axios.get(`${this.props.helper.baseUrl}/Promotions?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"status": 1},
                        {"or": [
                             {"name": {"like": `${prop}.*`, "options": "i" }},
                            
                        ]}
                    ]},
                    limit: this.state.limit,
                    skip: ((prop - 1) * this.state.limit),
                    "include": "restaurant"

                }
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              this.setState({dataRows: res.data});
              this.setState({alert: null});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }else{
          // for the actual list without names
          axios.get(`${this.props.helper.baseUrl}/Promotions?access_token=${this.props.helper.token}`, {
            params: {
              filter: {
                "limit": this.state.limit,
                "skip": 0,
                "where": {"and": [
                    {"status": 1},
                ]},
                "include": "restaurant"
              }
              
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              this.setState({dataRows: res.data});
              this.setState({alert: null});
              this.props.helper.setAlert(null); 
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }
    
      }

    debouncedSearch = _.debounce(()=>{
        this.getItems(this.state.searchKeyWord);
        this.getCount(this.state.searchKeyWord);
        this.setState({active: 1});
      }, 800);

    handleSearch(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        },this.debouncedSearch);
      }

    deleteItem(id) {
        const itemID = this.state.dataRows[id].id;
        axios.patch(`${this.props.helper.baseUrl}/Promotions/${itemID}/?access_token=${this.props.helper.token}`, {
            "status": 0
        }).then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                updatedData.splice(id, 1);
                this.setState({ dataRows: updatedData });
                this.props.helper.setAlert(null);
                this.getItems();
            } else {
                throw (res.status);
            }
        }).catch(error => { this.props.helper.handleError(error); });
    }

    render() {
        return (
            <div className="main-content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title={
                                    <Col md={12}>
                                      <FormGroup>
                                        <InputGroup>
                                          <FormControl
                                            type="text"
                                            placeholder="Search for promotion by name"
                                            name="searchKeyWord"
                                            value={this.state.searchKeyWord}
                                            onChange={this.handleSearch}
                                          />
                                          <InputGroup.Addon>
                                            <i className="fa fa-search"></i>
                                          </InputGroup.Addon>
                                        </InputGroup>
                                      </FormGroup>
                                    </Col>
                                  }
                                content={
                                    <div className="fresh-datatables">
                                        <table id="datatables" ref="main" className="table table-striped table-no-bordered table-hover"
                                            cellSpacing="0" width="100%" style={{ width: "100%" }}>
                                            <thead>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                {
                                                    this.state.dataRows.map((prop, key) => {
                                                        return (
                                                            <PromotionRow item={prop} key={key} itemID={key} deleteItem={this.deleteItem} />
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

export default props => (
    <AppContext.Consumer>
        {Helper => <PromotionsList {...props} helper={Helper} />}
    </AppContext.Consumer>
);
