import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Form, ButtonGroup, Button as Buttong, Image, boot } from 'react-bootstrap';
import 'react-select/dist/react-select.css';
import Button from '../../components/CustomButton/CustomButton.jsx';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { AppContext } from "../../containers/AppContext";
var _ = require('lodash');


export class NewSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            alert: null,
            submitMessage: 'Submit',
            requestID: '',
            settingsData: [],
            lambda: '',
            maxDistance: '',
            nbMax: '',
            suggestionNb: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.populateForm = this.populateForm.bind(this);

        if (this.props.match.params.id && (this.props.match.params.id !== ':id')) {
            this.state.requestID = this.props.match.params.id;
        }
        console.log('props', this.props);
    }

    async componentWillMount() {
    
        axios.get(`${this.props.helper.baseUrl}/GlobalSettings/${this.state.requestID}`, {
            params: {
                access_token: this.props.helper.token
            }
        }).then(result => {
            if (result.status == 200) {
                this.populateForm(result.data);
                this.setState({settingsData : result.data});
            } else {
                throw (result.status);
            }
        }).catch(error => {
            this.props.helper.handleError(error);
        });
    
    }



    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }


    async handleSubmit(e) {

        e.preventDefault();
        this.setState({ isLoading: true });

        let request = {
            "lambda": this.state.lambda,
            "maxDistance": this.state.maxDistance,
            "nbMax": this.state.nbMax,
            "suggestionNb": this.state.suggestionNb
        };
        
        
        let lambda = this.state.settingsData.find(obj => obj.attribut === 'lambda');
        let maxDistance = this.state.settingsData.find(obj => obj.attribut === 'maxDistance');
        let nbMax = this.state.settingsData.find(obj => obj.attribut === 'nbMax');
        let suggestionNb = this.state.settingsData.find(obj => obj.attribut === 'suggestionNb');


        const requestURLLambda =
            `${this.props.helper.baseUrl}/GlobalSettings/${lambda.id}?access_token=${this.props.helper.token}`;

            const requestURLMaxDistance =
            `${this.props.helper.baseUrl}/GlobalSettings/${maxDistance.id}?access_token=${this.props.helper.token}`;

            const requestURLNbMax =
            `${this.props.helper.baseUrl}/GlobalSettings/${nbMax.id}?access_token=${this.props.helper.token}`;

            const requestURLsuggestionNb =
            `${this.props.helper.baseUrl}/GlobalSettings/${suggestionNb.id}?access_token=${this.props.helper.token}`;

        try {
            await ( axios.patch(requestURLLambda, {"valeur": String(this.state.lambda)}) );
            await ( axios.patch(requestURLMaxDistance, {"valeur": String(this.state.maxDistance)}) );
            await ( axios.patch(requestURLNbMax, {"valeur": String(this.state.nbMax)}) );
            await ( axios.patch(requestURLsuggestionNb, {"valeur": String(this.state.suggestionNb)}) );
            this.props.helper.handleCorrectResponse();
            this.setState({ isLoading: false });
        } catch (error) {
            this.setState({ isLoading: false });
            this.props.helper.handleError(error);
        }
    }

   

    populateForm(prop) {
        prop = prop ? prop : {};
        console.log('populatefoem', prop)
        let submitMessage = prop ? 'update' : 'submit';
        let lambda = prop.find(obj => obj.attribut === 'lambda');
        let maxDistance = prop.find(obj => obj.attribut === 'maxDistance');
        let nbMax = prop.find(obj => obj.attribut === 'nbMax');
        let suggestionNb = prop.find(obj => obj.attribut === 'suggestionNb');

        this.setState({
            requestID: prop.id || '',
            submitMessage: submitMessage,
            isLoading: false,
            lambda: lambda && lambda.valeur || '',
            maxDistance: maxDistance && maxDistance.valeur || '',
            nbMax: nbMax && nbMax.valeur || '',
            suggestionNb: suggestionNb && suggestionNb.valeur || ''
        });

        this.props.helper.setAlert(null);

    }



    render() {
        const { isLoading } = this.state;
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Settings"
                                content={
                                    <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>
                                        <FormGroup>
                                            <ControlLabel className="col-md-2">
                                                lambda
                                            </ControlLabel>
                                            <Col md={9}>
                                                <FormControl
                                                    type="number"
                                                    placeholder="lambda"
                                                    name="lambda"
                                                    value={this.state.lambda}
                                                    onChange={this.handleInputChange}
                                                    required
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <ControlLabel className="col-md-2">
                                                maxDistance
                                            </ControlLabel>
                                            <Col md={9}>
                                                <FormControl
                                                    type="number"
                                                    placeholder="maxDistance"
                                                    name="maxDistance"
                                                    value={this.state.maxDistance}
                                                    onChange={this.handleInputChange}
                                                    required
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <ControlLabel className="col-md-2">
                                                nbMax
                                            </ControlLabel>
                                            <Col md={9}>
                                                <FormControl
                                                    type="number"
                                                    placeholder="nbMax"
                                                    name="nbMax"
                                                    value={this.state.nbMax}
                                                    onChange={this.handleInputChange}
                                                    required
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <ControlLabel className="col-md-2">
                                                suggestionNb
                                            </ControlLabel>
                                            <Col md={9}>
                                                <FormControl
                                                    type="number"
                                                    placeholder="suggestionNb"
                                                    name="suggestionNb"
                                                    value={this.state.suggestionNb}
                                                    onChange={this.handleInputChange}
                                                    required
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col md={10} mdOffset={2}>
                                                <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                                                    {isLoading ? 'processing...' : this.state.submitMessage}
                                                </Button>
                                            </Col>
                                        </FormGroup>
                                    </Form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

const UpdatedComponent = withRouter(NewSetting);
export default props => (
    <AppContext.Consumer>
        {Helper => <UpdatedComponent {...props} helper={Helper} />}
    </AppContext.Consumer>
);