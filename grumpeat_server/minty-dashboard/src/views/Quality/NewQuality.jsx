import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
  Form,
  ButtonGroup,
  Button as Buttong,
  Image,
  boot
} from 'react-bootstrap';
import 'react-select/dist/react-select.css';
import Button from '../../components/CustomButton/CustomButton.jsx';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { AppContext } from '../../containers/AppContext';
// added by Raoudha
import Select from 'react-select';
var _ = require('lodash');

export class NewQuality extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      alert: null,
      submitMessage: 'Submit',
      requestID: '',
      name: '',
      categories: '',
      categoriesOptions: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.populateForm = this.populateForm.bind(this);

    if (this.props.match.params.id && this.props.match.params.id !== ':id') {
      this.state.requestID = this.props.match.params.id;
    }
    console.log('props', this.props);
  }

  async componentWillMount() {
    //get all Categories
    axios
      .get(
        `${this.props.helper.baseUrl}/Categories/?access_token=${this.props.helper.token}`,
        {
          params: {
            // access_token: this.props.helper.token,
            filter: {
              where: { status: 1 }
            }
          }
        }
      )
      .then(res => {
        let categoriesData = [];

        for (let c of res.data) {
          categoriesData.push({ label: c.name, value: c.id });
        }
        this.setState({
          categoriesOptions: categoriesData
        });
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });

    if (this.state.requestID) {
      axios
        .get(`${this.props.helper.baseUrl}/Qualities/${this.state.requestID}`, {
          params: {
            access_token: this.props.helper.token
          }
        })
        .then(result => {
          if (result.status == 200) {
            this.populateForm(result.data);
          } else {
            throw result.status;
          }
        })
        .catch(error => {
          this.props.helper.handleError(error);
        });
    }
  }

  handleInputChange(event, name) {
    //handle Select inputs
    if (name) {
      if (event) {
        const value = event.value;
        this.setState({
          [name]: value
        });
      } //handle clear select
      else {
        this.setState({
          [name]: ''
        });
      }
    }
    //handle all other types of inputs
    else {
      const target = event.target;
      const name = target.name;
      const value = target.type === 'checkbox' ? target.checked : target.value;

      const baseObjectName = name.split('[')[0].split('.')[0];
      const baseObject = _.get(this.state, baseObjectName);
      const partToModify = name.replace(baseObjectName, '');

      if (partToModify) {
        _.set(baseObject, partToModify, value);
        this.setState({ [baseObjectName]: baseObject });
      } else {
        this.setState({ [baseObjectName]: value });
      }
    }
  }

  async handleSubmit(e) {
    e.preventDefault();
    this.setState({ isLoading: true });

    let request = {
      name: this.state.name
    };
    if (this.state.categories !== '') {
      request['categoryId'] = this.state.categories;
      console.log('request :', request);
    }
    /** add rank value */
    // axios
    //   .get(`${this.props.helper.baseUrl}/Qualities/count`)
    //   .then(result => {
    //     if (result.status == 200) {
    //       this.populateForm(result.data);
    //     } else {
    //       throw result.status;
    //     }
    //   })
    //   .catch(error => {
    //     this.props.helper.handleError(error);
    //   });

    const requestURL = this.state.requestID
      ? `${this.props.helper.baseUrl}/Qualities/${this.state.requestID}?access_token=${this.props.helper.token}`
      : `${this.props.helper.baseUrl}/Qualities?access_token=${this.props.helper.token}`;

    try {
      await (this.state.requestID
        ? axios.patch(requestURL, request)
        : axios.post(requestURL, request));
      this.props.helper.setAlert(null);
      this.setState({ isLoading: false });
      this.props.history.push('/quality');
    } catch (error) {
      this.setState({ isLoading: false });
      this.props.helper.handleError(error);
    }
  }

  populateForm(prop) {
    prop = prop ? prop : {};
    console.log('populateform', prop);
    let submitMessage = prop ? 'update' : 'submit';
    this.setState(
      {
        requestID: prop.id || '',
        submitMessage: submitMessage,
        isLoading: false,
        name: prop.name || '',
        categories: prop.categoryId || ''
      },
      () => console.log(this.state.categories)
    );

    this.props.helper.setAlert(null);
  }

  render() {
    const { isLoading } = this.state;
    return (
      <div className='main-content'>
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title='New Quality'
                content={
                  <Form
                    horizontal
                    onSubmit={!isLoading ? this.handleSubmit : null}>
                    <FormGroup>
                      <ControlLabel className='col-md-2'>Name</ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type='text'
                          placeholder='Name'
                          name='name'
                          value={this.state.name}
                          onChange={this.handleInputChange}
                          required
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className='col-md-2'>Category</ControlLabel>
                      <Col md={9}>
                        <Select
                          value={this.state.categories}
                          options={this.state.categoriesOptions}
                          onChange={value =>
                            this.handleInputChange(value, 'categories')
                          }
                          placeholder='Select'
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <Col md={10} mdOffset={2}>
                        <Button
                          bsStyle='info'
                          disabled={isLoading}
                          type='submit'
                          fill>
                          {isLoading
                            ? 'processing...'
                            : this.state.submitMessage}
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const UpdatedComponent = withRouter(NewQuality);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedComponent {...props} helper={Helper} />}
  </AppContext.Consumer>
);
