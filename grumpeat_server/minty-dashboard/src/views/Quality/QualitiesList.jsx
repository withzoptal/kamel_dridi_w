import React, { Component } from 'react';
import {
  Grid,
  Row,
  Col,
  InputGroup,
  Table,
  FormGroup,
  FormControl,
  Pagination,
  Form,
  ControlLabel,
  Button
} from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import { AppContext } from '../../containers/AppContext';
import { withRouter } from 'react-router-dom';
import QualityRow from '../../components/Table/QualityRow';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  console.log('list, startIndex, endIndex', list, startIndex, endIndex);
  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  //padding: grid * 2,
  //margin: `0 0 ${grid}px 0`,
  //display: "table",
  //table-row
  display: isDragging ? 'table' : 'table-row',
  //background: "white",

  // change background colour if dragging
  background: isDragging ? 'whitesmoke' : 'white',

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  //background: isDraggingOver ? "lightblue" : "lightgrey",
  background: 'white'
  //padding: grid,
  //width: 250

  /* height: isDraggingOver ? "100%" : "auto",
    overflowY: isDraggingOver ? "scroll" : "auto", */
});

class QualitiesList extends Component {
  constructor(prop) {
    super(prop);

    this.state = {
      isLoading: false,
      alert: null,
      submitMessage: 'Submit',
      requestID: '',
      limit: 100,
      count: 0,
      active: 1,
      rowTitle: ['#', 'Nom', 'Actions'],
      dataRows: [],
      searchKeyWord: '',
      name: '',
      initDataRows: []
    };

    this.deleteItem = this.deleteItem.bind(this);
    this.getItems = this.getItems.bind(this);
    this.getCount = this.getCount.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);

    if (this.props.match.params.id && this.props.match.params.id !== ':id') {
      this.state.requestID = this.props.match.params.id;
    }
    console.log(this.props);
  }

  componentWillMount() {
    this.getItems();
    this.getCount();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  deleteItem(id) {
    const itemID = this.state.dataRows[id].id;

    axios
      .patch(
        `${this.props.helper.baseUrl}/Qualities/${itemID}?access_token=${this.props.helper.token}`,
        { status: 0 }
      )
      .then(res => {
        if (res.status === 200) {
          let updatedData = this.state.dataRows;
          updatedData.splice(id, 1);
          this.setState({ dataRows: updatedData });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  }

  getCount() {
    axios
      .get(`${this.props.helper.baseUrl}/Qualities/count`, {
        params: {
          where: { status: 1 },
          access_token: this.props.helper.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          const pageCount = Math.ceil(res.data.count / this.state.limit);
          this.setState({ count: pageCount });
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  }

  getItems(prop = 1) {
    this.props.helper.handleWaiting();
    axios
      .get(`${this.props.helper.baseUrl}/Qualities`, {
        params: {
          filter: {
            // "where": {"or": [{"firstName": {"like": `${prop}.*`, "options": "i" }}, {"familytName": {"like": `${prop}.*`, "options": "i" }}]},
            limit: this.state.limit,
            skip: (prop - 1) * this.state.limit,
            where: { status: 1 },
            order: 'rank ASC'
          },
          access_token: this.props.helper.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({ dataRows: res.data });
          this.setState({ initDataRows: res.data });
          this.setState({ active: prop });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  }

  async onDragEnd(result) {
    // dropped outside the list
    if (
      !result.destination ||
      result.destination.index === result.source.index
    ) {
      return;
    }

    console.log('result', result);

    let start = {
      id: this.state.dataRows[result.source.index].id,
      from: result.source.index,
      to: result.destination.index,
      name: this.state.dataRows[result.source.index].name
    };

    let end = {
      id: this.state.dataRows[result.destination.index].id,
      from: result.destination.index,
      to: result.source.index,
      name: this.state.dataRows[result.destination.index].name
    };

    console.log('%cstart end', 'color:green;', start, end);

    const dataRows = reorder(
      this.state.dataRows,
      result.source.index,
      result.destination.index
    );

    this.setState(
      {
        dataRows
      },
      async () => {
        try {
          await axios.patch(
            `${this.props.helper.baseUrl}/Qualities/${start.id}?access_token=${this.props.helper.token}`,
            {
              rank: start.to
            }
          );

          await axios.patch(
            `${this.props.helper.baseUrl}/Qualities/${end.id}?access_token=${this.props.helper.token}`,
            {
              rank: end.to
            }
          );
        } catch (error) {
          this.props.helper.handleError(error);
        }
      }
    );
  }

  render() {
    const { isLoading } = this.state;
    return (
      <div className='main-content'>
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                tableFullWidth
                content={
                  <DragDropContext onDragEnd={this.onDragEnd}>
                    <div /* style={{"height": "auto", "overflowY": "auto"}} */>
                      <Table
                        responsive
                        className='table-bigboy'
                        style={{ tableLayout: 'auto' }}>
                        <thead>
                          <tr>
                            <th>{this.state.rowTitle[0]}</th>
                            <th>{this.state.rowTitle[1]}</th>
                            <th className='text-right'>
                              {this.state.rowTitle[2]}
                            </th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>{this.state.rowTitle[0]}</th>
                            <th>{this.state.rowTitle[1]}</th>
                            <th className='text-right'>
                              {this.state.rowTitle[2]}
                            </th>
                          </tr>
                        </tfoot>

                        <Droppable droppableId='droppable'>
                          {(provided, snapshot) => (
                            <tbody
                              {...provided.droppableProps}
                              ref={provided.innerRef}
                              style={getListStyle(snapshot.isDraggingOver)}>
                              {this.state.dataRows.map((prop, key) => {
                                return (
                                  <Draggable
                                    key={prop.id}
                                    draggableId={prop.id}
                                    index={key}>
                                    {(provided, snapshot) => (
                                      <QualityRow
                                        item={prop}
                                        itemID={key}
                                        ind={key}
                                        key={key}
                                        deleteItem={this.deleteItem}
                                        innerRef={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        style={getItemStyle(
                                          snapshot.isDragging,
                                          provided.draggableProps.style
                                        )}
                                      />
                                    )}
                                  </Draggable>
                                );
                              })}
                              {provided.placeholder}
                            </tbody>
                          )}
                        </Droppable>
                      </Table>
                    </div>
                  </DragDropContext>
                }
                legend={
                  <div>
                    {/* <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                        /> */}
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
const UpdatedComponent = withRouter(QualitiesList);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedComponent {...props} helper={Helper} />}
  </AppContext.Consumer>
);
