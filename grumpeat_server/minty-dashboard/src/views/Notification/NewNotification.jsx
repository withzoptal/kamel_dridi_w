import React, { Component } from 'react';
import {
  Grid, Row, Col,
  FormGroup, ControlLabel, FormControl, Form, HelpBlock
} from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import 'react-datetime/css/react-datetime.css';
import Button from '../../components/CustomButton/CustomButton';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert';
import { withRouter } from "react-router-dom";
import { AppContext } from "../../containers/AppContext";
const baseUrl = 'http://3.0.175.106/api';
const endPoint = `${baseUrl}/Notifications/sendByTag`;
let token;

export class NewNotification extends Component {
  constructor(prop) {
    super(prop);

    token = sessionStorage.getItem('sushi_jwt');
    this.state = {
      alert: null,
      submitMessage: 'Submit',
      searchKeyWord: '',

      msgEn: '',
      msgFr: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);



  }

  async componentWillMount() {

  }

  handleInputChange(event) {
    console.log(event.target);
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }





  handleSubmit(e) {
    e.preventDefault();

    //form validation: english message is required
    if(this.state.msgEn === ''){
      //error.response.status
      //error.response.data.error.message
      let error = { };
      error.response = {
        status: "Error",
        data: {
          error: {
            message: "Message in english is required"
          }
        }
      };
      this.props.helper.handleError(error);
      return false;
    }

    this.props.helper.handleWaiting();
    this.setState({ isLoading: true });

    const request = {
      msgEn: this.state.msgEn,
      ...(this.state.msg !== '' ? {msgFr: this.state.msgFr} : {})
    }

    axios.post(`${this.props.helper.baseUrl}/Notifications/SendNotif?access_token=${this.props.helper.token}`, request).then(res => {
      if (res.status === 200) {
        this.setState({ isLoading: false });
        this.props.helper.handleCorrectResponse();
        this.setState({
          msgEn: '',
          msgFr: ''
        });
      } else {
        throw (res.status);
      }
    }).catch(error => {
      this.setState({ isLoading: false });
      this.props.helper.handleError(error);
    });

  }


  render() {
    const { isLoading } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Notification"
                content={
                  <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>

                    <Row style={{ padding: '0 30px 0 45px' }}>
                      <Col md={12}>
                        <FormGroup controlId="formControlsSelect">
                          <ControlLabel>
                            Message (English)
                          </ControlLabel>
                          <FormControl
                            componentClass="textarea"
                            placeholder="English Message"
                            name="msgEn"
                            value={this.state.msgEn}
                            onChange={this.handleInputChange}
                            required
                            maxLength={119}
                          />
                          <HelpBlock>Remaining caracters ({119 - this.state.msgEn.length})</HelpBlock>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row style={{ padding: '0 30px 0 45px' }}>
                      <Col md={12}>
                        <FormGroup controlId="formControlsSelect">
                          <ControlLabel>
                            Message (French)
                          </ControlLabel>
                          <FormControl
                            componentClass="textarea"
                            placeholder="French Message"
                            name="msgFr"
                            value={this.state.msgFr}
                            onChange={this.handleInputChange}
                            maxLength={119}
                          />
                          <HelpBlock>Remaining caracters ({119 - this.state.msgFr.length})</HelpBlock>
                        </FormGroup>
                      </Col>
                    </Row>

                    <FormGroup>
                      <Col md={12}>
                        <Button style={{ margin: '0 20px 0 30px' }} bsStyle="info" disabled={isLoading} onClick={this.handleSubmit} fill>
                          {isLoading ? 'Sending...' : this.state.submitMessage}
                        </Button>
                        {/*
                        <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                          {isLoading ? 'Sending...' : this.state.submitMessage}
                        </Button>
                        */}

                      </Col>
                    </FormGroup>
                  </Form>
                }
              />
            </Col>
          </Row>

        </Grid>
      </div>
    );
  }
}

const UpdatedComponent = withRouter(NewNotification);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedComponent {...props} helper={Helper} />}
  </AppContext.Consumer>
);
