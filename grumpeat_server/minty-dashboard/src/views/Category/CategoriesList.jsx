import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '../../components/CustomButton/CustomButton';
import {
  // Grid,
  Row,
  Col,
  InputGroup,
  Table,
  FormGroup,
  FormControl,
  Pagination,
  Form,
  ControlLabel
  // Button
} from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import { AppContext } from '../../containers/AppContext';
import { withRouter } from 'react-router-dom';
import QualityRow from '../../components/Table/QualityRow';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
// added by Raoudha
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CategoryRow from '../../components/Table/CategoryRow';
// var classes = makeStyles(theme => ({
//   root: {
//     width: '100%',
//     maxWidth: 360,
//     backgroundColor: theme.palette.background.paper
//   },
//   nested: {
//     paddingLeft: theme.spacing(4)
//   }
// }));

const classes = makeStyles(theme => ({
  root: {
    width: '85%',
    // maxWidth: '85%',
    // backgroundColor: theme.palette.background.paper
    backgroundColor: 'rgba(0,0,0,.03)',
    borderBottom: '1px solid rgba(0,0,0,.125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56
    }
  },
  heading: {
    fontSize: '24px',
    flexBasis: '100%',
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: 'theme.palette.text.secondary'
  }
}));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  console.log('list, startIndex, endIndex', list, startIndex, endIndex);
  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  //padding: grid * 2,
  //margin: `0 0 ${grid}px 0`,
  //display: "table",
  //table-row
  display: isDragging ? 'table' : 'table-row',
  //background: "white",

  // change background colour if dragging
  background: isDragging ? 'whitesmoke' : 'white',
  width: '100%',

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  //background: isDraggingOver ? "lightblue" : "lightgrey",
  background: 'white'
  //padding: grid,
  //width: 250

  //  height: isDraggingOver ? "100%" : "auto",
  // overflowY: isDraggingOver ? "scroll" : "auto",
});

class ListC extends Component {
  constructor(prop) {
    super(prop);

    this.state = {
      isLoading: false,
      alert: null,
      submitMessage: 'Submit',
      requestID: '',
      limit: 100,
      count: 0,
      active: 1,
      rowTitle: ['#', 'Category', 'Actions'],
      dataRows: [],
      dataCategories: [],
      searchKeyWord: '',
      name: '',
      initDataCategories: [],
      expanded: false,
      setExpanded: false
    };

    this.handleDelete = this.handleDelete.bind(this);
    this.getItems = this.getItems.bind(this);
    this.deleteItem = this.deleteItem.bind(this);

    this.onDragEnd = this.onDragEnd.bind(this);

    if (this.props.match.params.id && this.props.match.params.id !== ':id') {
      this.state.requestID = this.props.match.params.id;
    }
    console.log(this.props);
  }

  componentWillMount() {
    this.getItems();
    this.getCategoriesItems();
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }
  deleteItem(id) {
    const itemID = this.state.dataRows[id].id;

    axios
      .patch(
        `${this.props.helper.baseUrl}/Qualities/${itemID}?access_token=${this.props.helper.token}`,
        { status: 0 }
      )
      .then(res => {
        if (res.status === 200) {
          let updatedData = this.state.dataRows;
          updatedData.splice(id, 1);
          this.setState({ dataRows: updatedData });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  }

  handleDelete(id) {
    const itemID = this.state.dataCategories[id].id;
    axios
      .patch(
        `${this.props.helper.baseUrl}/Categories/${itemID}?access_token=${this.props.helper.token}`,
        { status: 0 }
      )
      .then(res => {
        if (res.status === 200) {
          let updatedData = this.state.dataCategories;
          updatedData.splice(id, 1);
          this.setState({ dataCategories: updatedData });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });

    // Patch Qualities Affected to that Category
    const qls = this.state.dataRows;
    console.log('############', qls);
    var qualitiesToBeDeleted = [];
    for (let i = 0; i < qls.length; i++) {
      if (qls[i].categoryId) {
        if (qls[i].categoryId === itemID) {
          qualitiesToBeDeleted.push(qls);
        }
      }
    }
    if (qualitiesToBeDeleted.length > 0) {
      for (let q = 0; q < qualitiesToBeDeleted.length; q++) {
        delete qls[q].categoryId;
        console.log('********** quality after delete categoryId :', qls[q]);

        axios
          .put(
            `${this.props.helper.baseUrl}/Qualities/${qls[q].id}?access_token=${this.props.helper.token}`,
            qls[q]
          )
          .then(res => {
            if (res.status === 200) {
              console.log('***********', res.data);
              // let updatedData = this.state.dataRows;
              // updatedData.splice(id, 1);
              // this.setState({ dataRows: updatedData });
              // this.props.helper.setAlert(null);
            } else {
              throw res.status;
            }
          })
          .catch(error => {
            this.props.helper.handleError(error);
          });
      }
    }
  }

  getItems(prop = 1, categoryId) {
    this.props.helper.handleWaiting();
    axios
      .get(`${this.props.helper.baseUrl}/Qualities`, {
        params: {
          filter: {
            // "where": {"or": [{"firstName": {"like": `${prop}.*`, "options": "i" }}, {"familytName": {"like": `${prop}.*`, "options": "i" }}]},
            limit: this.state.limit,
            skip: (prop - 1) * this.state.limit,
            where: { status: 1 },
            order: 'rank ASC'
            // categoryId: categoryId
          },
          access_token: this.props.helper.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({ dataRows: res.data });
          this.setState({ initDataRows: res.data });
          this.setState({ active: prop });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  }

  //**  get list Categories Items */

  getCategoriesItems = (prop = 1) => {
    this.props.helper.handleWaiting();
    axios
      .get(`${this.props.helper.baseUrl}/Categories`, {
        params: {
          filter: {
            limit: this.state.limit,
            skip: (prop - 1) * this.state.limit,
            where: { status: 1 },
            order: 'rank ASC'
          },
          access_token: this.props.helper.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          console.log('############', res.data);

          this.setState({ dataCategories: res.data });
          this.setState({ initDataCategories: res.data });
          this.setState({ active: prop });
          this.props.helper.setAlert(null);
        } else {
          throw res.status;
        }
      })
      .catch(error => {
        this.props.helper.handleError(error);
      });
  };

  async onDragEnd(result) {
    // dropped outside the list
    console.log('###########', result);

    if (
      !result.destination ||
      result.destination.index === result.source.index
    ) {
      return;
    }

    console.log('result', result);

    let start = {
      id: this.state.dataRows[result.source.index].id,
      from: result.source.index,
      to: result.destination.index,
      name: this.state.dataRows[result.source.index].name
    };

    let end = {
      id: this.state.dataRows[result.destination.index].id,
      from: result.destination.index,
      to: result.source.index,
      name: this.state.dataRows[result.destination.index].name
    };

    console.log('%cstart end', 'color:green;', start, end);

    const dataRows = reorder(
      this.state.dataRows,
      result.source.index,
      result.destination.index
    );

    this.setState(
      {
        dataRows
      },
      async () => {
        try {
          await axios.patch(
            `${this.props.helper.baseUrl}/Qualities/${start.id}?access_token=${this.props.helper.token}`,
            {
              rank: start.to
            }
          );

          await axios.patch(
            `${this.props.helper.baseUrl}/Qualities/${end.id}?access_token=${this.props.helper.token}`,
            {
              rank: end.to
            }
          );
        } catch (error) {
          this.props.helper.handleError(error);
        }
      }
    );
  }

  // added by Raoudha
  handleChange = panel => (event, isExpanded) => {
    isExpanded
      ? this.setState({ expanded: panel })
      : this.setState({ expanded: false });
  };

  render() {
    const { isLoading } = this.state;

    return (
      <div className='main-content'>
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                tableFullWidth
                content={
                  <DragDropContext onDragEnd={this.onDragEnd}>
                    <div style={{ height: 'auto', overflowY: 'auto' }}>
                      <Table
                        responsive
                        className='table-bigboy'
                        style={{ tableLayout: 'auto' }}>
                        {/* <thead>
                          <tr>
                            <th>{this.state.rowTitle[0]}</th>
                            <th>{this.state.rowTitle[1]}</th>
                            <th className='text-right'>
                              {this.state.rowTitle[2]}
                            </th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th>{this.state.rowTitle[0]}</th>
                            <th>{this.state.rowTitle[1]}</th>
                            <th className='text-right'>
                              {this.state.rowTitle[2]}
                            </th>
                          </tr>
                        </tfoot> */}

                        <Droppable droppableId='droppable'>
                          {(provided, snapshot) => (
                            <tbody
                              {...provided.droppableProps}
                              ref={provided.innerRef}
                              style={getListStyle(snapshot.isDraggingOver)}>
                              {this.state.dataCategories.map((el, key) => {
                                var ind= 0;
                                return (
                                  <Grid container className={classes.root}>
                                    <Grid item xs={11}>
                                      <ExpansionPanel
                                        style={{
                                          width: '100%',
                                          marginBottom: '15px'
                                        }}
                                        expanded={this.state.expanded===key}
                                        onChange={this.handleChange(key)}>
                                        <ExpansionPanelSummary
                                          style={{ fontSize: '22px' }}
                                          expandIcon={<ExpandMoreIcon />}
                                          aria-controls='panel1bh-content'
                                          id='panel1bh-header'>
                                          {el.name}
                                        </ExpansionPanelSummary>
                                        {this.state.dataRows.map(
                                          (prop, key) => {
                                            if (prop.categoryId === el.id) {
                                              // obj.ind++;
                                              // console.log(
                                              //   '***------++++++',
                                              //   obj.ind,
                                              //   prop.name
                                              // );

                                              return (
                                                <ExpansionPanelDetails>
                                                  <Draggable
                                                    key={prop.id}
                                                    draggableId={prop.id}
                                                    index={key}>
                                                    {(provided, snapshot) => (
                                                      <QualityRow
                                                        item={prop}
                                                        itemID={key}
                                                        ind={ind++}
                                                        key={key}
                                                        deleteItem={
                                                          this.deleteItem
                                                        }
                                                        innerRef={
                                                          provided.innerRef
                                                        }
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                          snapshot.isDragging,
                                                          provided
                                                            .draggableProps
                                                            .style
                                                        )}
                                                      />
                                                    )}
                                                  </Draggable>
                                                </ExpansionPanelDetails>
                                              );
                                            }
                                          }
                                        )}
                                      </ExpansionPanel>
                                    </Grid>
                                    <Grid item xs={1}>
                                      <CategoryRow
                                        item={el}
                                        itemID={key}
                                        key={key}
                                        deleteItem={
                                          this.handleDelete
                                        }></CategoryRow>
                                    </Grid>
                                  </Grid>
                                );
                              })}
                              {provided.placeholder}
                            </tbody>
                          )}
                        </Droppable>
                      </Table>
                    </div>
                  </DragDropContext>
                }
                legend={<div></div>}
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
const UpdatedComponent = withRouter(ListC);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedComponent {...props} helper={Helper} />}
  </AppContext.Consumer>
);
