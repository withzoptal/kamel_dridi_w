import React, { Component } from 'react';
import { Grid, Row, Col, Pagination, InputGroup, FormGroup, FormControl } from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import { AppContext } from "../../containers/AppContext";
import UserRow from "../../components/Table/UserRow";
import _ from "lodash";

class UsersList extends Component {
    constructor(prop) {
        super(prop);

        this.state = {
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['Client', 'Email', 'Total Favorite', 'Followers', 'Following', "Action"],
            "dataRows": [],
            paginationItems: [],
            searchKeyWord: '',
            followsData: [],
            favoriteData: []
        };

        this.goToPage = this.goToPage.bind(this);
        this.getUserFavorite = this.getUserFavorite.bind(this);
        this.getUserFollowers = this.getUserFollowers.bind(this);
        this.getUserFollowing = this.getUserFollowing.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getCount = this.getCount.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount() {
        this.getItems();
        this.getCount();
        //get all following data to treat it instead of snding request for every user
        axios.get(`${this.props.helper.baseUrl}/Follows?access_token=${this.props.helper.token}`).then(res => {
            if (res.status === 200) {  
                
                this.setState({followsData: res.data});
              } else {
                throw(res.status);
              }
        }).catch( error => { this.props.helper.handleError(error); } );


        //get all favorite data to treat it instead of snding request for every user
        axios.get(`${this.props.helper.baseUrl}/RestFavs?access_token=${this.props.helper.token}`).then(res => {
            if (res.status === 200) {  
                
                this.setState({favoriteData: res.data});
              } else {
                throw(res.status);
              }
        }).catch( error => { this.props.helper.handleError(error); } );
    }

    getUserFollowers(id){
        return this.state.followsData.filter(follow => follow.profilId === id);
    }

    getUserFollowing(id){
        return this.state.followsData.filter(follow => follow.followerId === id);
    }

    getUserFavorite(id){
        return this.state.favoriteData.filter(favorite => favorite.userId === id);
    }

    getCount(prop){

        if(prop){
          // get the count
          axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
            params: {
              
              filter:{
                "where": {"and": [
                    {"statu": 1},
                    {"role": "client"},
                    {"or": [
                        {"firstName": {"like": `${prop}.*`, "options": "i" }},
                        {"lastName": {"like": `${prop}.*`, "options": "i" }},
                        {"email": {"like": `${prop}.*`, "options": "i" }},
                    ]}
                ]}
              },
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              const pageCount = Math.ceil(res.data.length/this.state.limit);
              this.setState({count: pageCount});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }else{
          // get the count
          axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"statu": 1},
                        {"role": "client"}
                    ]}
                },
                //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              const pageCount = Math.ceil(res.data.length/this.state.limit);
              this.setState({count: pageCount});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }
    
      }


    /*getItems(prop = 1) {
        this.props.helper.handleWaiting();
        axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"statu": 1},
                        {"role": "client"}
                    ]},
                    limit: this.state.limit,
                    skip: ((prop - 1) * this.state.limit),

                },
                // access_token: this.props.helper.token
            }
        }).then(res => {
            if (res.status === 200) {
                this.setState({ dataRows: res.data });
                console.log(res.data)
                this.setState({ active: prop });
                this.props.helper.setAlert(null);
            } else {
                throw (res.status);
            }
        }).catch(error => { this.props.helper.handleError(error); });
    }*/

    getItems(prop){
        
        this.props.helper.handleWaiting();
        if(prop){
          // for the actual list with name
          axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"statu": 1},
                        {"role": "client"},
                        {"or": [
                            {"firstName": {"like": `${prop}.*`, "options": "i" }},
                            {"lastName": {"like": `${prop}.*`, "options": "i" }},
                            {"email": {"like": `${prop}.*`, "options": "i" }},
                        ]}
                    ]},
                    limit: this.state.limit,
                    skip: ((prop - 1) * this.state.limit),
                    order: 'createdAt DESC'

                },
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              this.setState({dataRows: res.data});
              this.setState({alert: null});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }else{
          // for the actual list without names
          axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
            params: {
              filter: {
                "limit": this.state.limit,
                "skip": 0,
                "where": {"and": [
                    {"statu": 1},
                    {"role": "client"}
                ]},
                order: 'createdAt DESC'
              },
              
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              this.setState({dataRows: res.data});
              this.setState({alert: null});
              this.props.helper.setAlert(null); 
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        }
    
      }

    debouncedSearch = _.debounce(()=>{
        this.getItems(this.state.searchKeyWord);
        this.getCount(this.state.searchKeyWord);
        this.setState({active: 1});
      }, 800);

    handleSearch(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        },this.debouncedSearch);
      }


      goToPage(prop) {
        // for the actual list
        axios.get(`${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`, {
          params: {
            filter: {
              "limit": this.state.limit,
              //"include": "category",
              "skip": ((prop - 1) * this.state.limit),
              "where": {"and": [
                {"statu": 1},
                {"role": "client"}
                ]},
            }
          }
        }).then(res => {
          if (res.status === 200) {
            this.setState({ dataRows: res.data });
            this.setState({ active: prop })
          } else {
            throw (res.status);
          }
        }).catch(error => { this.handleError(error); });
      }

    deleteItem(id) {
        const itemID = this.state.dataRows[id].id;
        axios.patch(`${this.props.helper.baseUrl}/Profils/${itemID}/?access_token=${this.props.helper.token}`, {
            "status": 0
        }).then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                updatedData.splice(id, 1);
                this.setState({ dataRows: updatedData });
                this.props.helper.setAlert(null);
                this.getItems();
            } else {
                throw (res.status);
            }
        }).catch(error => { this.props.helper.handleError(error); });
    }

    render() {
        return (
            <div className="main-content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title={
                                    <Col md={12}>
                                      <FormGroup>
                                        <InputGroup>
                                          <FormControl
                                            type="text"
                                            placeholder="Search for user email/ First name / Last name"
                                            name="searchKeyWord"
                                            value={this.state.searchKeyWord}
                                            onChange={this.handleSearch}
                                          />
                                          <InputGroup.Addon>
                                            <i className="fa fa-search"></i>
                                          </InputGroup.Addon>
                                        </InputGroup>
                                      </FormGroup>
                                    </Col>
                                  }
                                content={
                                    <div className="fresh-datatables">
                                        <table id="datatables" ref="main" className="table table-striped table-no-bordered table-hover"
                                            cellSpacing="0" width="100%" style={{ width: "100%" }}>
                                            <thead>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                {
                                                    this.state.dataRows.map((prop, key) => {
                                                        return (
                                                            <UserRow item={prop} key={key} itemID={key} deleteItem={this.deleteItem} followers={this.getUserFollowers(prop.id)} following={this.getUserFollowing(prop.id)} favorite={this.getUserFavorite(prop.id)} />
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.goToPage}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

export default props => (
    <AppContext.Consumer>
        {Helper => <UsersList {...props} helper={Helper} />}
    </AppContext.Consumer>
);
