import React, {Component} from 'react';
import {Col, ControlLabel, Form, FormControl, FormGroup, Grid, Row, Panel, PanelGroup, Checkbox} from "react-bootstrap";
import Card from "../../components/Card/Card";
import Button from "../../components/CustomButton/CustomButton";
import Select from 'react-select';
import {withRouter} from "react-router-dom";
import {AppContext} from "../../containers/AppContext";
import axios from "axios";
import * as _ from 'lodash';
import ImageUploader from "react-images-upload";

class NewUser extends Component {

    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            alert: null,
            submitMessage: 'Submit',
            requestID: '',
            requestID: '',
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            username: '',
            confirmPass: '',
            image: {},
            img: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.populateForm = this.populateForm.bind(this);
        this.onFileChange = this.onFileChange.bind(this);

        if(this.props.match.params.id && (this.props.match.params.id !== ':id')){
            this.state.requestID = this.props.match.params.id;
        }
    }


    async componentWillMount(){

       

        if(this.state.requestID){
            try{
                this.props.helper.handleWaiting();
                const result = await axios.get(`${this.props.helper.baseUrl}/Profils/${this.state.requestID}`,  {
                    params: {
                        filter: {
                            "where" : {"status": 1},
                        },
                        access_token: this.props.helper.token
                    }
                });
                if (result.status == 200) {
                    this.populateForm(result.data)
                } else {
                    throw(result.status);
                }
            }catch(error){
                this.props.helper.handleError(error);
            }
        }
    }

    async handleSubmit(e) {
        e.preventDefault();

        this.setState({ isLoading: true });
        this.props.helper.handleWaiting();
        let imageState = this.state.image;

        if(this.state.password !== this.state.confirmPass){
            let error = {}
             error.response = {};
             error.response.data = {};
             error.response.data.error = {}; 
             error.response.data.error.message = "Please confirm your password";
            this.props.helper.handleError(error);
            this.setState({ isLoading: false });
            return false;
        }


        // Upload images
        //disable image upload for the mean time
        const _this = this;
        const promises = Object.keys(imageState).map(async function (objectKey, key) {
            if (imageState[objectKey].data) {
                const imageData = new FormData();
                imageData.append('file', imageState[objectKey].data);
                try {
                    const image = await axios.post(`${_this.props.helper.baseUrl}/Containers/${this.props.helper.container}/upload`, imageData);
                    console.log('result image', image)
                    imageState[objectKey].url = image.data.result.files.file[0].name;
                } catch (error) {
                    _this.props.helper.handleError(error);
                }
            }
        });

        await Promise.all(promises);

        let request = {
            "email": this.state.email,
            "username": this.state.username,
            "firstName": this.state.firstName,
            "lastName": this.state.lastName,
            "password": this.state.password,
        };

        Object.keys(imageState).map((prop, idx) => {
            request[prop] = imageState[prop].url;
        });

        //password is not required when update
        if(this.state.requestID && this.state.password === ''){
            delete request['password'];
        }

        const requestURL = this.state.requestID ?
            `${this.props.helper.baseUrl}/Profils/${this.state.requestID}?access_token=${this.props.helper.token}`:
            `${this.props.helper.baseUrl}/Profils?access_token=${this.props.helper.token}`;

        try{
            const result = await (this.state.requestID ? axios.patch(requestURL, request) : axios.post(requestURL, request));
            this.props.helper.setAlert(null);
            this.props.history.push('/users');
            if(result.status == 200){
                this.setState({ isLoading: false });
            }
        }catch(error){
            this.setState({ isLoading: false });
            this.props.helper.handleError(error);
        }

    }
    handleInputChange(event) {
       
            const target = event.target;
            const name = target.name;
            const value = target.type === 'checkbox' ? target.checked : target.value;

            const baseObjectName =name.split('[')[0].split('.')[0];
            const baseObject = _.get(this.state, baseObjectName);
            const partToModify = name.replace(baseObjectName, '');

            if(partToModify){
                _.set(baseObject, partToModify, value);
                this.setState({[baseObjectName]: baseObject});
            }else{
                this.setState({[baseObjectName]: value});
            }

    }

    populateForm(prop){
        prop = prop ? prop : {};
        let submitMessage = prop ? 'update' : 'submit';
        
        this.setState({
            requestID: prop.id || '',
            submitMessage: submitMessage,
            isLoading: false,
            email: prop.email || '',
            username: prop.username || '',
            firstName: prop.firstName || '',
            lastName: prop.lastName || '',
            img: prop.image || '',
        });

        this.props.helper.setAlert(null);
    }

    onFileChange(picture, ref) {
        const image = this.state.image;
        if (picture.length == 0) {
            delete image[ref];
        } else {
            const currentImage = {};
            currentImage.data = (picture && picture[0]) || null;
            currentImage.name = (picture && picture[0] && picture[0].name) || '';
            image[ref] = currentImage;
        }
        this.setState({ image: image });
    }


    render() {

        const { isLoading } = this.state;
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Add User"
                                content={
                                    <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>

                                            <FormGroup controlId="formControlsFirstname">
                                                <ControlLabel className="col-md-2">
                                                    First Name *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="text"
                                                        placeholder="First Name"
                                                        name="firstName"
                                                        value={this.state.firstName}
                                                        onChange={this.handleInputChange}
                                                        required
                                                    />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup controlId="formControlsLastname">
                                                <ControlLabel className="col-md-2">
                                                    Last Name *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="text"
                                                        placeholder="Last Name"
                                                        name="lastName"
                                                        value={this.state.lastName}
                                                        onChange={this.handleInputChange}
                                                        required
                                                    />
                                                </Col>
                                            </FormGroup>
                                            <FormGroup controlId="formControlsUsername">
                                                <ControlLabel className="col-md-2">
                                                    Nick Name *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="text"
                                                        placeholder="Nick Name"
                                                        name="username"
                                                        value={this.state.username}
                                                        onChange={this.handleInputChange}
                                                        required
                                                    />
                                                </Col>
                                            </FormGroup>

                                            <FormGroup controlId="formControlsEmail">
                                                <ControlLabel className="col-md-2">
                                                    Email *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="email"
                                                        placeholder="Email"
                                                        name="email"
                                                        value={this.state.email}
                                                        onChange={this.handleInputChange}
                                                        required
                                                    />
                                                </Col>
                                            </FormGroup>


                                            <FormGroup controlId="formControlsPass">
                                                <ControlLabel className="col-md-2">
                                                    Password *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="password"
                                                        placeholder="Password"
                                                        name="password"
                                                        value={this.state.password}
                                                        onChange={this.handleInputChange}
                                                        
                                                    />
                                                </Col>
                                            </FormGroup>

                                            <FormGroup controlId="formControlsPassConfirm">
                                                <ControlLabel className="col-md-2">
                                                    Confirm Password *
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <FormControl
                                                        type="password"
                                                        placeholder="Confirm Password"
                                                        name="confirmPass"
                                                        value={this.state.confirmPass}
                                                        onChange={this.handleInputChange}
                                                        required={this.state.password !== ''}
                                                    />
                                                </Col>
                                            </FormGroup>

                                            <FormGroup controlId="formControlsSelect">
                                                <ControlLabel className="col-md-2">
                                                    Image
                                                </ControlLabel>
                                                <Col md={9}>
                                                    <ImageUploader
                                                        label="Image"
                                                        withIcon={true}
                                                        withPreview={true}
                                                        buttonText='Choose image'
                                                        maxFileSize={5242880}
                                                        singleImage={true}
                                                        defaultImage={this.state.requestID && this.state.img && (this.props.helper.baseUrl + `/Containers/${this.props.helper.container}/download/` + this.state.img)}
                                                        onChange={(picture) => this.onFileChange(picture, "image")}
                                                    />
                                                </Col>
                                            </FormGroup>


                                            <FormGroup>
                                                <Col md={10} mdOffset={2}>
                                                    <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                                                        {isLoading ? 'processing...' : this.state.submitMessage}
                                                    </Button>
                                                </Col>
                                            </FormGroup>
                                        
                                    </Form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>

        );
    }
}

const UpdatedComponent = withRouter(NewUser);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedComponent {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
