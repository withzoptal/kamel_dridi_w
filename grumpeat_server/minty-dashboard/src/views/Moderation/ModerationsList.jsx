import React, { Component } from 'react';
import { Grid, Row, Col, Pagination, InputGroup, FormGroup, FormControl, Modal, Form, ControlLabel } from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import { AppContext } from "../../containers/AppContext";
import _ from "lodash";
import ModerationRow from '../../components/Table/ModerationRow';
import Select from 'react-select';

class ModerationsList extends Component {
    constructor(prop) {
        super(prop);

        this.state = {
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['Name', 'address', 'Date', 'Category', 'From', "Action"],
            "dataRows": [],
            paginationItems: [],
            searchKeyWord: '',
            show: false,
            submitMessage: 'Submit',
            selectedCategory: '',
            requestID: '',
            dataRestau: {},
            selectedCategory: '',
        };

        this.getItems = this.getItems.bind(this);
        this.getCount = this.getCount.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handlePopUp = this.handlePopUp.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        this.getItems();
        this.getCount();
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    handlePopUp(id){

        const itemID = this.state.dataRows[id].id;
        this.setState({requestID: itemID});

        axios.get(`${this.props.helper.baseUrl}/Restaurants/${itemID}`, {
            params: {
              filter: {
                "limit": this.state.limit,
                "skip": 0,
                "where": {"and": [
                    {"status": 1},
                    { "price.from" : { nin:["admin", "google"] } } 
                ]}
              }
              
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              this.setState({dataRestau: res.data, selectedCategory: res.data.price.tier});
              this.setState({alert: null});
              this.props.helper.setAlert(null); 
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        
        this.handleShow();
        
        console.log('id itemID', id, itemID);
    }

    async handleSubmit(e) {
        e.preventDefault();

        this.setState({ isLoading: true });
        this.props.helper.handleWaiting();

        let request = {
            "price": {
                ...this.state.dataRestau.price,
                "tier": this.state.selectedCategory,
                "from": "admin"
            }
        }
        

        axios.patch(`${this.props.helper.baseUrl}/Restaurants/${this.state.requestID}`, request)
        .then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                let id = this.state.dataRows.findIndex(restau => restau.id === this.state.requestID);
                updatedData.splice(id, 1);
                this.setState({ dataRows: updatedData });
                this.props.helper.setAlert(null);
                this.handleClose();
            } else {
              throw (res.status);
            }
          }).catch(error => { this.props.helper.handleError(error); });

    }

    getCount(prop){

          // get the count
          axios.get(`${this.props.helper.baseUrl}/Restaurants?access_token=${this.props.helper.token}`, {
            params: {
              
              filter:{
                "where": {"and": [
                    {"status": 1},
                    { "price.from" : { nin:["admin", "google"] } } ,
                    {"or": [
                         {"name": {"like": `${this.state.searchKeyWord}.*`, "options": "i" }},
                    ]}
                ]}
              },
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {
              const pageCount = Math.ceil(res.data.length/this.state.limit);
              this.setState({count: pageCount});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
    
      }


    

    getItems(prop){
        this.props.helper.handleWaiting();
          // for the actual list with name
          axios.get(`${this.props.helper.baseUrl}/Restaurants?access_token=${this.props.helper.token}`, {
            params: {
                filter: {
                    "where": {"and": [
                        {"status": 1},
                        { "price.from" : { nin:["admin", "google"] } },
                        {"or": [
                             {"name": {"like": `${this.state.searchKeyWord}.*`, "options": "i" }},
                            
                        ]}
                    ]},
                    limit: this.state.limit,
                    skip: ((prop - 1) * this.state.limit),
                    order: "createdAt DESC",

                }
              //access_token: this.props.helper.token
            }
          }).then(res => {
            if (res.status === 200) {

              this.setState({dataRows: res.data, active: prop, alert: null});
              this.props.helper.setAlert(null);
            } else {
              throw(res.status);
            }
          }).catch( error => { this.props.helper.handleError(error); } );
        
    
      }

    debouncedSearch = _.debounce(()=>{
        this.getItems();
        this.getCount();
        
      }, 800);

    handleSearch(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        },this.debouncedSearch);
      }

    deleteItem(id) {
        const itemID = this.state.dataRows[id].id;
        axios.patch(`${this.props.helper.baseUrl}/Restaurants/${itemID}/?access_token=${this.props.helper.token}`, {
            "status": 0
        }).then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                updatedData.splice(id, 1);
                this.setState({ dataRows: updatedData });
                this.props.helper.setAlert(null);
                this.getItems();
            } else {
                throw (res.status);
            }
        }).catch(error => { this.props.helper.handleError(error); });
    }

    render() {
        const { isLoading } = this.state;
        return (
            <div className="main-content">
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Charger une édition</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Grid fluid>
                            <Row>
                                <Col md={12}>

                                    <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>

                                    <FormGroup>
                                        <ControlLabel className="col-md-2">
                                            Category
                                        </ControlLabel>
                                        <Col md={9}>
                                            <Select
                                            name="selectedCategory"
                                            value={this.state.selectedCategory}
                                            options={[
                                                { label: "$", value: 1 },
                                                { label: "$$", value: 2 },
                                                { label: "$$$", value: 3 },
                                                { label: "$$$$", value: 4 }
                                                //this.state.categoriesOptions
                                            ]}
                                            onChange={(v) => { this.setState({ selectedCategory: v && v.value || '' }) }}
                                            required
                                            />
                                        </Col>
                                    </FormGroup>
                                        
                                        <FormGroup>
                                            <Col md={10} mdOffset={2}>
                                                <Button bsStyle="info" disabled={isLoading} type="submit"
                                                    fill>
                                                    {isLoading ? 'loading..' : this.state.submitMessage}
                                                </Button>
                                            </Col>
                                        </FormGroup>
                                    </Form>

                                </Col>
                            </Row>
                        </Grid>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Fermer</Button>
                    </Modal.Footer>
                </Modal>
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title={
                                    <Col md={12}>
                                      <FormGroup>
                                        <InputGroup>
                                          <FormControl
                                            type="text"
                                            placeholder="Search by name"
                                            name="searchKeyWord"
                                            value={this.state.searchKeyWord}
                                            onChange={this.handleSearch}
                                          />
                                          <InputGroup.Addon>
                                            <i className="fa fa-search"></i>
                                          </InputGroup.Addon>
                                        </InputGroup>
                                      </FormGroup>
                                    </Col>
                                  }
                                content={
                                    <div className="fresh-datatables">
                                        <table id="datatables" ref="main" className="table table-striped table-no-bordered table-hover"
                                            cellSpacing="0" width="100%" style={{ width: "100%" }}>
                                            <thead>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    {
                                                        this.state.rowTitle.map((row, i) => (
                                                            <th key={i} className={ i === this.state.rowTitle.length -1 && "text-right"}>{row}</th>
                                                        ))
                                                    }
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                {
                                                    this.state.dataRows.map((prop, key) => {
                                                        return (
                                                            <ModerationRow item={prop} key={key} itemID={key} deleteItem={this.deleteItem} handlePopUp={this.handlePopUp} />
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

export default props => (
    <AppContext.Consumer>
        {Helper => <ModerationsList {...props} helper={Helper} />}
    </AppContext.Consumer>
);
