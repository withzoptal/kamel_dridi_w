import React, { Component } from 'react';
import {
  Grid, Row, Col,
  FormGroup, FormControl, InputGroup, Pagination
} from 'react-bootstrap';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import Card from '../../components/Card/Card.jsx';
import MapCard from '../../components/Card/MapCard.jsx';
import 'react-select/dist/react-select.css';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import RestaurantRow from '../../components/Table/RestaurantRow';
import imagePlaceHolder from '../../assets/img/placeholder_dash.png';
import Img from 'react-image';
import _ from 'lodash';
  
const baseUrl = 'http://3.0.175.106/api';
const endPoint = `${baseUrl}/Restaurants`;
const googlePlaceKey = "AIzaSyCLtOx4tx1-E0kyi5XqVEVDLJKmNwKS3sI";
let token;

class RestaurantsList extends Component {
  constructor(props) {
    super(props);

    token = sessionStorage.getItem('grabingo_jwt');
    this.state = {
      limit: 10,
      count: 0,
      active: 1,
      alert: null,
      "headerRow": ['Partner', 'Phone', 'Actions'],
      "footerRow": ['Partner', 'Phone', 'Actions'],
      "dataRows": [],
      searchKeyWord: '',
      zoom: 6,
      currentPartner: null,
      partnerCategory: null,
      restQualsData: 0,
    };

    this.getQualityNumber = this.getQualityNumber.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleWaiting = this.handleWaiting.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
    this.newAlert = this.newAlert.bind(this);
    this.goToPage = this.goToPage.bind(this);
    this.getCount = this.getCount.bind(this);
    this.getItems = this.getItems.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentWillMount() {
    this.getCount();
    this.getItems();
    //get total number of restaurant qualities
    axios.get(`${baseUrl}/RestQuals`).then(res => {
      if (res.status === 200) {
        
        this.setState({ restQualsData: res.data });
       
      } else {
        throw (res.status);
      }
    }).catch(error => { this.handleError(error); });

  }

  getQualityNumber(qualityId, restauId){
    let total = this.state.restQualsData.length;
    let qualOccurence = this.state.restQualsData.filter(qual => qual.qualityId === qualityId && restauId === qual.restaurantId);
    return qualOccurence.length
  }

  debouncedSearch = _.debounce(() => {
    this.getItems(this.state.searchKeyWord);
    this.getCount(this.state.searchKeyWord);
    this.setState({ active: 1 });
  }, 800);

  handleSearch(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.debouncedSearch);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  newAlert(alert) {
    this.setState({ alert: alert });
  }

  hideAlert() {
    this.setState({ alert: null });
  }

  handleWaiting() {
    this.setState({
      alert: (
        <SweetAlert
          title="Waiting"
          style={{ display: "block", marginTop: "-100px" }}
          showConfirm={false}
          onConfirm={prop => { }}
        >
          Fetching data from the server
        </SweetAlert>
      )
    });
  }

  handleError(error) {
    let errorMsg = '';
    let errorStatus = '';
    if (error.response) {
      errorStatus = error.response.status;
      errorMsg = error.response.data.error.message;
    } else {
      errorStatus = 'Error';
      errorMsg = 'The app encountred an error with the server';
    }

    this.setState({
      alert: (
        <SweetAlert
          danger
          title={errorStatus}
          confirmBtnBsStyle="danger"
          style={{ display: "block", marginTop: "-100px" }}
          onConfirm={() => this.setState({ alert: null })}
        >
          {errorMsg}
        </SweetAlert>
      )
    });
  }

  selectItem(id) {
    const currentItem = id ? this.state.dataRows[id] : this.state.dataRows[0];
    this.setState({ currentPartner: currentItem });
    this.setState({ zoom: 13 });
  }

  deleteItem(id) {
    const itemID = this.state.dataRows[id].id;

    axios.delete(`${endPoint}/${itemID}?access_token=${token}`).then(res => {
      if (res.status === 200) {
        let updatedData = this.state.dataRows;
        updatedData.splice(id, 1);
        this.setState({ dataRows: updatedData });
        this.hideAlert();
      } else {
        throw (res.status);
      }
    }).catch(error => { this.handleError(error); });
  }

  goToPage(prop) {
    // for the actual list
    axios.get(endPoint, {
      params: {
        filter: {
          "limit": this.state.limit,
          //"include": "category",
          "skip": ((prop - 1) * this.state.limit),
          "include": {
            "relation": "qualities",
            "scope": { 
              "include": ["quality"] 
            }
          }
        },
        access_token: token
      }
    }).then(res => {
      if (res.status === 200) {
        this.setState({ dataRows: res.data });
        this.setState({ active: prop })
      } else {
        throw (res.status);
      }
    }).catch(error => { this.handleError(error); });
  }

  getCount(prop) {

    if (prop) {
      // get the count
      axios.get(`${endPoint}/count`, {
        params: {
          "where": { "name": { "like": `${prop}.*`, "options": "i" } },
          access_token: token
        }
      }).then(res => {
        if (res.status === 200) {
          const pageCount = Math.ceil(res.data.count / this.state.limit);
          this.setState({ count: pageCount });
        } else {
          throw (res.status);
        }
      }).catch(error => { this.handleError(error); });
    } else {
      // get the count
      axios.get(`${endPoint}/count`, {
        params: {
          access_token: token
        }
      }).then(res => {
        if (res.status === 200) {
          const pageCount = Math.ceil(res.data.count / this.state.limit);
          this.setState({ count: pageCount });
        } else {
          throw (res.status);
        }
      }).catch(error => { this.handleError(error); });
    }

  }

  getItems(prop) {
    this.handleWaiting();
    if (prop) {
      // for the actual list with name
      axios.get(endPoint, {
        params: {
          filter: {
            "where": { "name": { "like": `${prop}.*`, "options": "i" } },
            "limit": this.state.limit,
            //  "include": "category",
            "skip": 0,
            "include": {
              "relation": "qualities",
              "scope": { 
                "include": ["quality"] 
              }
            }
          },
          access_token: token
        }
      }).then(res => {
        if (res.status === 200) {
          this.setState({ dataRows: res.data });
          this.setState({ alert: null });
        } else {
          throw (res.status);
        }
      }).catch(error => { this.handleError(error); });
    } else {
      // for the actual list without names
      axios.get(endPoint, {
        params: {
          filter: {
            "limit": this.state.limit,
            //  "include": "category",
            "skip": 0,
            "include": {
              "relation": "qualities",
              "scope": { 
                "include": ["quality"] 
              }
            }
          },
          access_token: token
        }
      }).then(res => {
        if (res.status === 200) {

          this.setState({ dataRows: res.data });
          this.setState({ alert: null });
        } else {
          throw (res.status);
        }
      }).catch(error => { this.handleError(error); });
    }

  }

  render() {
    //TODO: logo & cover sizes
    const currentPartner = this.state.currentPartner || {};
    //const category = currentPartner.category || {};
    const address = currentPartner.location;
    const image = currentPartner.photos && currentPartner.photos[0] && currentPartner.photos[0].photo_reference || '';
    const logo = currentPartner.avatar || '';
    let imgSrc = (image) ? `${baseUrl}/Containers/${this.props.helper.container}/download/${image}` : '';
    if (image && currentPartner.googleId !== undefined) {
        imgSrc = `https://maps.googleapis.com/maps/api/place/photo?photoreference=${image}&key=${googlePlaceKey}&maxheight=500`
    }
    
    const logoSrc = (logo) ? `${baseUrl}/Containers/${this.props.helper.container}/download/${logo}` : '';
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Col md={6}>
              <div>
                {this.state.alert}
                <Grid fluid>
                  <Row>
                    <Col md={12}>
                      <Card
                        title={
                          <Col md={12}>
                            <FormGroup>
                              <InputGroup>
                                <FormControl
                                  type="text"
                                  placeholder="Search for user by name"
                                  name="searchKeyWord"
                                  value={this.state.searchKeyWord}
                                  onChange={this.handleSearch}
                                />
                                <InputGroup.Addon>
                                  <i className="fa fa-search"></i>
                                </InputGroup.Addon>
                              </InputGroup>
                            </FormGroup>
                          </Col>
                        }
                        content={
                          <div className="fresh-datatables">
                            <table id="datatables" ref="main" className="table table-striped table-no-bordered table-hover"
                              cellSpacing="0" width="100%" style={{ width: "100%" }}>
                              <thead>
                                <tr>
                                  <th>{this.state.headerRow[0]}</th>
                                  <th>{this.state.headerRow[1]}</th>
                                  <th className="text-right">{this.state.headerRow[2]}</th>
                                </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>{this.state.footerRow[0]}</th>
                                  <th>{this.state.footerRow[1]}</th>
                                  <th className="text-right">{this.state.footerRow[2]}</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                {
                                  this.state.dataRows.map((prop, key) => {
                                    return (
                                      <RestaurantRow item={prop} itemID={key} key={key} deleteItem={this.deleteItem} selectItem={this.selectItem} hideAlert={this.hideAlert} newAlert={this.newAlert} />
                                    )
                                  })
                                }
                              </tbody>
                            </table>
                          </div>
                        }
                        legend={
                          <Pagination
                            first
                            next
                            prev
                            last
                            ellipsis
                            maxButtons={10}
                            boundaryLinks
                            items={this.state.count}
                            activePage={this.state.active}
                            onSelect={this.goToPage}
                          />
                        }
                      />
                    </Col>
                  </Row>
                </Grid>
              </div>
            </Col>
            <Col md={6}>
              <Card
                title="Partner details"
                content={
                  <div style={{ margin: 'auto auto 20px auto' }}>
                    <Row>
                      <Col md={6}>
                        <Img
                          src={imgSrc || imagePlaceHolder}
                          className="img-responsive"
                          alt=" "
                          loader={<div className="loader"> </div>}
                        />
                      </Col>
                      <Col md={6}>

                        <table id="datatables" className="table table-striped table-no-bordered table-hover"
                          cellSpacing="0" width="100%" style={{ width: "100%" }}>
                          <thead>
                            <tr>
                              <th style={{"paddingTop": 0}}>Qualities</th>
                              <th style={{"paddingTop": 0}} className="text-right">Users</th>
                            </tr>
                          </thead>
                       
                          <tbody>
                            
                           
                            {
                              currentPartner && currentPartner.qualities && currentPartner.qualities.map((quality, i) => {
                                console.log("%cpartner", "color:red;", currentPartner);
                                if( i == currentPartner.qualities.findIndex(qual => qual.qualityId === quality.qualityId)){
                                  return (
                                    <tr key={i}>
                                      <td>{quality && quality.quality && quality.quality.name}</td>
                                      <td className="text-right">{this.getQualityNumber(quality.qualityId, currentPartner.id)}</td>
                                    </tr>
                                  );
                                }
                              })
                            }
                           
                          </tbody>
                        </table>

                      </Col>
                    </Row>

                   
                    <img />
                    <div style={{ margin: '20px 0 0 10px' }}>
                      <p>Name: {currentPartner.name || ''}</p>
                      <p>Category: {currentPartner && currentPartner.price && currentPartner.price.tier ? "$".repeat(currentPartner.price.tier) : ''}</p>
                      <p>Adress: {currentPartner.formatted_address || ''}</p>
                      {/* <p>Specialty: {currentPartner && currentPartner.categories && currentPartner.categories.name}</p> */}
                      <p>Phone: {currentPartner && currentPartner.formatted_phone_number }</p>
                     
                    </div>
                  </div>
                }
              />
              <MapCard
                title="Partner Location"
                content={
                  <Map
                    containerStyle={{ position: 'relative' }}
                    style={{ width: '100%', height: '300px', position: 'relative' }}
                    google={this.props.google}
                    initialCenter={{ lat: 34.5536302, lng: 10.021561 }}
                    center={{ lat: (currentPartner.location && currentPartner.location.lat) || 34.5536302, lng: (currentPartner.location && currentPartner.location.lng) || 10.021561 }}
                    zoom={this.state.zoom || 6}
                    clickableIcons={false}
                  >
                    <Marker onClick={this.onMarkerClick}
                      position={{ lat: (currentPartner.location && currentPartner.location.lat) || '', lng: (currentPartner.location && currentPartner.location.lng) || '' }}
                      name={'Current location'}
                    />
                  </Map>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyBc8CzSpRmy4fWh_E_9yED7NlON9x5oOcg"
})(RestaurantsList);
