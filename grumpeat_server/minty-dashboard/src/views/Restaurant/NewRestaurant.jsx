import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Form, ButtonGroup, Button as Buttong, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Button from '../../components/CustomButton/CustomButton.jsx';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios';
import ImageUploader from 'react-images-upload';
import { withRouter } from 'react-router-dom'
import { AppContext } from "../../containers/AppContext";
import countryList from '../../variables/countryList';
import Datetime from 'react-datetime';
import { Typeahead, AsyncTypeahead } from 'react-bootstrap-typeahead';
import moment from "moment";
var _ = require('lodash');

export class NewRestaurant extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      alert: null,
      submitMessage: 'Submit',
      requestID: '',
      name: '',
      email: '',
      workingHourDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      selectedWorkingDays: {},
      lastSelectedDay: '',
      countryOptions: [],
      selectedCountry: '',
      image: {},
      specialtiesOptions: [],
      selectedSpecialty: '',
      phone: '',
      url: '',
      address: '',
      lat: '',
      lng: '',
      city: '',
      about: '',
      postalCode: '',
      specialtiesData: [],
      img: '',
      restaurantOptions: [],
      isSearchLoading: false,
      fetchedRestaurants: [],
      fromGoogle: false,
      googleRestaurant: null
    };

    this.getPropertyFromAddress = this.getPropertyFromAddress.bind(this);
    this.getDateFromTime = this.getDateFromTime.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleWorkingHours = this.handleWorkingHours.bind(this);
    this.handleWorkingDay = this.handleWorkingDay.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.populateForm = this.populateForm.bind(this);
    this.onFileChange = this.onFileChange.bind(this);

    if (this.props.match.params.id && (this.props.match.params.id !== ':id')) {
      this.state.requestID = this.props.match.params.id;
    }
    this._instance = React.createRef();
  }

  async componentWillMount() {
    //fill country select list
    let countryOptions = countryList.map(country => ({ label: country.name, value: country.name.toLowerCase() }));
    this.setState({ countryOptions });

    if (this.state.requestID) {

      try {
        this.props.helper.handleWaiting();
        const result = await axios.get(`${this.props.helper.baseUrl}/Restaurants/${this.state.requestID}`, {
          params: {
            filter: {
              "where": { "status": 1 },
            },
            access_token: this.props.helper.token
          }
        });
        if (result.status == 200) {
          this.populateForm(result.data)
        } else {
          throw (result.status);
        }
      } catch (error) {
        this.props.helper.handleError(error);
      }
      
    }


  }

  //address is a table of objects, each object contains an array of strings 
  // ===> search this array of strings for prop
  getPropertyFromAddress(address, prop) {
    return address.find(adressObj => {
      return adressObj.types.find(props => props === prop);
    });
  }


  debouncedSearch = _.debounce(() => {
    axios.get(`${this.props.helper.baseUrl}/Restaurants/search`, {
      params: {
        query: this.state.name,
        access_token: this.props.helper.token
      }
    }).then(res => {
      if (res.status === 200) {
        let restaurantOptions = res.data.map(rest => ({ name: rest.name, id: rest.id }));
        this.setState({ restaurantOptions, isSearchLoading: false, fetchedRestaurants: res.data })
      } else {
        throw (res.status);
      }
    }).catch(error => { 
      if(error.response.data.error.code === "ERROR_GOOGLE_SEARCH_DETAILS"){
        this.setState({ restaurantOptions: [], isSearchLoading: false, fetchedRestaurants: [] }, ()=>console.log(this.state))
      }else{
        this.props.helper.handleError(error); 
      }
      
    });
  }, 800);

  handleSearch(option) {

    //when selecting restaurant from autocomplete show user data (read-only)
    if (Array.isArray(option) && option.length !== 0 && !option[0].customOption) {
      let id = option[0].id;
      let restauranData = this.state.fetchedRestaurants.find(r => r.id === id);
      let { address_components, price, location } = restauranData;
      let cityAddress = this.getPropertyFromAddress(address_components, "locality");
      let postalCodeAddress = this.getPropertyFromAddress(address_components, "postal_code");
      let imgPath = "";
      if( restauranData.photos && restauranData.photos[0] ){
        imgPath = `https://maps.googleapis.com/maps/api/place/photo?photoreference=${restauranData.photos[0].photo_reference}&key=${this.props.helper.googlePlaceKey}&maxheight=500`
      }

      this.setState({
        googleRestaurant: restauranData,
        fromGoogle: true,
        phone: parseInt(restauranData.formatted_phone_number.replace(/\s/gi, "")) || '',
        url: restauranData.website || '',
        city: cityAddress && cityAddress.long_name || '',
        selectedCountry: { label: location.country, value: location.country },
        postalCode: postalCodeAddress && postalCodeAddress.long_name || '',
        lng: location.lng,
        lat: location.lat,
        img: imgPath,
        address: restauranData.formatted_address || '',
        selectedCategory: { label: price && price.tier && "$".repeat(price.tier) }
      });
    } else {
      this.setState({
        fromGoogle: false,
        googleRestaurant: {},
        phone: '',
        city: '',
        selectedCountry: '',
        postalCode: '',
        lng: '',
        lat: '',
        img: '',
        address: '',
        selectedCategory: '',
        url: ''
      })
    }

    let name = option;
    if (Array.isArray(option) && option.length !== 0) {
      name = option[0].name;
    }

    if(name.length > 2){
      this.setState({
        name,
        isSearchLoading: true
      }, this.debouncedSearch);
    }
    
  }

  getDateFromTime(time) {
    let date = new Date();
      var index = time.indexOf(":"); // replace with ":" for differently displayed time.
      var index2 = time.indexOf(" ");
  
      var hours = time.substring(0, index);
      var minutes = time.substring(index + 1, index2);
  
      var mer = time.substring(index2 + 1, time.length);
      /* if (mer == "PM"){
          hours = hours + 12;
      } */
  
  
      date.setHours(hours);
      date.setMinutes(minutes);
      date.setSeconds("00");
  
      return date;
  }

  handleWorkingDay(e, day, toDelete) {
    e.stopPropagation();
    let sel = this.state.selectedWorkingDays;

    if (sel[day]) {
      //delete sel[day];
    } else {
      sel[day] = { "openAt": "", "closeAt": "" }
    }


    if (sel[day] && toDelete) {
      delete sel[day];
    }


    this.setState({ selectedWorkingDays: sel, lastSelectedDay: day });
  }

  handleWorkingHours(name, value) {
    let workingDays = this.state.selectedWorkingDays;

    try {
      workingDays[this.state.lastSelectedDay][name] = value.toDate();

    } catch (e) {
      let error = {}
      error.response = {};
      error.response.data = {};
      error.response.data.error = {};
      error.response.data.error.message = "Veuillez sélectionner une date";
      this.props.helper.handleError(error);
    }
    this.setState({ selectedWorkingDays: workingDays });

  }

  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    const baseObjectName = name.split('[')[0].split('.')[0];
    const baseObject = _.get(this.state, baseObjectName);
    const partToModify = name.replace(baseObjectName, '');

    if (partToModify) {
      _.set(baseObject, partToModify, value);
      this.setState({ [baseObjectName]: baseObject });
    } else {
      this.setState({ [baseObjectName]: value });
    }
  }

  async handleSubmit(e) {
    e.preventDefault();
   



    this.setState({ isLoading: true });
    this.props.helper.handleWaiting();
    // Upload images
    const _this = this;
    const promises = Object.keys(_this.state.image).map(async function (objectKey, key) {
      if (_this.state.image[objectKey].data) {
        const imageData = new FormData();
        imageData.append('file', _this.state.image[objectKey].data);
        try {
          const image = await axios.post(`${_this.props.helper.baseUrl}/Containers/${this.props.helper.container}/upload`, imageData);
          const images = _this.state.image;
          images[objectKey].url = image.data.result.files.file[0].name;
          _this.setState({ image: images });
        } catch (error) {
          _this.props.helper.handleError(error);
        }
      }
    });

    await Promise.all(promises);

   

    //transform selectedWorkingDays object to match api model
    let timeframes = Object.keys(this.state.selectedWorkingDays).map(day => ({
      days: day.substring(0, 3),
      open: [
        { renderedTime: `${moment(this.state.selectedWorkingDays[day].openAt).format('HH:mm A')}-${moment(this.state.selectedWorkingDays[day].closeAt).format('HH:mm A')}` },
      ]
    }))
    let dayNames = this.state.workingHourDays;
    let periods = Object.keys(this.state.selectedWorkingDays).map(day => {
      return {
        "open": {
          "day": dayNames.findIndex(d => d === day),
          "time": `${moment(this.state.selectedWorkingDays[day].openAt).format('HHmm')}`
        },
        "close": {
          "day": dayNames.findIndex(d => d === day),
          "time": `${moment(this.state.selectedWorkingDays[day].closeAt).format('HHmm')}`
        }
      }
      
    });
    const weekday_text = Object.keys(this.state.selectedWorkingDays).map(day => {
      return `${day}: ${moment(this.state.selectedWorkingDays[day].openAt).format('HH:mm A')} - ${moment(this.state.selectedWorkingDays[day].closeAt).format('HH:mm A')}`;
    });
    

    let request = {};

    if (!this.state.fromGoogle) {
      

      request = {
        "name": this.state.name,
        "email": this.state.email,
        "website": this.state.url,

        "formatted_address": this.state.address + " " + this.state.postalCode + " " + this.state.city + " " + this.state.selectedCountry.label,

        "price": {
          "tier": this.state.selectedCategory,
          "currency": "$",
          "from": "admin"
        },

        "about": this.state.about,

        "location": {
          "address": this.state.address,
          "lat": this.state.lat,
          "lng": this.state.lng,
          "country": this.state.selectedCountry.label,
          "state": this.state.city,
          "postal_code": this.state.postalCode
        },

        "formatted_phone_number":  String(this.state.phone).replace(/(.{2})/g, "$1 "),
        
    

        "geometry": {
          "location": {
            "lat": this.state.lat,
            "lng": this.state.lng
          }
        },

        "opening_hours": {
          "periods": periods,
          "weekday_text": weekday_text
        }

      };
      if(Object.keys(this.state.image).length > 0){
          request['photos'] = [];
          Object.keys(this.state.image).map((prop, idx) => {
            request['photos'].push({ "photo_reference" : this.state.image[prop].url });
          });
      }
      
    } else {
      //delete foursquare obj id to post it 
      request = this.state.googleRestaurant;
      //delete this properties to make google model as amira suggested
      delete request['id'];
      delete request['position'];
      delete request['hasPromo'];
      delete request['formbase'];
      delete request['favorited'];
      
    }

        const requestURL = this.state.requestID ?
          `${this.props.helper.baseUrl}/Restaurants/${this.state.requestID}?access_token=${this.props.helper.token}` :
          `${this.props.helper.baseUrl}/Restaurants?access_token=${this.props.helper.token}`;
    
        try {
          await (this.state.requestID ? axios.patch(requestURL, request) : axios.post(requestURL, request));
          this.props.helper.setAlert(null);
          this.props.history.push('/restaurant')
        } catch (error) {
          this.setState({ isLoading: false });
          this.props.helper.handleError(error);
        }
  }





  populateForm(prop) {
    console.log('%cprop', 'color:red;', prop);
    debugger;
    prop = prop ? prop : {};
    let submitMessage = prop ? 'update' : 'submit';
    let { location, contact } = prop;
    let country = location && location.country && location.country.toLowerCase();
    //populate working hours
    if(prop.opening_hours && prop.opening_hours.weekday_text){

      let selectedWorkingDays = prop.opening_hours.weekday_text.map(frame => {
        //frames = "Monday: 05:00 AM - 05:00 AM"
      
        let dateTable = frame.split(': ');
        //dateTablr = ["Monday", "05:00 AM - 05:00 AM"]
        let days = dateTable[0];

        this.setState({lastSelectedDay: days});

        let openAt = this.getDateFromTime(dateTable[1].split(' - ')[0]);
        let closeAt = this.getDateFromTime(dateTable[1].split(' - ')[1]);
        let open =  {        
          openAt, closeAt
        };
        return {[days]: open};
      })
      .reduce((obj, item) => {
        return Object.assign(obj,item)
      }, {});

      this.setState({selectedWorkingDays});
    }

    this.setState({
      requestID: prop.id || '',
      submitMessage: submitMessage,
      isLoading: false,
      name: prop.name || '',
      email: prop.email || '',
      phone: parseInt(prop.formatted_phone_number.replace(/\D/gi,'')) || '',
      img: prop && prop.photos[0] && prop.photos[0].photo_reference || '',
      url: prop.website || '',
      address: location && location.address || '',
      lat: location && location.lat || '',
      lng: location && location.lng || '',
      postalCode: location && location.postal_code || '',
      city: location && location.state || '',
      about: prop.about || '',
      selectedCategory: prop.price && prop.price.tier || '',
      selectedCountry: this.state.countryOptions.find(c => c.value === country) || ''
    });

    this.props.helper.setAlert(null);
  }

  onFileChange(picture, ref) {
    const image = this.state.image;
    if (picture.length == 0) {
      delete image[ref];
    } else {
      const currentImage = {};
      currentImage.data = (picture && picture[0]) || null;
      currentImage.name = (picture && picture[0] && picture[0].name) || '';
      image[ref] = currentImage;
    }
    this.setState({ image: image });
  }

  render() {
    //to allow search in category select because parent category is passed in <b> tag
    const customFilterOption = (option, rawInput) => {
      const words = rawInput.split(' ');
      return words.reduce(
        (acc, cur) => {
          let text = "";
          if (option.label && option.label.props && option.label.props.children) {
            text = option.label.props.children;
          } else {
            text = option.label;
          }
          return acc && text.toLowerCase().includes(cur.toLowerCase())
        },
        true,
      );
    };
    const { isLoading, fromGoogle } = this.state;
    return (
      <div className="main-content">
        {this.state.alert}
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="New Restaurant"
                content={
                  <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>

                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Name
                      </ControlLabel>
                      <Col md={9}>
                        {this.state.requestID ?
                          (<FormControl
                            type="text"
                            placeholder=""
                            name="name"
                            value={this.state.name}
                            onChange={this.handleInputChange}
                          />) :
                          (<AsyncTypeahead
                            labelKey="name"
                            //multiple={multiple}
                            options={this.state.restaurantOptions}
                            allowNew
                            onChange={this.handleSearch}
                            newSelectionPrefix="Add new Restaurant: "
                            placeholder="Search or add a new Restaurant"
                            minLength={2}
                            value={this.state.name}
                            onSearch={this.handleSearch}
                            isLoading={this.state.isSearchLoading}
                          />)}
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Email
                        </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="email"
                          placeholder="name@domain.com"
                          name="email"
                          value={this.state.email}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Phone
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="number"
                          placeholder="Phone"
                          name="phone"
                          value={this.state.phone}
                          onChange={this.handleInputChange}
                          min={0}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Website
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="domain.com"
                          name="url"
                          value={this.state.url}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Address
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="1 Rue 1 Tunis"
                          name="address"
                          value={this.state.address}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Latitude
                      </ControlLabel>
                      <Col md={3}>
                        <FormControl
                          placeholder="40.7484405"
                          type="text"
                          name="lat"
                          value={this.state.lat}
                          onChange={this.handleInputChange}
                          min={0}
                          disabled={fromGoogle}
                        />
                      </Col>
                      <ControlLabel className="col-md-3">
                        Longitude
                      </ControlLabel>
                      <Col md={3}>
                        <FormControl
                          placeholder="40.7484405"
                          type="text"
                          name="lng"
                          value={this.state.lng}
                          onChange={this.handleInputChange}
                          min={0}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Postal Code
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="EH6 8SA"
                          name="postalCode"
                          value={this.state.postalCode}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        City
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          type="text"
                          placeholder="City"
                          name="city"
                          value={this.state.city}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Country
                      </ControlLabel>
                      <Col md={9}>
                        <Select
                          name="country"
                          value={this.state.selectedCountry}
                          options={this.state.countryOptions}
                          onChange={(v) => { this.setState({ selectedCountry: v || '' }) }}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        About
                      </ControlLabel>
                      <Col md={9}>
                        <FormControl
                          componentClass="textarea"
                          placeholder="About"
                          name="about"
                          value={this.state.about}
                          onChange={this.handleInputChange}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>
                    {!this.state.fromGoogle &&
                      <FormGroup>
                        <ControlLabel className="col-md-2">
                          Working Hours:
                      </ControlLabel>
                        <Col md={9}>
                          <ButtonGroup>
                            {
                              this.state.workingHourDays.map((day, i) => (
                                <Button bsStyle={this.state.selectedWorkingDays[day] && "primary"} fill key={i} onClick={(e) => this.handleWorkingDay(e, day)} style={this.state.selectedWorkingDays[day] && { "paddingRight": "0" }}>
                                  <span style={{ "marginRight": "5px" }}>{day}</span>

                                  {this.state.selectedWorkingDays[day] &&
                                    <OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip">Delete Day</Tooltip>}>

                                      <i className="fa fa-times text-danger text-right" onClick={(e) => this.handleWorkingDay(e, day, true)}></i>

                                    </OverlayTrigger>
                                  }
                                </Button>
                              ))
                            }

                          </ButtonGroup>
                        </Col>
                      </FormGroup>
                    }

                    {this.state.selectedWorkingDays[this.state.lastSelectedDay] &&
                      <React.Fragment>
                        <FormGroup>
                          <ControlLabel className="col-md-2">
                            Open at*
                      </ControlLabel>
                          <Col md={3}>
                            <Datetime
                              utc
                              closeOnSelect
                              dateFormat={false}
                              inputProps={{ placeholder: "9:00 AM", required:true}}
                              name="openAt"
                              value={this.state.selectedWorkingDays[this.state.lastSelectedDay].openAt}
                              onChange={(value) => { this.handleWorkingHours("openAt", value) }}
                            />
                          </Col>
                          <ControlLabel className="col-md-2">
                            Close at*
                      </ControlLabel>
                          <Col md={3}>
                            <Datetime
                              utc
                              closeOnSelect
                              dateFormat={false}
                              inputProps={{ placeholder: "16:00 PM", required:true}}
                              name="closeAt"
                              value={this.state.selectedWorkingDays[this.state.lastSelectedDay].closeAt}
                              onChange={(value) => { this.handleWorkingHours("closeAt", value) }}
                              disabled={fromGoogle}
                            />
                          </Col>
                        </FormGroup>
                        <br />
                      </React.Fragment>
                    }
                   {/*  <FormGroup>
                      <ControlLabel className="col-md-2">
                        Specialty
                      </ControlLabel>
                      <Col md={9}>
                        <Select
                          filterOption={customFilterOption}
                          name="specialty"
                          value={this.state.selectedSpecialty}
                          options={this.state.specialtiesOptions}
                          onChange={(v) => { this.setState({ selectedSpecialty: v && v.value || '' }) }}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup> */}


                    <FormGroup>
                      <ControlLabel className="col-md-2">
                        Category
                      </ControlLabel>
                      <Col md={9}>
                        <Select
                          name="selectedCategory"
                          value={this.state.selectedCategory}
                          options={[
                            { label: "$", value: 1 },
                            { label: "$$", value: 2 },
                            { label: "$$$", value: 3 },
                            { label: "$$$$", value: 4 }
                            //this.state.categoriesOptions
                          ]}
                          onChange={(v) => { this.setState({ selectedCategory: v && v.value || '' }) }}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <ControlLabel className="col-md-2">Logo</ControlLabel>
                      <Col md={9}>
                        <ImageUploader
                          label=""
                          withIcon={true}
                          withPreview={true}
                          buttonText='Choose images'
                          maxFileSize={5242880}
                          singleImage={true}
                          defaultImage={this.state.img && this.props.helper.baseUrl + '/Containers/${this.props.helper.container}/download/' + this.state.img}
                          onChange={(picture) => this.onFileChange(picture, "photo")}
                          disabled={fromGoogle}
                        />
                      </Col>
                    </FormGroup>

                    <FormGroup>
                      <Col md={10} mdOffset={2}>
                        <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                          {isLoading ? 'processing...' : this.state.submitMessage}
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

const UpdatedComponent = withRouter(NewRestaurant);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedComponent {...props} helper={Helper} />}
  </AppContext.Consumer>
);
