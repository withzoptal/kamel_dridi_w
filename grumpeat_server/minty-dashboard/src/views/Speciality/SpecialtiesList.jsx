import React, { Component } from 'react';
import { Grid, Row, Col, InputGroup, Table, FormGroup, FormControl, Pagination, Form, ControlLabel, Button } from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from 'react-router-dom';
import SpecialtyRow from '../../components/Table/SpecialtyRow';

class SpecialtiesList extends Component {
    constructor(prop) {
        super(prop);

        this.state = {
            isLoading: false,
            alert: null,
            submitMessage: 'Submit',
            requestID: '',
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['Nom', 'Actions'],
            "dataRows": [],
            searchKeyWord: '',
            name: ''
            
        };

        this.deleteItem = this.deleteItem.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getCount = this.getCount.bind(this);

        if(this.props.match.params.id && (this.props.match.params.id !== ':id')){
            this.state.requestID = this.props.match.params.id;
          }
          console.log(this.props);
    }

    componentWillMount() {
        this.getItems();
        this.getCount();
    }


    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    deleteItem(id){
        const itemID = this.state.dataRows[id].id;

        axios.delete(`${this.props.helper.baseUrl}/Specialties/${itemID}?access_token=${this.props.helper.token}`,/* {"status": 0}*/)
            .then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                updatedData.splice(id, 1);
                this.setState({dataRows: updatedData});
                this.props.helper.setAlert(null);
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );
    }

    getCount(){
        /*
        axios.get(`${this.props.helper.baseUrl}/Specialties/count`, {
            params: {
                //"where" : {"status": 1},
                access_token: this.props.helper.token
            }
        }).then(res => {
            if (res.status === 200) {
                const pageCount = Math.ceil(res.data.count/this.state.limit);
                this.setState({count: pageCount});
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );
    */ }

    getItems(prop=1){
       /* this.props.helper.handleWaiting();
        axios.get(`${this.props.helper.baseUrl}/Specialties`, {
            params: {
                filter: {
                    // "where": {"or": [{"firstName": {"like": `${prop}.*`, "options": "i" }}, {"familytName": {"like": `${prop}.*`, "options": "i" }}]},
                    "limit": this.state.limit,
                    "skip": ((prop-1) * this.state.limit),
                    //"where" : {"status": 1}
                },
                access_token: this.props.helper.token
            }
        }).then(res => {
            if (res.status === 200) {
                this.setState({dataRows: res.data});
                this.setState({active: prop});
                this.props.helper.setAlert(null);
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );*/
    }

    

    render() {
        const { isLoading } = this.state;
        return (
            <div className="main-content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                
                                tableFullWidth
                                content={
                                    <Table responsive className="table-bigboy">
                                        <thead>
                                        <tr>
                                            <th>{this.state.rowTitle[0]}</th>
                                            <th>{this.state.rowTitle[1]}</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>{this.state.rowTitle[0]}</th>
                                            <th>{this.state.rowTitle[1]}</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        {
                                            this.state.dataRows.map((prop, key) => {
                                                return (
                                                    <SpecialtyRow item={prop} itemID={key} key={key} deleteItem={this.deleteItem} />
                                                )
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}
const UpdatedComponent = withRouter(SpecialtiesList);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedComponent {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
