import React, { Component } from 'react';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Form, ButtonGroup, Button as Buttong, Image, boot } from 'react-bootstrap';
import 'react-select/dist/react-select.css';
import Button from '../../components/CustomButton/CustomButton.jsx';
import Card from '../../components/Card/Card.jsx';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import { AppContext } from "../../containers/AppContext";
var _ = require('lodash');


export class NewSpecialty extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            alert: null,
            submitMessage: 'Submit',
            requestID: '',
            name: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.populateForm = this.populateForm.bind(this);

        if (this.props.match.params.id && (this.props.match.params.id !== ':id')) {
            this.state.requestID = this.props.match.params.id;
        }
        console.log('props', this.props);
    }

    async componentWillMount() {
        if (this.state.requestID) {
            axios.get(`${this.props.helper.baseUrl}/Specialties/${this.state.requestID}`, {
                params: {
                    access_token: this.props.helper.token
                }
            }).then(result => {
                if (result.status == 200) {
                    this.populateForm(result.data.data)
                } else {
                    throw (result.status);
                }
            }).catch(error => {
                this.props.helper.handleError(error);
            });
        }
    }



    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }


    async handleSubmit(e) {

        e.preventDefault();
        this.setState({ isLoading: true });

        let request = {
            "name": this.state.name,
        };
        
        


        const requestURL = this.state.requestID ?
            `${this.props.helper.baseUrl}/Specialties/${this.state.requestID}?access_token=${this.props.helper.token}` :
            `${this.props.helper.baseUrl}/Specialties?access_token=${this.props.helper.token}`;

        try {
            await (this.state.requestID ? axios.patch(requestURL, request) : axios.post(requestURL, request));
            this.props.helper.setAlert(null);
            this.setState({ isLoading: false });
            this.props.history.push('/quality')
        } catch (error) {
            this.setState({ isLoading: false });
            this.props.helper.handleError(error);
        }
    }

   

    populateForm(prop) {
        prop = prop ? prop : {};
        console.log('populatefoem', prop)
        let submitMessage = prop ? 'update' : 'submit';
        this.setState({
            requestID: prop.id || '',
            submitMessage: submitMessage,
            isLoading: false,
            name: prop.name || '',
        }, () => console.log(this.state.image));

        this.props.helper.setAlert(null);

    }



    render() {
        const { isLoading } = this.state;
        return (
            <div className="main-content">
                {this.state.alert}
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="New Specialty"
                                content={
                                    <Form horizontal onSubmit={!isLoading ? this.handleSubmit : null}>
                                        <FormGroup>
                                            <ControlLabel className="col-md-3">
                                                Name
                                            </ControlLabel>
                                            <Col md={8}>
                                                <FormControl
                                                    type="text"
                                                    placeholder="nom"
                                                    name="name"
                                                    value={this.state.name}
                                                    onChange={this.handleInputChange}
                                                    required
                                                />
                                            </Col>
                                        </FormGroup>
                                        <FormGroup>
                                            <Col md={10} mdOffset={2}>
                                                <Button bsStyle="info" disabled={isLoading} type="submit" fill>
                                                    {isLoading ? 'processing...' : this.state.submitMessage}
                                                </Button>
                                            </Col>
                                        </FormGroup>
                                    </Form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

const UpdatedComponent = withRouter(NewSpecialty);
export default props => (
    <AppContext.Consumer>
        {Helper => <UpdatedComponent {...props} helper={Helper} />}
    </AppContext.Consumer>
);