import React, { Component } from 'react';
import { Grid, Row, Jumbotron } from 'react-bootstrap';

class Dashboard extends Component{
  render(){
    return (
      <div className="main-content">
        <Grid fluid>
          <Row>
            <Jumbotron>
              <h1>Welcome to Grumpeat's Dashboard</h1>
              <p>
                This is the Headquarter of the Grumpeat app.<br/>
                In here you can controle your app content.<br/>
                Click on the menu on left side of the screen to begin navigating in the dashboard.<br/>
                Enjoy
              </p>
            </Jumbotron>
          </Row>
          <Row>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
