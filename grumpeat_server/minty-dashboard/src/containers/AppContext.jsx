import React from 'react';

export const AppContext = React.createContext({
  appName: 'Grumpeat',
  baseUrl: 'http://vps556422.ovh.net/api',
  token: sessionStorage.getItem('grumpeat_jwt') || '',
  role: '',
  alert: null,
  googlePlaceKey: "AIzaSyCLtOx4tx1-E0kyi5XqVEVDLJKmNwKS3sI",
  setAlert: (param) => {},
  handleError: (error) => {},
  handleWaiting: () => {},
  handleCorrectResponse: () => {},
  handleLogin: (param) => {},
  handleLogout: () => {},
});
