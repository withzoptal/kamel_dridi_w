import React, { Component } from 'react';
import {withRouter} from "react-router-dom";
import {AppContext} from "../../containers/AppContext";

class Footer extends Component {
    render(){
        return (
            <footer className={"footer" + (this.props.transparent !== undefined ? " footer-transparent":"")}>
                <div className={"container" + (this.props.fluid !== undefined ? "-fluid":"")}>
                    <p className="copyright pull-right">
                        &copy; {1900 + (new Date()).getYear()} <a href={this.props.helper.baseUrl.replace('/api', '')}>Grumpeat Dashboard</a>, made with <i className="fa fa-heart heart"></i> by <a href="http://www.mintit.io">Mint IT</a>
                    </p>
                </div>
            </footer>
        );
    }
}

const UpdatedComponent = withRouter(Footer);
export default props => (
  <AppContext.Consumer>
    { Helper => <UpdatedComponent {...props} helper={Helper} /> }
  </AppContext.Consumer>
);
