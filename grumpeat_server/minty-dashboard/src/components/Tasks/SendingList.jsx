import React, { Component } from 'react';
import { Tooltip, OverlayTrigger, Table, Grid, Row, Pagination, Col } from 'react-bootstrap';

import Checkbox from '../CustomCheckbox/CustomCheckbox.jsx';
import Card from '../../components/Card/Card.jsx';
import Button from '../CustomButton/CustomButton.jsx';

export class SendingList extends Component{
  render(){
    return (
      <Grid fluid>
        <Row>
          <Col md={12}>
            <Card
              content={
                <Table className="table">
                  <tbody>
                  <tr key={0}>
                    <td>
                      <Checkbox
                        number="sendAll"
                        checked={this.props.sendAllSelected}
                        onClick={this.props.handleCheckBox}
                      />
                    </td>
                    <td>Select all</td>
                  </tr>
                  {
                    Object.keys(this.props.data).map((prop) => {
                      return (
                        <tr key={prop}>
                          <td>
                            <Checkbox
                              number={`sending${prop}`}
                              checked={this.props.data[prop].selected}
                              onClick={this.props.handleCheckBox}
                            />
                          </td>
                          <td>{`${this.props.data[prop].firstName} ${this.props.data[prop].lastName}`}</td>
                        </tr>
                      )
                    })
                  }
                  </tbody>
                </Table>
              }
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default SendingList;
