import React, { Component } from 'react';
import { Table, Grid, Row, Pagination, Col } from 'react-bootstrap';
import Checkbox from '../CustomCheckbox/CustomCheckbox.jsx';
import Card from '../../components/Card/Card.jsx';

export class Selectable extends Component{
  render(){
    return (
      <Grid fluid>
        <Row>
          <Col md={12}>
            <Card
              content={
                <Table className="table">
                  <tbody>
                  <tr key={0}>
                    <td>
                      <Checkbox
                        number="selectAll"
                        checked={this.props.searchAllSelected}
                        onClick={this.props.handleCheckBox}
                      />
                    </td>
                    <td>Select all</td>
                  </tr>
                  {
                    Object.keys(this.props.data).map((prop) => {
                      return (
                        <tr key={prop}>
                          <td>
                            <Checkbox
                              number={`selectable${prop}`}
                              checked={this.props.data[prop].selected}
                              onClick={this.props.handleCheckBox}
                            />
                          </td>
                          <td>{`${this.props.data[prop].firstName} ${this.props.data[prop].lastName}`}</td>
                        </tr>
                      )
                    })
                  }
                  </tbody>
                </Table>
              }
              legend={
                <Pagination
                first
                next
                prev
                last
                ellipsis
                boundaryLinks
                maxButtons={5}
                items={this.props.count}
                activePage={this.props.active}
                onSelect={this.props.goToPage}
                />
              }
            />
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Selectable;
