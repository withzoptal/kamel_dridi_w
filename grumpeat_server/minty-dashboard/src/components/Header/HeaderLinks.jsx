import React, { Component } from 'react';
import {Nav, NavDropdown, MenuItem} from 'react-bootstrap';
import axios from 'axios';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from "react-router-dom";


class HeaderLinks extends Component{
    constructor(props){
      super(props);

      this.disconnect = this.disconnect.bind(this);
    }
    disconnect(){
      axios.post(`${this.props.helper.baseUrl}/Profils/logout?access_token=${this.props.helper.token}` )
          .then(res => {
            this.props.helper.handleLogout();
            this.props.history.push("/pages/login-page");
          });
    }
    render(){
        return(
            <div>
                <Nav pullRight>
                    <NavDropdown
                        eventKey={4}
                        title={(
                            <div>
                                <i className="fa fa-list"></i>
                                <p className="hidden-md hidden-lg">
                                    More
                                    <b className="caret"></b>
                                </p>
                            </div>
                        )} noCaret id="basic-nav-dropdown-3" bsClass="dropdown-with-icons dropdown">
                        <MenuItem eventKey={4.5} onSelect={this.disconnect}><div className="text-danger"><i className="pe-7s-close-circle"></i> Log out</div></MenuItem>
                    </NavDropdown>
                </Nav>
            </div>
        );
    }
}

const UpdatedComponent = withRouter(HeaderLinks);
export default props => (
  <AppContext.Consumer>
    { Helper => <UpdatedComponent {...props} helper={Helper} /> }
  </AppContext.Consumer>
);


