import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import SweetAlert from 'react-bootstrap-sweetalert';
import {Tooltip, OverlayTrigger} from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';


class RestaurantRow extends Component {

  constructor(prop){
    super(prop);

    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleDelete(){
    this.props.newAlert(
      <SweetAlert
        danger
        showCancel
        title="Confirmation"
        cancelBtnBsStyle="default"
        confirmBtnBsStyle="danger"
        style={{display: "block",marginTop: "-100px"}}
        onConfirm={ () => this.props.deleteItem(this.props.itemID) }
        onCancel={ () => this.props.hideAlert() }
      >
        Are you sure to delete this restaurant ?
      </SweetAlert>
    );
  }

  handleEdit(){
    const id = this.props.item.id;
    this.props.history.push(`/restaurant/new/${id}`);
  }


  handleSelect(e){
    e.preventDefault();
    this.props.selectItem(this.props.itemID);
  }

  render() {

    const editPost = (
      <Tooltip id="edit">Edit Restaurant</Tooltip>
  );
    const removePost = (
        <Tooltip id="remove">Delete Restaurant</Tooltip>
    );

    const actionsPost = (
      <td className="td-actions text-right">
      { this.props.item && !this.props.item.googleId &&
          <OverlayTrigger placement="left" overlay={editPost}>
              <Button onClick={this.handleEdit} simple icon bsStyle="warning">
                  <i className="fa fa-edit"></i>
              </Button>
          </OverlayTrigger>
      }
          <OverlayTrigger placement="left" overlay={removePost}>
              <Button onClick={this.handleDelete} simple icon bsStyle="danger">
                  <i className="fa fa-times"></i>
              </Button>
          </OverlayTrigger>
      </td>
  );

    return (
      <tr>
        <td><a href="#" onClick={this.handleSelect} >{this.props.item.name}</a></td>
        <td>{this.props.item.formatted_phone_number}</td>
        {actionsPost}
        {/* <td className="text-right">
        { this.props.item && !this.props.item.googleId &&
          <a className="btn btn-simple btn-warning btn-icon edit" onClick={this.handleEdit}>
            <i className="fa fa-edit"></i>
          </a>
        }
          <a className="btn btn-simple btn-danger btn-icon remove" onClick={this.handleDelete}>
            <i className="fa fa-times"></i>
          </a>
        </td>*/}
      </tr> 
    );
  }
}

export default withRouter(RestaurantRow);
