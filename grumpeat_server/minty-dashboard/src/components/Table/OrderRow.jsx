import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from "react-router-dom";
import {OverlayTrigger, Tooltip, Label, Image } from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';
import moment from "moment"; 



class OrderRow extends Component {
    constructor(prop){
        super(prop);

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
    handleDelete(){
        this.props.helper.setAlert(
            <SweetAlert
                danger
                showCancel
                title="Confirmation"
                cancelBtnBsStyle="default"
                confirmBtnBsStyle="danger"
                style={{display: "block",marginTop: "-100px"}}
                onConfirm={ () => this.props.deleteItem(this.props.itemID) }
                onCancel={ () => this.props.helper.setAlert(null) }
            >
                Are you sure to delete this order?
            </SweetAlert>
        );
    }

    handleEdit(){
        const id = this.props.item.id;
        this.props.history.push(`/quality/new/${id}`);
    }

    render() {
        const editPost = (
            <Tooltip id="edit">Edit Order</Tooltip>
        );
        const removePost = (
            <Tooltip id="remove">Remove Order</Tooltip>
        );
        const actionsPost = (
            <td className="td-actions text-right">
                
                <OverlayTrigger placement="left" overlay={removePost}>
                    <Button onClick={this.handleDelete} simple icon bsStyle="danger">
                        <i className="fa fa-times"></i>
                    </Button>
                </OverlayTrigger>
            </td>
        );
        return (
            <tr>
                
                <td>{this.props.item.profil.firstName} {this.props.item.profil.lastName}</td>
                <td>{this.props.item.promotion.name}</td>
                <td>{moment(this.props.item.date).format("DD/MM/YYYY")}</td>
                <td>{this.props.item.code}</td>
                <td>{this.props.item.nbPersonne}</td>
                <td>{this.props.item.status}</td>
                {actionsPost}
            </tr>
        );
    }
}

const UpdatedRow = withRouter(OrderRow);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedRow {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
