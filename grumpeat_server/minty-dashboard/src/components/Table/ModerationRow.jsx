import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from "react-router-dom";
import {OverlayTrigger, Tooltip, Label } from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';
import moment from 'moment';



class ModerationRow extends Component {
    constructor(prop){
        super(prop);

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
    handleDelete(){
        this.props.handlePopUp(this.props.itemID);
        /* this.props.helper.setAlert(
            <SweetAlert
                danger
                showCancel
                title="Confirmation"
                cancelBtnBsStyle="default"
                confirmBtnBsStyle="danger"
                style={{display: "block",marginTop: "-100px"}}
                onConfirm={ () => this.props.deleteItem(this.props.itemID) }
                onCancel={ () => this.props.helper.setAlert(null) }
            >
                Vous êtes sur de vouloir supprimer ?
            </SweetAlert>
        ); */
        
    }

    handleEdit(){
        this.props.handlePopUp(this.props.itemID);
    }

    render() {
        const editPost = (
            <Tooltip id="edit">Change price </Tooltip>
        );
        const removePost = (
            <Tooltip id="remove">Supprimer </Tooltip>
        );
        const actionsPost = (
            <td className="td-actions text-right">
                <OverlayTrigger placement="left" overlay={editPost}>
                    <Button onClick={this.handleEdit} simple icon bsStyle="warning">
                        <i className="fa fa-edit"></i>
                    </Button>
                </OverlayTrigger>
                
            </td>
        );

        const currency = this.props.item.price.currency || '$';
        console.log('item', this.props.item);
        const from = this.props.item.price.from && this.props.item.price.from.toLowerCase() || "";
        const bstyle = from === 'user'    ? 'warning':
                       from === 'default' ? 'info' : '';
        return (
            <tr>
                
                <td>{this.props.item.name}</td>
                <td>{this.props.item.formatted_address}</td>
                <td>{moment(this.props.item.createdAt).format('MM/DD/YYYY')}</td>
                <td>{currency.repeat( this.props.item.price.tier )}</td>
                <td>{<Label bsStyle={bstyle}>{this.props.item.price.from}</Label>}</td>
                {actionsPost}
            </tr>
        );
    }
}

const UpdatedRow = withRouter(ModerationRow);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedRow {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
