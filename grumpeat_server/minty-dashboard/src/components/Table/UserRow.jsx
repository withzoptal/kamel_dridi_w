import React, {Component} from 'react';
import {withRouter} from "react-router-dom";
import {AppContext} from "../../containers/AppContext";
import SweetAlert from "react-bootstrap-sweetalert";
import {Tooltip, OverlayTrigger} from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';

class OfferRow extends Component {
    constructor(prop){
        super(prop);

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
    handleDelete(){
        this.props.helper.setAlert(
            <SweetAlert
                danger
                showCancel
                title="Confirmation"
                cancelBtnBsStyle="default"
                confirmBtnBsStyle="danger"
                style={{display: "block",marginTop: "-100px"}}
                onConfirm={ () => this.props.deleteItem(this.props.itemID) }
                onCancel={ () => this.props.helper.setAlert(null) }
            >
                Are you sure to delete this user ?
            </SweetAlert>
        );
    }

    handleEdit(){
        const id = this.props.item.id;
        this.props.history.push(`/users/new/${id}`);
    }


    render() {

        const editPost = (
            <Tooltip id="edit">Edit User</Tooltip>
        );
        const removePost = (
            <Tooltip id="remove">Delete User</Tooltip>
        );
        const actionsPost = (
            <td className="td-actions text-right">
                <OverlayTrigger placement="left" overlay={editPost}>
                    <Button onClick={this.handleEdit} simple icon bsStyle="warning">
                        <i className="fa fa-edit"></i>
                    </Button>
                </OverlayTrigger>
                <OverlayTrigger placement="left" overlay={removePost}>
                    <Button onClick={this.handleDelete} simple icon bsStyle="danger">
                        <i className="fa fa-times"></i>
                    </Button>
                </OverlayTrigger>
            </td>
        );

        return (
            <tr>
                <td>{this.props.item.firstName + ' ' + this.props.item.lastName}</td>
                <td>{this.props.item.email}</td>
                <td>{this.props.favorite.length}</td>
                <td>{this.props.followers.length}</td>
                <td>{this.props.following.length}</td>
                {actionsPost}
            </tr>
        );
    }
}

const UpdatedRow = withRouter(OfferRow);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedRow {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
