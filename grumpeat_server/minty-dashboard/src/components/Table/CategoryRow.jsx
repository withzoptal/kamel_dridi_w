import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { AppContext } from '../../containers/AppContext';
import { withRouter } from 'react-router-dom';
import { OverlayTrigger, Tooltip, Label, Image } from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';

class CategoryRow extends Component {
  constructor(prop) {
    super(prop);

    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
  handleDelete() {
    this.props.helper.setAlert(
      <SweetAlert
        danger
        showCancel
        title='Confirmation'
        cancelBtnBsStyle='default'
        confirmBtnBsStyle='danger'
        style={{ display: 'block', marginTop: '-100px' }}
        onConfirm={() => this.props.deleteItem(this.props.itemID)}
        onCancel={() => this.props.helper.setAlert(null)}>
        Are you sure to delete this quality?
      </SweetAlert>
    );
  }

  handleEdit() {
    const id = this.props.item.id;
    this.props.history.push(`/category/new/${id}`);
  }

  render() {
    const editPost = <Tooltip id='edit'>Edit Category</Tooltip>;
    const removePost = <Tooltip id='remove'>Remove Category</Tooltip>;
    const actionsPost = (
      <td
        className='td-actions text-right'
        style={{ width: '20%', boxSizing: 'border-box' }}>
        <OverlayTrigger placement='left' overlay={editPost}>
          <Button onClick={this.handleEdit} simple icon bsStyle='warning'>
            <i className='fa fa-edit'></i>
          </Button>
        </OverlayTrigger>
        <OverlayTrigger placement='left' overlay={removePost}>
          <Button onClick={this.handleDelete} simple icon bsStyle='danger'>
            <i className='fa fa-times'></i>
          </Button>
        </OverlayTrigger>
      </td>
    );

    return (
      <tr>
        {/* <td style={{"width": "5%", "boxSizing": "border-box"}}>{this.props.itemID + 1}</td>
                <td className="td-name" style={{"width": "80%", "boxSizing": "border-box"}}>{this.props.item.name}</td> */}
      {actionsPost}
      </tr>
    );
  }
}

const UpdatedRow = withRouter(CategoryRow);
export default props => (
  <AppContext.Consumer>
    {Helper => <UpdatedRow {...props} helper={Helper} />}
  </AppContext.Consumer>
);
