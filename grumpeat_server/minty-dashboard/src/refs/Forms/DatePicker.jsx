import React, {Component} from 'react';
import Datetime from 'react-datetime';
// DatePicker class to select dates only (no time)
class DatePicker extends Component {
    constructor(props){
        super(props);

        this.state = {
            //default date: newDate or moment object
            startDate: ''
        }
    }

    //callback when date is selected
    handleDateInputChange(name, prop){
        if(prop){
            this.setState({ [name]: prop.toDate()});
        }else{
            this.setState({ [name]: null});
        }
    }

    //this.setState({startDate: prop.startDate ? moment(prop.startDate).toDate() : ''});
    render() {
        return (

                //timeFormat={false} to prevent the component from selecting time and only select dates

                    <Datetime
                        utc
                        closeOnSelect
                        timeFormat={false}
                        inputProps={{placeholder:"Select the starting date"}}
                        name="startDate"
                        value={this.state.startDate}
                        onChange={(value) => {this.handleDateInputChange("startDate", value)}}
                    />
        );
    }
}

export default DatePicker;