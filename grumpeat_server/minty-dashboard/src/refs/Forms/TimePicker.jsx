import React, {Component} from 'react';
import{  Col, FormGroup, ControlLabel } from 'react-bootstrap';
import Datetime from 'react-datetime';
// TimePicker class to select time only (no date)
class TimePicker extends Component {
    constructor(props){
        super(props);

        this.state = {
            startTime: ''
        }
    }


    handleDateInputChange(name, prop){
        if(prop){
            this.setState({ [name]: prop.toDate()});
        }else{
            this.setState({ [name]: null});
        }
    }

    //this.setState({startDate: prop.startDate ? moment(prop.startDate).toDate() : ''});
    render() {
        return (
            <FormGroup>
                <ControlLabel className="col-md-2">
                    Time
                </ControlLabel>
                <Col md={9}>
                    {
                        //dateFormat={false} to prevent the component from selecting Dates and only select Time}
                    }
                    <Datetime
                        utc
                        closeOnSelect
                        dateFormat={false}
                        inputProps={{placeholder:"Select the starting date"}}
                        name="startTime"
                        value={this.state.startTime}
                        onChange={(value) => {this.handleDateInputChange("startTime", value)}}
                    />
                </Col>
            </FormGroup>
        );
    }
}

export default TimePicker;