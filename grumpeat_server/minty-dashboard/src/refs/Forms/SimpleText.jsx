import React, {Component} from "react";
import {Col, ControlLabel, FormControl, FormGroup} from "react-bootstrap";



class SimpleText extends Component{
    render() {
        return (
            <FormGroup>
                <ControlLabel className="col-md-2">
                    Enter Text
                </ControlLabel>
                <Col md={9}>
                    <FormControl
                        type="text"
                        placeholder="title"
                        name="title"
                        value={this.state.title}
                        onChange={this.handleInputChange}
                        required="true"
                    />
                </Col>
            </FormGroup>
        );
    }
}

export default SimpleText;