import React, {Component} from 'react';
import {Col, ControlLabel, FormGroup} from "react-bootstrap";
import ImageUploader from "react-images-upload";

class ImageUpload extends Component{
    constructor(props){
        super(props);
        this.state = {
            pictures: [],
            image: {},
        }

        this.onFileChange = this.onFileChange.bind(this);

    }

    //triggers when user selects an image
    onFileChange(picture, ref) {
        const images = {...this.state.image};
        if(picture.length == 0){
            delete images[ref];
        }else{
            const image = [];
            picture.map( (val, key) => {
                const currentImage = {};
                currentImage.data = val || null;
                currentImage.name = (val && val.name) || '';
                image.push(currentImage);
            });

            images[ref] = image;
        }
        this.setState({image: images});
    }



    render () {
        return (
            <FormGroup>
                <ControlLabel className="col-md-2">Photo </ControlLabel>
                <Col md={9}>
                    <ImageUploader
                        label="mdpi 286px/448px"
                        withIcon={true}
                        withPreview={true}
                        buttonText='Choose images'
                        maxFileSize={5242880}
                        defaultImage={this.state.pictures && this.state.pictures[0] && this.props.helper.baseUrl+`/Containers/${this.props.helper.container}/download/`+this.state.pictures[0]}
                        onChange={(picture) => this.onFileChange(picture, "pictures")}
                    />
                </Col>
            </FormGroup>
        );
    }
}

export default ImageUpload;