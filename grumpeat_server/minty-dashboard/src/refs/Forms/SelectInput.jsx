import React, {Component} from "react";
import {Col, ControlLabel, FormGroup} from "react-bootstrap";



class SimpleText extends Component{
    render() {
        return (
            <FormGroup>
                <ControlLabel className="col-md-2">
                    Choose option
                </ControlLabel>
                <Col md={9}>
                    <Select
                        value={this.state.typeId}
                        options={this.state.typeOptions}
                        onChange={(value) => this.setState({ typeId: value && value.value})}
                        required
                    />
                </Col>
            </FormGroup>
        );
    }
}

export default SimpleText;