import {Component} from "react";
import {Col, FormControl, Grid, Pagination, Row} from "react-bootstrap";
import Card from "../../components/Card/Card";
import SimpleRow from "./Rows/SimpleRow";
import React from "react";

class SearchableTable extends Component{
    constructor (props) {
        super(props);

        this.state = {
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['id', 'Name', 'Age'],
            "dataRows": [{id:"0",name:'Tim',age:27},{id:"1",name:'Sarah',age:30}, {id:"2",name:'Josh',age:5}],
            searchKeyWord: '',
            results : [{id:"0",name:'Tim',age:27},{id:"1",name:'Sarah',age:30}, {id:"3",name:'Josh',age:5}]
        };

        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearch(event) {
        const name  = event.target.name;
        const value = event.target.value;
        const results = this.state.dataRows.filter(item =>  (String(item[name])).search(RegExp(value,'i')) !== -1);
        this.setState({results});
        console.log(results)
    }

    render() {
        return (
            <div>
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                content={
                                    <div className="fresh-datatables">
                                        <table id="datatables" ref="main" className="table table-striped table-no-bordered table-hover"
                                               cellSpacing="0" width="100%" style={{width: "100%"}}>
                                            <thead>
                                            <tr>
                                                <th>
                                                    {this.state.rowTitle[0]}
                                                    <FormControl
                                                        type="text"
                                                        placeholder="ID"
                                                        name="id"
                                                        onChange={this.handleSearch}
                                                        required="true"
                                                    />
                                                </th>
                                                <th>
                                                    {this.state.rowTitle[1]}
                                                    <FormControl
                                                        type="text"
                                                        placeholder="NAME"
                                                        name="name"
                                                        onChange={this.handleSearch}
                                                        required="true"
                                                    />
                                                </th>
                                                <th>
                                                    {this.state.rowTitle[2]}
                                                    <FormControl
                                                        type="text"
                                                        placeholder="AGE"
                                                        name="age"
                                                        onChange={this.handleSearch}
                                                        required="true"
                                                    />
                                                </th>

                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>{this.state.rowTitle[0]}</th>
                                                <th>{this.state.rowTitle[1]}</th>
                                                <th>{this.state.rowTitle[2]}</th>
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            {
                                                this.state.results.map((prop, key) => {
                                                    return (
                                                        <SimpleRow item={prop} key={key}/>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
export default SearchableTable;