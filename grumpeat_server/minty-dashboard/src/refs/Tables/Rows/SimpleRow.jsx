import React, {Component} from 'react';

class SimpleRow extends Component{
    render(){
        return (
            <tr>
                <td>{this.props.item.id}</td>
                <td>{this.props.item.name}</td>
                <td>{this.props.item.age}</td>
            </tr>
        );
    }
}

export default SimpleRow;