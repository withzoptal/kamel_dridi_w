import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from "react-router-dom";

class EditRow extends Component {
    constructor(prop){
        super(prop);

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
    handleDelete(){
        this.props.helper.setAlert(
            <SweetAlert
                danger
                showCancel
                title="Confirmation"
                cancelBtnBsStyle="default"
                confirmBtnBsStyle="danger"
                style={{display: "block",marginTop: "-100px"}}
                onConfirm={ () => this.props.deleteItem(this.props.itemID) }
                onCancel={ () => this.props.helper.setAlert(null) }
            >
                Are you sure to delete this user ?
            </SweetAlert>
        );
    }

    handleEdit(){
        const id = this.props.item.id;
        this.props.history.push(`/color/new/${id}`);
    }

    render() {
        let role = this.props.item.roles || {};
        role = role[0] || {};
        return (
            <tr>
                <td>{this.props.item.name}</td>
                <td>{this.props.item.code}</td>
                <td className="text-right">
                    <a className="btn btn-simple btn-warning btn-icon edit" onClick={this.handleEdit}>
                        <i className="fa fa-edit"></i>
                    </a>
                    <a className="btn btn-simple btn-danger btn-icon remove" onClick={this.handleDelete}>
                        <i className="fa fa-times"></i>
                    </a>
                </td>
            </tr>
        );
    }
}

const UpdatedRow = withRouter(EditRow);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedRow {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
