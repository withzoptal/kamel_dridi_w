import React, { Component } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import {AppContext} from "../../containers/AppContext";
import {withRouter} from "react-router-dom";
import {OverlayTrigger, Tooltip, Label, Image } from 'react-bootstrap';
import Button from '../../components/CustomButton/CustomButton';



class BigRow extends Component {
    constructor(prop){
        super(prop);

        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    //eliminate this shit, why the fuck anybody want to put sweet alert everywhere ?!!
    handleDelete(){
        this.props.helper.setAlert(
            <SweetAlert
                danger
                showCancel
                title="Confirmation"
                cancelBtnBsStyle="default"
                confirmBtnBsStyle="danger"
                style={{display: "block",marginTop: "-100px"}}
                onConfirm={ () => this.props.deleteItem(this.props.itemID) }
                onCancel={ () => this.props.helper.setAlert(null) }
            >
                Vous êtes sur de vouloir supprimer ?
            </SweetAlert>
        );
    }

    handleEdit(){
        const id = this.props.item.id;
        this.props.history.push(`/category/new/${id}`);
    }

    render() {
        const editPost = (
            <Tooltip id="edit">Editer </Tooltip>
        );
        const removePost = (
            <Tooltip id="remove">Supprimer </Tooltip>
        );
        const actionsPost = (
            <td className="td-actions">
                <OverlayTrigger placement="left" overlay={editPost}>
                    <Button onClick={this.handleEdit} simple icon bsStyle="success">
                        <i className="fa fa-edit"></i>
                    </Button>
                </OverlayTrigger>
                <OverlayTrigger placement="left" overlay={removePost}>
                    <Button onClick={this.handleDelete} simple icon bsStyle="danger">
                        <i className="fa fa-times"></i>
                    </Button>
                </OverlayTrigger>
            </td>
        );
        return (
            <tr>
                <td>
                    <div className="img-container">
                        <Image src={this.props.item.pictures && this.props.item.pictures[0] && `${this.props.helper.baseUrl}/Containers/${this.props.helper.container}/download/${this.props.item.pictures[0]}`}/>
                    </div>
                </td>
                <td className="td-name">{this.props.item.name}</td>
                {actionsPost}
            </tr>
        );
    }
}

const UpdatedRow = withRouter(BigRow);
export default props => (
    <AppContext.Consumer>
        { Helper => <UpdatedRow {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
