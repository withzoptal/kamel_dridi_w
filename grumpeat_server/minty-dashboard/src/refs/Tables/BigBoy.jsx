import React, { Component } from 'react';
import { Grid, Row, Col, InputGroup, Table, FormGroup, FormControl, Pagination } from 'react-bootstrap';
import axios from 'axios';
import Card from '../../components/Card/Card.jsx';
import BigRow from './Rows/BigRow';
import {AppContext} from "../../containers/AppContext";

class BigBoy extends Component {
    constructor(prop) {
        super(prop);

        this.state = {
            limit: 30,
            count: 0,
            active: 1,
            "rowTitle": ['PhotoRow', 'Row2', 'ActionsRow'],
            "dataRows": [],
            searchKeyWord: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getCount = this.getCount.bind(this);
    }

    componentWillMount() {
        this.getItems();
        this.getCount();
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    deleteItem(id){
        const itemID = this.state.dataRows[id].id;

        axios.delete(`${this.props.helper.baseUrl}/Types/${itemID}?access_token=${this.props.helper.token}`).then(res => {
            if (res.status === 200) {
                let updatedData = this.state.dataRows;
                updatedData.splice(id, 1);
                this.setState({dataRows: updatedData});
                this.props.helper.setAlert(null);
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );
    }

    getCount(){
        axios.get(`${this.props.helper.baseUrl}/Types/count`, {
            params: {
                //"where": {"username": {"like": `${this.state.searchKeyWord}.*`, "options": "i" }},
                access_token: this.props.helper.token
            }
        }).then(res => {
            if (res.status === 200) {
                const pageCount = Math.ceil(res.data.data.count/this.state.limit);
                this.setState({count: pageCount});
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );
    }

    getItems(prop=1){
        this.props.helper.handleWaiting();
        axios.get(`${this.props.helper.baseUrl}/Types`, {
            params: {
                filter: {
                    // "where": {"or": [{"firstName": {"like": `${prop}.*`, "options": "i" }}, {"familytName": {"like": `${prop}.*`, "options": "i" }}]},
                    "limit": this.state.limit,
                    "skip": ((prop-1) * this.state.limit)
                },
                access_token: this.props.helper.token
            }
        }).then(res => {
            if (res.status === 200) {
                this.setState({dataRows: res.data.data});
                this.setState({active: prop});
                this.props.helper.setAlert(null);
            } else {
                throw(res.status);
            }
        }).catch( error => { this.props.helper.handleError(error); } );
    }

    render() {
        return (
            <div>
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                tableFullWidth
                                content={
                                    <Table responsive className="table-bigboy">
                                        <thead>
                                        <tr>
                                            <th>{this.state.rowTitle[0]}</th>
                                            <th>{this.state.rowTitle[1]}</th>
                                            <th>{this.state.rowTitle[2]}</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>{this.state.rowTitle[0]}</th>
                                            <th>{this.state.rowTitle[1]}</th>
                                            <th>{this.state.rowTitle[2]}</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        {
                                            this.state.dataRows.map((prop, key) => {
                                                return (
                                                    <BigRow item={prop} itemID={key} key={key} deleteItem={this.deleteItem}/>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </Table>
                                }
                                legend={
                                    <Pagination
                                        first
                                        next
                                        prev
                                        last
                                        ellipsis
                                        boundaryLinks
                                        maxButtons={10}
                                        items={this.state.count}
                                        activePage={this.state.active}
                                        onSelect={this.getItems}
                                    />
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}

export default props => (
    <AppContext.Consumer>
        { Helper => <BigBoy {...props} helper={Helper} /> }
    </AppContext.Consumer>
);
