'use strict';
const path = require('path');
const os = require('os');
const fs = require('fs');
const winston = require('winston');
winston.emitErrs = true;
const hostname = os.hostname();

/**
 * Create Log Directory if not exist
 */
const logDir = path.join(__dirname, '../../../', 'logs');
if (!fs.existsSync(logDir)) {
  try {
    fs.mkdirSync(logDir);
  } catch (e) {
    console.log(e);
  }
}
const logDirAPI = `${logDir}/apiServer`;
if (!fs.existsSync(logDirAPI)) {
  try {
    fs.mkdirSync(logDirAPI);
  } catch (e) {
    console.log(e);
  }
}
const logDirDbServer = `${logDir}/dbServer`;
if (!fs.existsSync(logDirDbServer)) {
  try {
    fs.mkdirSync(logDirDbServer);
  } catch (e) {
    console.log(e);
  }
}
const logDirPaymentServer = `${logDir}/paymentServer`;
if (!fs.existsSync(logDirPaymentServer)) {
  try {
    fs.mkdirSync(logDirPaymentServer);
  } catch (e) {
    console.log(e);
  }
}

/**
 * Define API Server Logs Transporters
 */
const apiServerErrorTransporters = new winston.transports.File({
  name: 'apiServerError',
  level: 'error',
  silent: false,
  timestamp: true,
  filename: `${logDirAPI}/${hostname}-error.log`,
  maxsize: 2097152, // 2 MB
  json: true,
  zippedArchive: false,
});

/**
 * Define DB Server Logs Transporters
 */
const dbServerErrorTransporters = new winston.transports.File({
  name: 'dbServerError',
  level: 'error',
  silent: false,
  timestamp: true,
  filename: `${logDirDbServer}/${hostname}-error.log`,
  maxsize: 2097152, // 2 MB
  json: true,
  zippedArchive: false,
});

/**
 * Define Payment Server Logs Transporters
 */
const paymentServerErrorTransporters = new winston.transports.File({
  name: 'soapServerError',
  level: 'error',
  silent: false,
  timestamp: true,
  filename: `${logDirPaymentServer}/${hostname}-error.log`,
  maxsize: 2097152, // 2 MB
  json: true,
  zippedArchive: false,
});

winston.loggers.add('apiServer', {
  transports: [
    apiServerErrorTransporters,
  ],
});

winston.loggers.add('dbServer', {
  transports: [
    dbServerErrorTransporters,
  ],
});

winston.loggers.add('paymentServer', {
  transports: [
    paymentServerErrorTransporters,
  ],
});

exports.default = {
  apiLogger: winston.loggers.get('apiServer'),
  dbLogger: winston.loggers.get('dbServer'),
  paymentLogger: winston.loggers.get('paymentServer'),
};
