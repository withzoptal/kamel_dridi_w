const Logger = require('../../server/logger').default;

module.exports = function (app) {

    const Profil = app.models.Profil;


    Profil.specialCreate =  function(data,next){ 
        var q = data.firstName+"."+data.lastName;
        q = q.replace(/ /g, "");
        var pattern = new RegExp('.*'+q+'.*', "i");
        Profil.find({where:{username:{like: pattern}}},async (err,pr)=>{
            if(err){Logger.dbLogger.error(err); next(err);}
            if((data.username === undefined) || (data.username === "")){
                data.username = q+String(pr.length + 1);
            }
            Profil.create(data, (errorCreate, result) => { 
                if(errorCreate){Logger.dbLogger.error(errorCreate); next(errorCreate);}
                Profil.login(data,'user',  function(errorLogin, token){ 
                    if(errorLogin){Logger.dbLogger.error(errorLogin); next(errorLogin);}
                    next(null, token)
                }); 
            }); 
        });
    };
 
    Profil.remoteMethod('specialCreate',{
        description: 'Create a new instance and login',
        accepts: [ 
            {arg: 'data', type: 'Object', required: true, 'http': {source: 'body'},default:Profil} 
        ],
        returns: {type: 'object',  http: {source: 'body'},root:true},
        http: {verb: 'Post', path: '/specialCreate'}
    });

};  