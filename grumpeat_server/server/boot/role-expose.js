module.exports = function (app){
    const _ = require('lodash');
    app.remotes().phases
        .addBefore('invoke', 'options-form-request')
        .use(function(ctx, next){ 
            if (ctx.args.options && ctx.args.options.accessToken) {     
                app.models.Role.getRoles({
                    principalType: app.models.RoleMapping.USER,
                    principalId: ctx.args.options.accessToken.userId
                },{returnOnlyRoleNames: true}, 
                function (roleErr, roles) {  
                    if (roleErr) {
                        return next(roleErr); 
                    } 
                    roles = _.filter(roles, function (role) {
                        return typeof role === 'string'
                    });
                    ctx.args.options.isSuper = _.includes(roles, 'superadmin');
                    ctx.args.options.isAdmin = _.includes(roles, 'superadmin'); 
                    ctx.args.options.isClient = _.includes(roles, 'superadmin');
                    ctx.args.options.isAdmin = _.includes(roles, 'admin');
                    ctx.args.options.isClient = _.includes(roles, 'client');
                    next();
                }) 
            }else{
               return next();
            }
        })
}