const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');
const config = require('../config.json');
const _ = require('lodash');
const Logger = require('../../server/logger').default;

module.exports = function (app) {

    const Profil = app.models.Profil;
    
    /** Valider le compte par le code  */
    // Profil.codeActivation = function(code, options, next){ 
    //     if(options.accessToken.userId){
    //         Profil.findById(options.accessToken.userId, (error, profil) => { 
    //             if(error){Logger.dbLogger.error(error);return next(error);}
    //             if(profil){
    //                 if(profil.codeValidation == code){
    //                     profil.updateAttributes({'emailVerified':true}, function(err,ctx){
    //                         if(err){
    //                             Logger.dbLogger.error(err); return next(err);
    //                         }else{
    //                             next();
    //                         }
    //                     })
    //                 }else{  
    //                     next(null,{'error':'code de validation incorrecte'});
    //                 }
    //             }
    //         });
    //     }
    // };

    // Profil.remoteMethod('codeActivation',{
    //     description: "Email verification via code ",
    //     accepts : [
    //         {arg: 'code', type: 'string', required: true},
    //         {arg: 'options', type: 'object', http: 'optionsFromRequest'} 
    //     ],
    //     returns: {type: 'object', root: true},
    //     http: {verb: 'post', path: '/codeActivation'}
    // });

    /** send verification email after registration */
    // Profil.afterRemote('specialCreate', function SendCodeValidation(context, userInstance, next) {  
    //     console.log('> user.afterRemote triggered');
    //         Profil.getRoles(userInstance.userId, (error, roles) => { 
    //             if(error){Logger.dbLogger.error(error);next(error);}
    //             if(roles.isClient){
    //                 Profil.findById(userInstance.userId, (error, profil) => { 
    //                     if(error){Logger.dbLogger.error(error);next(error);}
    //                     const mailSubject = 'Code de validation';
    //                     let templatePath, htmlMailCodeValidation, mailTemplate, mailHtml;
    //                     templatePath = path.join(__dirname, '..', 'templates', 'code-validation-mail.html'); 
    //                     htmlMailCodeValidation = fs.readFileSync(templatePath, 'utf8'); 
    //                     mailTemplate = Handlebars.compile(htmlMailCodeValidation);
    //                     const codeValidation = Math.random().toString(36).slice(-6);
    //                     profil.updateAttributes({codeValidation: codeValidation}, function (error, profil) { 
    //                         if(error){Logger.dbLogger.error(error);next(error);}
    //                         mailHtml = mailTemplate({ 
    //                                         firstName: profil.firstName,
    //                                         lastName: profil.lastName,
    //                                         codeValidation: codeValidation
    //                                     });
    //                         Profil.app.models.Email.send({
    //                                 to: profil.email,
    //                                 from: config.mail.from,
    //                                 subject: mailSubject,
    //                                 html: mailHtml
    //                             }, function (err) {
    //                                 if (err) {
    //                                     Logger.dbLogger.error(err);
    //                                     console.log('> error sending code validation email');
    //                                     next(err);
    //                                 } else {
    //                                     console.log('> sending code validation email to:', profil.email);
    //                                     next();
    //                                 }
    //                         });
    //                     }); 
    //                 });
    //             }else{
    //                 next();
    //             }
    //         }); 
    // });
}