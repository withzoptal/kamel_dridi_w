const _ = require('lodash');
const request = require('request');
const crypto = require('crypto');
const g = require('strong-globalize')();
// const Logger = require('../../server/logger').default;
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var xhr = new XMLHttpRequest();
var document = require("min-document");


module.exports = function (app) {
  const Profil = app.models.Profil;
  const Container = app.models.Container;

  Profil.remoteMethod('facebookConnect',
    {
      description: 'Connect user with Facebook AccessToken',
      accepts: [
        {arg: 'accessToken', type: 'string', required: true}
      ],
      returns: {
        type: 'object', root: true
      },
      http: {verb: 'post', path: '/login/facebook'}
    }
  );
 
  // Login par facebook
  Profil.facebookConnect = function (accessToken, fn) {
    const fields = ["locale", "name", "picture.type(large)", "email", "gender","link"]; // [,"user_friends", "link", "locale", "name", "picture", "verified", "email"]
    const graphApiUrl = 'https://graph.facebook.com/v2.10/me?fields=' + fields.join(',');
    request.get({url: graphApiUrl, qs: {access_token: accessToken}, json: true}, function (err, response, profile) { console.log(profile);
      if (response.statusCode !== 200) {
        const errorFB = new Error(g.f(profile.error.message));
        errorFB.statusCode = 500;
        errorFB.code = 'ERROR';
        errorFB.message = "Malformed access token ";
        return fn(errorFB); 
      }  
      var newNameFile = new Date().getTime()+'_'+'grumpeat_'+ profile.id +'.jpg';
      var fs = require('fs'), request = require('request');
      request.get({url: profile.picture.data.url,json:true}, function(error, response, file){
        var tof = Object();
        tof.container = 'grumpeat'; tof.name = newNameFile; tof.type = response.headers['content-type'];
        tof.originalFilename = newNameFile; tof.size = response.headers['content-length']; 
        tof.field = 'file'; 
        profile.avatar = tof.name;
        var photo = profile.picture.data.url;
        const credentials = {accessToken: accessToken};
      app.models.UserIdentity.findOne({
        where: {
          provider: 'facebook', 
          externalId: profile.id
        }
      }, async (err, identity) => {
        if (err) {
          return fn(err);
        } else {
          if (identity) { 
            identity.credentials = credentials;
            return identity.updateAttributes({
              profile: profile,
              credentials: credentials,
              modified: new Date()
            }, function (err, identity) {
              if (err) {
                fn(err);
              } else { 
                return identity.user(function (err, user) { 
                  if (err) {
                    fn(err);
                  } else {
                    user.accessTokens.create({
                      created: new Date(), 
                      ttl: 31556926
                    }, function (err, token) {
                      if (err) {  
                        return fn(err);
                      } else {
                        app.models.Profil.findById(user.id, { 
                          include: ['roles'] 
                        }, function (error, dataUser) {
                          if (error) {
                            fn(error);
                          } else {  
                            dataUser = dataUser.toJSON(); 
                            user = user.toJSON();
                            try {
                              const role = _.first(dataUser.roles);
                              user.role = role.name;
                            } catch (e) {
                              user.role = 'client';
                            }
                            token = token.toJSON();
                            token.user = user;
                            fn(null,token);
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        }
        var email = ""; 
        var validemail = true;
        if(profile.email){
            email = profile.email;
            validemail = true;
        }else{
            email = profile.id + '@facebook.com';
            validemail = false;
        }
        // const gender = profile.gender === 'male' ? 'm' : 'f';
        var username = profile.id; 
        const hmac = crypto.createHmac('sha1', username);
        const buf = crypto.randomBytes(32);
        hmac.update(buf);
        const password = hmac.digest('hex');
        const fullName = profile.name;
        const fullNameList = fullName.split(' ');
        var res1 = fullNameList[0].toLowerCase();
        var res2 = fullNameList[1].toLowerCase();
        var q = res1+"."+res2;
        q = q.replace(/ /g, "");
        var pattern = new RegExp('.*'+q+'.*', "i");
        var pr = await Profil.find({where:{username:{like: pattern}}});
        if(pr.length > 0){
          username = q+String(pr.length + 1);
        }else{
          username = q
        }
        const userObj = {
          firstName: fullNameList[0] || '',
          lastName: fullNameList[1] || '',
          username: username,
          fb: profile.link,
          password: password,
          email: email,
          registrationId: '', 
          emailVerified: true,
          avatar : profile.avatar,
          validemail : validemail,
          fFB: true
        };
        let query;
        if (userObj.email) { 
          query = {
            or: [
              {email: userObj.email}
            ]
          };
        } else if (userObj.email) {
          query = {email: userObj.email};
        }
        app.models.Profil.findOrCreate({where: query}, userObj, function (err, user) {  
          if (err) { 
            return fn(err);
          } else { 
            app.models.UserIdentity.findOrCreate({where: {externalId: profile.id}}, {
              provider: 'facebook',
              externalId: profile.id,
              authScheme: 'oAuth 2.0',
              profile: profile,
              credentials: credentials,
              userId: user.id,
              created: new Date(),
              modified: new Date()
            }, function (err, identity) {
              if (err) {
                fn(err); 
              } else { 
                user.accessTokens.create({
                  created: new Date(),
                  ttl: 31556926
                }, function (err, token) {
                  if (err) {
                    fn(err);
                  } else {
                    app.models.Profil.findById(user.id, {
                      include: ['roles'] 
                    }, function (error, dataUser) {
                      if (error) {
                        fn(error);
                      } else {
                        dataUser = dataUser.toJSON();
                        user = user.toJSON();
                        try {
                          const role = _.first(dataUser.roles);
                          user.role = role.name;
                        } catch (e) { 
                          user.role = 'client';
                        }
                        token = token.toJSON();
                        token.user = user;
                        fn(null,token); 
                      } 
                    });
                  }
                });
              }
            });
          }
        });
      });
      }).pipe(Container.uploadStream('grumpeat', newNameFile));
  });
  };

};
