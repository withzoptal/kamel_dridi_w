const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');
const config = require('../config.json');
const Logger = require('../../server/logger').default;

module.exports = function (app) {

  const Profil = app.models.Profil;

  // send password when requested
  Profil.on('resetPasswordRequest', function (info) {
    const userId = info.user.id || false;
    if (userId) {
      Profil.findById(userId, (error, profil) => {
        if(error){Logger.dbLogger.error(error);}
        if (!error) {
          const mailSubject = 'Reset Password';
          let templatePath, htmlMailResetPassword, mailTemplate, mailHtml;
          Profil.getRoles(userId, (error, roles) => { 
            if(error){Logger.dbLogger.error(error);}
            if (!error) {
              templatePath = path.join(__dirname, '..', 'templates', 'user-reset-password.html');
              htmlMailResetPassword = fs.readFileSync(templatePath, 'utf8');
              mailTemplate = Handlebars.compile(htmlMailResetPassword);
              const newPassword = Math.random().toString(36).slice(-8);
              profil.updateAttributes({password: newPassword}, function (error, profil) {
                if(error){Logger.dbLogger.error(error);}
                if (!error) {
                  mailHtml = mailTemplate({
                    firstName: profil.firstName,
                    lastName: profil.lastName,
                    newPassword: newPassword
                  });
                  Profil.app.models.Email.send({
                    to: profil.email,
                    from: config.mail.from,
                    subject: mailSubject,
                    html: mailHtml
                  }, function (err) {
                    if (err) {
                      Logger.dbLogger.error(err);
                      console.log('> error sending password reset email2');
                    } else {
                      // console.log(info);
                      console.log('> sending password reset email to:', info.email);
                    }
                  }); 
                }
              });
            }
          });
        }
      });
    }
  });

};
