module.exports = function(app) {
    var remotes = app.remotes();

    remotes.after('**', function (ctx, next) {
        var start = process.hrtime();
        var st = ctx.methodString;
        console.log(st)
        ctx.res.once('finish', function() {
            var diff = process.hrtime(start);
            var ms = diff[0] * 1e3 + diff[1] * 1e-6;
            console.log('The request processing time is %d ms.', ms);
            // console.log( ms);
        });  
        next();
    });

};
