module.exports = function (app) {
    var _ = require('lodash');
    var Profil = app.models.Profil,
        Role = app.models.Role,
        RoleMapping = app.models.RoleMapping;

    // Creation des roles 
    Role.findOrCreate({where:{name:'superadmin'}},{name:'superadmin'});
    Role.findOrCreate({where:{name:'admin'}},{name:'admin'});
    Role.findOrCreate({where:{name:'client'}},{name:'client'});

    // creation super admin
    Profil.findOrCreate({ where: { email: 'superadmin@mint-grumpeat.it'  } }, 
        { username: 'superadmin', email: 'superadmin@mint-grumpeat.it', password: 'mint2015@', role: 'superadmin' }, 
        function(err, user) {
            if (err) throw err;
            //Creation de super rôle
            Role.findOrCreate({where:{name:'superadmin'}},{name:'superadmin'}, function(err, role) {
                if (err) throw err;
                // Associer le role  
                RoleMapping.find({ filter: { where: { roleId: role.id, principalId: user.id } }},
                    function(err, principals) {
                        if (err) {
                            return console.error(err);    
                        }
                        if (principals.length == 0) { 
                            role.principals.create({
                                    principalType: RoleMapping.USER,  
                                    principalId: user.id
                                }, function(err, principal) {
                                    if (err) throw err;
                                    // console.log(principal);
                                    // console.log(role.name);
                            });
                        }
                    }
                );
            });
    });

    // creation des profils de test
    Profil.findOrCreate({ where: { email: 'admin@test.it'  } }, 
        { username: 'admin', email: 'admin@test.it', password: '0000', role: 'admin' }, 
        function(err, user) {
            if (err) throw err;
            //Creation de super rôle
            Role.findOrCreate({where:{name:'admin'}},{name:'admin'}, function(err, role) {
                if (err) throw err;
                // Associer le role  
                RoleMapping.find({ filter: { where: { roleId: role.id, principalId: user.id } }}, 
                    function(err, principals) {
                        if (err) {
                            return console.error(err);    
                        }
                        if (principals.length == 0) { 
                            role.principals.create({
                                    principalType: RoleMapping.USER,  
                                    principalId: user.id
                                }, function(err, principal) {
                                    if (err) throw err;
                                    console.log(principal);
                                    console.log(role.name);
                            });
                        }
                    }
                );
            });
    });


}