'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const moment = require('moment')
module.exports = function(Promotion) {

    /** findpromotions by date  */
    Promotion.getPromtionByDate = (date, restaurantId, next)=> {
        var d = (moment(date).format('dddd')).toLowerCase(); 
        switch (d){
            case 'sunday': d = 0; break;
            case 'monday': d = 1; break;
            case 'tuesday': d = 2; break;
            case 'wednesday': d = 3; break;
            case 'thursday': d = 4; break;
            case 'friday': d = 5; break;
            case 'saturday': d = 6; break;
        } 
        Promotion.find({where:{status:1, restaurantId:restaurantId,start:{lte:new Date(date)},end:{gte:new Date(date)},"times.day":d}},(err,promos)=>{ 
            if(err){Logger.dbLogger.error(err); next(err);} 
            var times = [];
            for(var i = 0;i < promos.length; i++){
                var t = promos[i].times; 
                for(var j=0;j < t.length;j++){
                    if(t[j].day === d){
                    var f = t[j].frames;
                    for(var a=0;a < f.length;a++ ){
                        f[a].promotionId = promos[i].id;
                    }
                    times.push(t[j].frames)}
                }
            }
            var result = [];
            for(var k = 0;k < times.length; k++){
                for(var l = 0;l < times[k].length; l++){
                    result.push(times[k][l])
                }
            }
            next(null, result)
        });
    };

    Promotion.remoteMethod('getPromtionByDate',{
        description: 'list request to follow & update add fav friends ##',
        accepts: [
                    { arg: 'date', type: 'date', required: true},
                    { arg: 'restaurantId', type: 'string', required: true}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/getPromtionByDate'}
    });

    /** get promotion by near  */
    Promotion.getNearPromtion = (location, options, next) => {  
        if(options && options.accessToken && options.accessToken.userId){
            const RestFav = app.models.RestFav;
            if(location){  
                Promotion.find({"where":{"status":1},"include":{"relation":"restaurant","scope":{"where":{"position": {"near":location,"maxDistance":30,"unit":"kilometers"}}}}}, async (err,promos)=>{
                    if(err){Logger.dbLogger.error(err); next(err);}
                    var resultat = new Array();
                    for(var i =0;i < promos.length;i++){
                        promos[i] = promos[i].toJSON();
                        if(promos[i].restaurant){
                            resultat.push(promos[i]);
                        }
                    }
                    for(var i=0 ; i < resultat.length ; i++){
                        var res = resultat[i];
                        if(res.restaurant){
                            var fav = await RestFav.find({where:{userId:options.accessToken.userId,restaurantId:res.restaurant.id}});
                            if(fav.length > 0 ){
                                res.restaurant.favorited = true;
                            }else{
                                res.restaurant.favorited = false;
                            }
                        }
                    }
                    next(null,resultat);
                });
            }else{ 
                Promotion.find({"where":{"status":1},"include":{"relation":"restaurant"}},(err,promos)=>{
                    if(err){Logger.dbLogger.error(err); next(err);}
                    next(null,promos);
                });
            }
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Promotion.remoteMethod('getNearPromtion',{
        description: 'list promotion get by near restaurant ##',
        accepts: [
            {arg: 'location',  type: 'string', required: false, description: 'localisation: lat,lng: 36.81133,10.18715'},
            {arg: 'options', type: 'object', http: 'optionsFromRequest'}
        ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/getNearPromtion'}
    });
};
