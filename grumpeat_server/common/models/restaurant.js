'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const axios = require('axios');
const config = require('../../server/config.json');
const _ = require('lodash');
const loopback = require('loopback');
const geopoint = require('geopoint')

module.exports = function(Restaurant) {
    
    /** add codePays for old restaurant in DB  */
    Restaurant.CodePaysUpdated = async () => {
        var code_pays = ["AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CW","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SX","SK","SI","SB","SO","ZA","GS","SS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW"];
        try{
            var restaurants = await Restaurant.find();
        }catch(err){Logger.dbLogger.error(err); return err;}
        for(var j = 0; j < restaurants.length;j++){ 
            var restaurant = restaurants[j].toJSON(); 
            if(restaurant && restaurant.address_components){
                for(var i = 0;i < restaurant.address_components.length;i++){ 
                    if(code_pays.indexOf(restaurant.address_components[i].short_name) > -1){
                        var codePays = restaurant.address_components[i].short_name;
                        try{
                            await restaurants[j].updateAttributes({codePays: codePays});
                        }catch(err){Logger.dbLogger.error(err); return err;}
                    }
                }
            }
        }
        return 'finished';
    };

    Restaurant.remoteMethod('CodePaysUpdated',{
        description: 'add codePays for old restaurant in DB ###', 
        accepts: [],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/CodePaysUpdated'} 
    });

    
    async function newRank (userId){
        const RestFav = app.models.RestFav;
        try{
            var res = await RestFav.find({where:{userId:userId}});
        }catch(err) { Logger.dbLogger.error(err); next(err); }
        var newRank = Number(res.length) + 1; 
        return newRank;
    }

    /** add multiple restaurant to user */
    Restaurant.addMultFavRest = async (data, options, next)=>{   
        if(options && options.accessToken && options.accessToken.userId){ 
            const RestFav = app.models.RestFav, RestQual = app.models.RestQual;
            var rest, listFav = new Array(); 
            for(var i = 0;i < data.length;i++){ 
                var dat = data[i]; 
                if(dat.restaurantId){ 
                    try{ 
                        var favR = await RestFav.find({where:{restaurantId: dat.restaurantId, userId: options.accessToken.userId}}); 
                    }catch( err ) { Logger.dbLogger.error(err); return err; }
                    if(favR.length === 0){ 
                        try{
                            var rank =  await newRank(String(options.accessToken.userId));
                            var newFav = await RestFav.create({restaurantId: dat.restaurantId , userId: options.accessToken.userId, rank: rank}); 
                            listFav.push(newFav.id);
                        }catch( err ) { Logger.dbLogger.error(err);return err; }
                            for(var j=0;j < dat.qualitie.length; j++){ 
                                try{
                                    await RestQual.create({qualityId:dat.qualitie[j],userId:options.accessToken.userId,restaurantId:dat.restaurantId});
                                }catch( err ) { Logger.dbLogger.error(err); return(err); }
                            }
                            /** update price if default => client  */
                            if(typeof dat.price === "number"){
                                try{
                                    rest =  await Restaurant.findById(dat.restaurantId);
                                    var p = {"tier":dat.price, "currency":"$", "from":"client"};
                                    rest.updateAttributes({price:p});
                                }catch(err){Logger.dbLogger.error(err); next(err); }
                            }
                        }else if(data.length === 1 && favR.length > 0){
                            const error = new Error("Restaurant is already in favorites list");
                            error.statusCode = 401;
                            error.code = 'RESTAURANT_IS_ALREADY_IN_FAVORITES_LIST'; 
                            next(error);
                        } 
                    } else{ 
                        if(typeof data[i].price === "number"){
                            let p = {"tier":dat.price, "currency":"$", "from":"client"};
                            data[i].price = p;
                        }
                        if(data[i].qualitie){
                            var qualitiesRest = data[i].qualitie;
                            delete data[i].qualitie;
                        }
                        try{
                            rest = await Restaurant.create(data[i]);
                        }catch(err){Logger.dbLogger.error(err); next(err);}
                        if(qualitiesRest){
                            for(let j=0;j < qualitiesRest.length; j++){
                                try{
                                    await RestQual.create({qualityId:qualitiesRest[j],userId:options.accessToken.userId,restaurantId:rest.id});
                                }catch( err ) { Logger.dbLogger.error(err); console.log(err); next(err);}
                            }
                        }
                        try{
                            let rank = await newRank(options.accessToken.userId);
                            let newFav = await RestFav.create({restaurantId: rest.id, userId: options.accessToken.userId, rank: rank});
                            listFav.push(newFav.id);
                        }catch( err ) { Logger.dbLogger.error(err); console.log(err); }
                    }
                }
                next(null,listFav);
            
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('addMultFavRest',{
        description: 'add multiple restaurant to list of favorite restaurant for connected user ###', 
        accepts: [  
                    {arg: 'data', type: 'array', required: true, http: { source: 'body' }},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/addMultFavRest'} 
    });

    /** add favorite restaurant to user */
    Restaurant.addFavRest = (restaurantId, qualitie, price, options, next)=>{
        const RestQual = app.models.RestQual;
        if(options && options.accessToken && options.accessToken.userId){ 
            const RestFav = app.models.RestFav;
            RestFav.find({where:{restaurantId: restaurantId, userId: options.accessToken.userId}}, async (err,favR) => { 
                if(err){Logger.dbLogger.error(err); next(err);}
                if(favR.length === 0){
                    var rank = await newRank(options.accessToken.userId);
                    RestFav.create({restaurantId: restaurantId, userId: options.accessToken.userId, rank: rank});
                    for(var i=0;i < qualitie.length; i++){
                        RestQual.create({qualityId:qualitie[i],userId:options.accessToken.userId,restaurantId:restaurantId});
                    } 
                    if(price){
                        try{
                            var rest = await Restaurant.findById(restaurantId);
                            var p = {"tier":price, "currency":"$", "from":"client"};
                            rest.updateAttributes({price:p});
                        }catch(err){Logger.dbLogger.error(err); next(err);}
                    }                    
                }
                next();
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('addFavRest',{
        description: 'add restaurant to list of favorite restaurant for connected user ###', 
        accepts: [  
                    {arg: 'restaurantId', type: 'string', required: true},
                    {arg: 'qualitie', type: 'array', required: true},
                    {arg: 'price', type: 'number', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/addFavRest'} 
    });

    /** delete restaurant from list of favorite for connected user */
    Restaurant.deleteFavRest = (restaurantId, options, next)=>{
        if(options && options.accessToken && options.accessToken.userId){
            const RestFav = app.models.RestFav, RestQual = app.models.RestQual;
            RestFav.destroyAll({restaurantId: restaurantId, userId: options.accessToken.userId});
            RestQual.destroyAll({restaurantId: restaurantId, userId: options.accessToken.userId});
            next();
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('deleteFavRest',{
        description: 'delete restaurant from list of favorite restaurant for connected user ###', 
        accepts: [  
                {arg: 'restaurantId', type: 'string', required: true},
                {arg: 'options', type: 'object', http: 'optionsFromRequest'}
            ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/deleteFavRest'} 
    });

    /** list favorite restaurant for connected user */
    Restaurant.favoriteRestaurant = async (userId, options)=>{    
        if(options && options.accessToken && options.accessToken.userId){
            const Profil = app.models.Profil, RestFav = app.models.RestFav, Follow = app.models.Follow;
            if(userId === String(options.accessToken.userId)){
                try{
                    var fav = await RestFav.find({where:{userId:options.accessToken.userId},include:"restaurant"});
                }catch(err){Logger.dbLogger.error(err); throw err;}
                for(var i = 0;i < fav.length;i++){
                    fav[i] = fav[i].toJSON(); 
                    if(fav[i].restaurant){
                        fav[i].restaurant.favorited = true;
                    }else{
                        fav.splice(i, 1);
                        i--;
                        continue;
                    }
                } 
                var result = {
                    list: fav,
                    nb: fav.length
                }
                return result;
            }else{
                try{
                    var p = await Profil.findById(String(userId)); 
                }catch(err){Logger.dbLogger.error(err); throw err;}
                try{
                    var rel = await Follow.findOne({where:{followerId:options.accessToken.userId, profilId:p.id, status:2}}); 
                }catch(err){Logger.dbLogger.error(err); throw err;}
                if(!p.privateAcc || rel){ 
                    try{
                        var rest = await RestFav.find({where:{userId:userId},include:{relation:"restaurant",scope:{include:"qualities"}}}); 
                    }catch(err){Logger.dbLogger.error(err); throw err;}
                    var newRest = new Array();
                    for(let i=0; i < rest.length; i++){ 
                        var re = rest[i].toJSON(); 
                        if(re && re.restaurant && re.restaurant.id){ 
                            try{
                                fav = await RestFav.find({where:{userId:options.accessToken.userId,restaurantId:re.restaurant.id}});
                            }catch(err){Logger.dbLogger.error(err); throw err;}
                            if(fav && fav.length > 0 ){
                                re.restaurant.favorited = true;
                            }else{
                                re.restaurant.favorited = false;
                            }
                            rest[i] = re;
                            newRest.push(re);
                        }
                    }
                    result = {
                                list: newRest, 
                                nb: newRest.length
                            } 
                    return result;
                }else{
                    var res = [];
                    try{
                        rest = await RestFav.find({where:{userId:userId}});
                    }catch(err){Logger.dbLogger.error(err); throw err;} 
                    result = {
                        list: res,
                        nb: rest.length
                    }
                    return result;
                }
            } 
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            throw error;
        } 
    };

    Restaurant.remoteMethod('favoriteRestaurant',{
        description: 'get list of favorite restaurant for connected user ###', 
        accepts: [  
                    {arg: 'userId',  type: 'string', required: true},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/favoriteRestaurant'} 
    });

    /** pagination */
    function Paginator (arr, page1, per_page){
        var deb = page1 * per_page;
        var fin = deb + per_page;
        if(fin > arr.length){fin = arr.length}
        return arr.slice(deb,fin);
    }

    /** search with name Autocomplete */
    Restaurant.searchAutocomplete = (query, limit, offset, location, options, next) => { 
        if(options && options.accessToken && options.accessToken.userId){
            var param = {key: config.googlePlace.apiKey,  types : "establishment"}
            param.input= query;
            if(location){
                param.location = location;
                param.radius = 10000;
            }
            axios.get('https://maps.googleapis.com/maps/api/place/autocomplete/json', {params: param})
                .then( async resp =>{ 
                    if(resp.data.status === 'OK'){ 
                        var all = resp.data.predictions;
                        var results = new Array();
                        for(var j=0;j < all.length;j++){ 
                            /** get result without google details */
                            if(all[j].structured_formatting && all[j].structured_formatting.main_text){
                                all[j].name = all[j].structured_formatting.main_text;
                            }
                            if(all[j].structured_formatting && all[j].structured_formatting.secondary_text){
                                all[j].formatted_address = all[j].structured_formatting.secondary_text;
                            }
                            /** get result with google details */
                            var det = await getRestaurantDetailsFromGoogle(all[j].place_id); 
                            if(det){
                                // det.favorited = false;
                                results.push(det);
                            }
                        }
                        next(null,results)
                    }else if (resp.data.predictions.length === 0){ 
                        next(null,[])
                    }else{
                        const error = new Error("Error Google autocomplete ");
                        error.statusCode = 401;
                        error.code = 'ERROR_GOOGLE_AUTOCOMPLETE'; 
                        next(error);
                    }
                });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('searchAutocomplete',{
        description: 'search restaurant ###', 
        accepts: [  
                    {arg: 'query',  type: 'string', required: true}, 
                    { arg: 'limit', type: 'number', required: false},
                    { arg: 'offset', type: 'number', required: false},
                    {arg: 'location',  type: 'string', required: false, description: 'localisation: lat,lng: 36.81133,10.18715'},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/searchAutocomplete'} 
    }); 
    
    /** save restaurant if not existe on DB */
    async function SaveOnDB (restaurant){ 
        var code_pays = ["AF","AX","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BQ","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CD","CK","CR","CI","HR","CU","CW","CY","CZ","DK","DJ","DM","DO","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FR","GF","PF","TF","GA","GM","GE","DE","GH","GI","GR","GL","GD","GP","GU","GT","GG","GN","GW","GY","HT","HM","VA","HN","HK","HU","IS","IN","ID","IR","IQ","IE","IM","IL","IT","JM","JP","JE","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MO","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","ME","MS","MA","MZ","MM","NA","NR","NP","NL","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SX","SK","SI","SB","SO","ZA","GS","SS","ES","LK","SD","SR","SJ","SZ","SE","CH","SY","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","GB","US","UM","UY","UZ","VU","VE","VN","VG","VI","WF","EH","YE","ZM","ZW"];
        try{
            var res = await Restaurant.findOne({where:{place_id:restaurant.place_id, status:1}});
        }catch( err ) { Logger.dbLogger.error(err); return(err);}
        if(res){ 
            return res;
        }else{  
            var request = require('request');
            const Container = app.models.Container;
            delete restaurant.google;
            delete restaurant.id;
            delete restaurant.scope;
            if(restaurant && restaurant.photos && restaurant.photos[0]){
                var link = 'https://maps.googleapis.com/maps/api/place/photo?photoreference='+restaurant.photos[0].photo_reference
                        +'&maxheight='+restaurant.photos[0].height+'&maxwidth='+restaurant.photos[0].width+'&key='+config.googlePlace.apiKey;
                var newNameFile = new Date().getTime()+'_'+'grumpeat.jpg';
                restaurant.photo = newNameFile;
                try{
                    await request.get({url: link,json:true}, function(error, response, file){
                    }).pipe(Container.uploadStream('grumpeat', newNameFile));
                }catch( err ) { Logger.dbLogger.error(err); return err;}
            } 
            if(restaurant && restaurant.address_components){ 
                for(var i = 0;i < restaurant.address_components.length;i++){ 
                    if(restaurant.address_components && code_pays.indexOf(restaurant.address_components[i].short_name) > -1){
                        restaurant.codePays = restaurant.address_components[i].short_name;
                    }
                }
            } 
            try{
                var newRestaurant = await Restaurant.create(restaurant);
            }catch( err ) { Logger.dbLogger.error(err); console.log(err);}
            return newRestaurant;
        }
    }

    /** Get details restaurant from google api  */
    Restaurant.GetDetails = (placeId, options, next) => {
        if(options && options.accessToken && options.accessToken.userId){
            Restaurant.findOne({where:{place_id:placeId}},async (err,r)=>{
                if(err){Logger.dbLogger.error(err); next(err)}
                if(r){ 
                    next(null,r);
                }else{
                    try{ 
                        var det = await getRestaurantDetailsFromGoogle(placeId);
                    }catch( err ) { Logger.dbLogger.error(err); console.log(err);}
                    if(det){ 
                        try{ 
                            var restaurant = await SaveOnDB(det);
                        }catch( err ) { Logger.dbLogger.error(err); console.log(err);}
                        next(null,restaurant);
                    }else{ 
                        const error = new Error("Not Restaurant Type");
                        error.statusCode = 401;
                        error.code = 'NOT_RESTAURANT_TYPE'; 
                        next(error);
                    }
                }
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('GetDetails',{
        description: 'Get details restaurant from google ###', 
        accepts: [  
                    {arg: 'placeId',  type: 'string', required: true}, 
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/GetDetails'} 
    });

    /** search with name */
    Restaurant.search = (query, limit, offset, location, options, next) => { 
        if(options && options.accessToken && options.accessToken.userId){
            const RestFav = app.models.RestFav, GlobalSettings = app.models.GlobalSettings;
            var pattern = new RegExp('.*'+query+'.*', "i");
            var filtre = {
                status:1,
                name: {like: pattern}
            };
            if(location){filtre.position = {near: location, maxDistance: 50, unit: "kilometers" }} 
            Restaurant.find({ where: filtre }, async (err,res)=>{ 
                if(err){Logger.dbLogger.error(err); next(err)}
                // get id list of fav resto
                var favoritesRes = await RestFav.find({where:{userId:options.accessToken.userId},include:"restaurant"});
                var idFav = new Array(), gIdFav = new Array();
                for(var a = 0;a < favoritesRes.length;a++){
                    if(favoritesRes[a].restaurantId){
                        idFav.push(String(favoritesRes[a].restaurantId));
                    }
                    favoritesRes[a] = favoritesRes[a].toJSON();
                    if(favoritesRes[a] && favoritesRes[a].restaurant && favoritesRes[a].restaurant.googleId){
                        gIdFav.push(String(favoritesRes[a].restaurant.googleId));
                    }
                }
                // get id list of rest from DB
                var listIdFromBase = new Array();
                for(var b = 0;b < res.length;b++){ 
                    listIdFromBase.push(res[b].googleId);
                    res[b].formbase = true;
                } 
                // delete rest from res when it's in fav
                for(let b = 0;b < res.length;b++){ 
                    if(gIdFav.indexOf(String(res[b].googleId)) > -1){ 
                        res.splice(b, 1);
                        b--;
                        continue;
                    }
                }
                var getfromgoogle = await GlobalSettings.findOne({where:{attribut:"getfromgoogle"}});
                var nb = Number(getfromgoogle.valeur); 
                if(res.length < nb){  
                    var param = {key: config.googlePlace.apiKey}
                    param.input= query;
                    if(location){
                        param.location = location;
                        param.radius = 10000;
                    }
                    axios.get('https://maps.googleapis.com/maps/api/place/queryautocomplete/json', {params: param})
                        .then( async resp =>{  
                            if(resp.data.status === 'OK'){
                                var all = resp.data.predictions;
                                var results = new Array();
                                for(var j=0;j < all.length;j++){ 
                                    if(listIdFromBase.indexOf(all[j].id) === -1){ // if not exist in DB
                                        var det = await getRestaurantDetailsFromGoogle(all[j].place_id); 
                                        if(det){
                                            try{
                                                var restaurant = await SaveOnDB(det);
                                            }catch( err ) { Logger.dbLogger.error(err); console.log(err);}
                                            restaurant.favorited = false;
                                            results.push(restaurant);
                                        }
                                    }
                                }
                                res = res.concat(results);
                                for(var b = 0;b < res.length;b++){ 
                                    if(gIdFav.indexOf(String(res[b].googleId)) > -1){ 
                                        res.splice(b, 1);
                                        b--;
                                        continue;
                                    }
                                }
                                next(null,res)
                            }else if (resp.data.predictions.length === 0){
                                for(let b = 0;b < res.length;b++){ 
                                    if(gIdFav.indexOf(String(res[b].googleId)) > -1){ 
                                        res.splice(b, 1);
                                        b--;
                                        continue;
                                    }
                                }
                                next(null,res)
                            }else{
                                const error = new Error("Error Google autocomplete ");
                                error.statusCode = 401;
                                error.code = 'ERROR_GOOGLE_AUTOCOMPLETE'; 
                                next(error);
                            }
                        });
                }else{
                    next(null,res)
                }
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restaurant.remoteMethod('search',{
        description: 'search restaurant ###', 
        accepts: [  
                    {arg: 'query',  type: 'string', required: false}, 
                    { arg: 'limit', type: 'number', required: false},
                    { arg: 'offset', type: 'number', required: false},
                    {arg: 'location',  type: 'string', required: false, description: 'localisation: lat,lng: 36.81133,10.18715'},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/search'} 
    });

    /** delete restaurant from fav */
    Restaurant.delete =  (restaurantId, options, next) => {
        const RestFav = app.models.RestFav, RestQual = app.models.RestQual;
        RestFav.destroyAll({restaurantId: restaurantId, userId: options.accessToken.userId}, (err,res) => {
            if(err){Logger.dbLogger.error(err); next(err);}
            RestQual.destroyAll({restaurantId: restaurantId, userId: options.accessToken.userId}, (err,res1) => {
                if(err){Logger.dbLogger.error(err); next(err);}
                next(null,res1);
            });
        });
    };

    Restaurant.remoteMethod('delete',{
        description: 'delete restaurant from favorite list ###', 
        accepts: [  
                    {arg: 'restaurantId',  type: 'string', required: true},  
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'delete', path: '/delete'} 
    });

    function uniqueObjectId(array){ 
        var un = [], tab = [];
        for(var t =0; t < array.length;t++){
            if(un.indexOf(String(array[t].id))=== -1){ 
                un.push(String(array[t].id))
                tab.push(array[t]);
            }
        }
        return tab;
    }

    function affectation(nbMax, source, tab1, tab2, tab3, tab4){ 
        var max = nbMax - 1;
        for(var a=0;a < source.length;a++){ 
            if(source[a].price && source[a].price.tier ){
                switch (source[a].price.tier){
                    case 1: if(tab1.length < max){tab1.push(source[a]);break;}else{break;}
                    case 2: if(tab2.length < max){tab2.push(source[a]);break;}else{break;}
                    case 3: if(tab3.length < max){tab3.push(source[a]);break;}else{break;}
                    case 4: if(tab4.length < max){tab4.push(source[a]);break;}else{break;}
                }
            }
        }
    }

    /** get server time for next suggestion */
    Restaurant.serverTime = (options, next) => {
        const SuggestionOrdre = app.models.SuggestionOrdre;
        SuggestionOrdre.findOne({where:{profilId:options.accessToken.userId}}, async (err,sor)=> { 
            if(err){ Logger.dbLogger.error(err); console.log(err); return(err); }
            if(sor){
                sor = sor.toJSON();
                var date = new Date();
                var datelast = new  Date(sor.updatedAt); 
                datelast.setHours(new  Date(sor.updatedAt).getHours() + 24);
                var tempsRestant = Math.trunc((datelast - date) / 1000);
                next(null,{"time":tempsRestant})
            }
        });
    };

    Restaurant.remoteMethod('serverTime',{
        description: 'get server time for next suggestion ###', 
        accepts: [  
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/serverTime'} 
    });

    /** suggestion Acceuil */
    Restaurant.suggestion = async (location, options, codePays)=>{
        const   RestQual = app.models.RestQual, RestFav = app.models.RestFav, Follow = app.models.Follow, 
                    GlobalSettings = app.models.GlobalSettings, SuggestionOrdre = app.models.SuggestionOrdre;
        var maxDistance = 1;
        try{ 
            var ResmaxDistance = await GlobalSettings.findOne({where:{attribut:"maxDistance"}});
        }catch(err){Logger.dbLogger.error(err); throw err;}
        maxDistance = Number(ResmaxDistance.valeur); console.log("maxDistance")
        if(options && options.accessToken && options.accessToken.userId){  
            var userId = String(options.accessToken.userId);
            /** nombre de prise par pour ordre */
            var arrPosition = -1, ordre = 0, nbfrfcu, nbSug, random, retourRes, ind, resultat;
            try{
                var sor = await SuggestionOrdre.findOne({where:{profilId:options.accessToken.userId}});
            }catch(err) { Logger.dbLogger.error(err); throw err; }
            if(sor){ 
                var so = sor.toJSON();
            } console.log('so: ',so); console.log('sor: ',sor);
            /** get rest fav  */
            try{
                var AllUserFavRest = await RestFav.find({where:{userId:userId}});
            }catch(err){ Logger.dbLogger.error(err); throw err; }
            nbfrfcu = AllUserFavRest.length; /** nbFavoriteRestaurantForCurrentUser */
            /** get params from global settings */
            var nbMax = 15;
            try{ 
                var nbMaxRes = await GlobalSettings.findOne({where:{attribut:"nbMax"}}); 
                nbMax = Number(nbMaxRes.valeur);
            } catch( err ) { Logger.dbLogger.error(err); throw err; }
            var lambda = 5;
            try{
                var lambdaRes = await GlobalSettings.findOne({where:{attribut:"lambda"}}); console.log("lambda: ",lambdaRes);
                lambda = Number(lambdaRes.valeur);  console.log('lambda: ',lambda);
            } catch( err ) { Logger.dbLogger.error(err); throw err; } console.log("lambda 01");
            /** definir le nombre global possible de suggestion */
            if(nbfrfcu < 6){  
                nbSug = 3; 
            }else if((nbfrfcu < 9) && (nbfrfcu > 5)){
                nbSug = 6;
            }else if(nbfrfcu > 8){
                nbSug = 9;
            } console.log('arrPosition: ',arrPosition);
            while(arrPosition === -1){
                var current  = 0; console.log('arrPosition1: ',arrPosition);
                if(so){ 
                    current = so.nbOrdre;
                    var lastUpdateMid = new Date(so.updatedAt.setHours(25, 0, 0, 0)); 
                    var today = new Date(); console.log('arrPosition 2');
                    if(so.nbOrdre >= nbSug){ /** l'utilisateur a terminé ses suggestions autorisé  */ console.log('arrPosition 3');
                        /** verification sur la date  */
                        if(today > lastUpdateMid){ /** il a le droit de nouveau suggestion car il a depassé minuit */
                            random = Math.floor(Math.random() * nbMax);
                            while(ordre === so.ordre){
                                ordre = Math.round(((Math.log((random * lambda)+1))/(Math.log((nbMax * lambda)+1)))* random);
                            } console.log('arrPosition 4');
                            try{
                                await sor.updateAttributes({"ordre": ordre, "nbOrdre":1});
                            }catch(err) { Logger.dbLogger.error(err); throw err; }
                            current = 1; console.log('arrPosition 5');
                        }else{ /** il recoit la derniere resultat du suggestion avec le temps restant pour la prochaine suggestion  */
                            var tempsRestant = Math.trunc((lastUpdateMid - today) / 1000); console.log('arrPosition 6');
                            var remainingTime = tempsRestant; console.log('arrPosition 7');
                        }
                    }else{ /**  generate new ordre value, l'utilisateur n'a pas epuisé le nombre de nouvelle suggestion */
                        random = Math.floor(Math.random() * nbMax); console.log('arrPosition 8');
                        while(ordre === so.ordre){
                            ordre = Math.round(((Math.log((random * lambda)+1))/(Math.log((nbMax * lambda)+1)))* random);
                        }
                        try{
                            await sor.updateAttributes({"ordre": ordre, "nbOrdre":(so.nbOrdre + 1)});
                        }catch(err) { Logger.dbLogger.error(err); throw err; }
                        current = current + 1; console.log('arrPosition 8');
                    }
                }else{ /** la premiere fois que l'utilisateur demande la suggestion */
                    random = Math.floor(Math.random() * nbMax); console.log('arrPosition 9');
                    ordre = Math.round(((Math.log((random * lambda)+1))/(Math.log((nbMax * lambda)+1)))* random);
                    try{
                        await SuggestionOrdre.create({"ordre": ordre,"nbOrdre": 1,"profilId": options.accessToken.userId}); 
                    }catch(err) { Logger.dbLogger.error(err); console.log(err); throw err;} console.log('arrPosition 10');
                }
                arrPosition = Number(ordre); 
            }   console.log("lambda 02")
            if(remainingTime){  console.log("lambda 03")
                resultat = {};
                resultat.remainingTime = remainingTime;
                /** info suggestion */
                resultat.total = nbSug;
                resultat.current = current;
                return resultat;
            }else{ console.log("lambda 04")
                try{
                    var listRQ = await RestQual.find({where:{userId:userId}}); console.log("listRQ"); 
                }catch(err){Logger.dbLogger.error(err); throw err;}
                /** get fav rest for current user (options) by location 
                    -- get maxDistance from DB or get default 1 */
                try{ 
                    var nearReastaurant = await Restaurant.find({where:{position : {near: location,maxDistance: maxDistance,unit: "kilometers"}}});
                }catch(err){Logger.dbLogger.error(err); throw err;}
                var resLust = new Array(); console.log("listRQ 01");
                for(var i = 0;i < nearReastaurant.length;i++){
                    resLust.push(String(nearReastaurant[i].id));
                }
                console.log("listRQ 02");
                try{
                    var userFav = await RestFav.find({where:{userId:userId, restaurantId:{inq:resLust}},include:"restaurant"});
                }catch(err){Logger.dbLogger.error(err); throw err;}    
                console.log("userFav") 
                retourRes = new Array();
                var listIdFavRst = new Array();
                for(let i = 0; i < userFav.length; i++){
                    userFav[i] = userFav[i].toJSON();
                    if(userFav[i].restaurant && userFav[i].restaurant.id){ 
                        retourRes.push(userFav[i]);
                        listIdFavRst.push(String(userFav[i].restaurant.id))
                    }
                }
                userFav = retourRes;  /** list fav rest for current user by location */
                listIdFavRst = [...new Set(listIdFavRst)]; /** liste des Id restaurants favoris  current user by location */
                /** Les qualités du restaurant correspondent aux qualités préférées de l’utilisateur.
                    Les qualités préférées de l’utilisateur */
                var listQ = new Array();
                for(var a = 0; a < listRQ.length; a++){
                    listQ.push(listRQ[a].qualityId);
                }
                console.log("1")
                var ulq = new Array(); /** unique list quality */
                var unblq = new Array(); /** number off apparition */
                for(var b = 0; b < listQ.length; b++){ 
                    if(ulq.indexOf(String(listQ[b])) === -1){
                        ulq.push(String(listQ[b]));
                        var compt = 0;
                        for(var c=0;c < listQ.length;c++){ 
                            if(String(listQ[b]) === String(listQ[c])){ 
                                compt++;
                            }
                        } 
                        unblq.push(compt);
                    }
                } console.log("2")
                /** tri list quality */
                var tri = false;
                while (tri === false) { 
                    tri = true;
                    for(let a = 0; a < unblq.length-1; a++){
                        if(unblq[a] < unblq[a+1]){
                            var aux = unblq[a];
                            unblq[a] =  unblq[a+1];
                            unblq[a+1] = aux;
                            aux = ulq[a];
                            ulq[a] =  ulq[a+1];
                            ulq[a+1] = aux;
                            tri = false;
                        }
                    }
                }
                console.log('2.1');
                var userQualities = ulq.slice(0, 4);
                /** get qulities */
                var crit1 = new Array(); var crit1Id = new Array();
                try{
                    var rq = await RestQual.find({where:{userId:{neq:userId},qualityId:{inq:userQualities}, restaurantId: {inq:resLust}},include:{relation:'restaurant'}});
                }catch( err ) { Logger.dbLogger.error(err); throw err; } 
                retourRes = new Array(); console.log("3")
                for(let i = 0; i < rq.length; i++){
                    rq[i] = rq[i].toJSON();
                    if(rq[i].restaurant && rq[i].restaurant.id){ 
                        retourRes.push(rq[i]);
                        crit1.push(rq[i].restaurant)
                        crit1Id.push(String(rq[i].restaurant.id));  
                    }
                }
                rq = retourRes;  
                crit1 = uniqueObjectId(crit1); 
                crit1Id = [...new Set(crit1Id)];
                /** conseillé par des amis 
                    -- get firends  */  
                var friends = [];
                try{ console.log("4")
                    var followers = await Follow.find({where:{profilId:userId,status:2},fields:'followerId'}); 
                } catch( err ) { Logger.dbLogger.error(err); throw err; } 
                for(let i = 0; i < followers.length; i++){
                    friends.push(String(followers[i].followerId))
                }
                try{
                    var following = await Follow.find({where:{followerId:userId,status:2},fields:'profilId'}); 
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                for(var j = 0;j < following.length;j++){
                    friends.push(String(following[j].profilId))
                } console.log("5")
                friends = [...new Set(friends)];
                /** get friends fav restaurants */
                var ffr = [], ffrId = []; 
                try{
                    var rest = await RestFav.find({where:{userId: {inq: friends}, restaurantId: {inq: resLust}}, include:{ relation: 'restaurant'}}); 
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                retourRes = new Array(); 
                for(let i = 0; i < rest.length; i++){
                    rest[i] = rest[i].toJSON();
                    if(rest[i].restaurant){ 
                        retourRes.push(rest[i]);
                    }
                } console.log("6");  
                rest = retourRes;  
                for(let l = 0; l < rest.length; l++){
                    ffr.push(rest[l].restaurant);
                    ffrId.push(String(rest[l].restaurant.id));
                }
                var crit2 = uniqueObjectId(ffr);  /** list friends favorites restaurant */
                var crit2Id = [...new Set(ffrId)];   /** list id friends favorites restaurant */
                /**  Restaurants dans la liste des préférés d’un autre utilisateur qui a aimé aussi un de nos restaurants */
                try{
                    friends.push(userId);
                    var lrp = await RestFav.find({where:{userId:{nin:friends},restaurantId:{inq:listIdFavRst}}}); 
                } catch( err ) { Logger.dbLogger.error(err); throw err; } 
                var nf = new Array(); /** profil n'ont friend mais ont meme restaurants fav */
                for(var y = 0; y < lrp.length; y++){
                    nf.push(lrp[y].userId)
                } console.log("7")
                var crit3 = new Array(), crit3Id = new Array();
                var stringListIdFavRst = new Array(), stringResLust = new Array();
                for(let c = 0; c < listIdFavRst.length; c++){
                    stringListIdFavRst.push(String(listIdFavRst[c]));
                }
                for(let c = 0; c < resLust.length; c++){
                    stringResLust.push(String(resLust[c]));
                }
                let difference = stringListIdFavRst.filter(value => -1 !== stringResLust.indexOf(value));
                try{ 
                    var lrpp = await RestFav.find({where:{userId:{inq:nf},restaurantId:{inq: difference}}, include: {relation:'restaurant'}});
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                retourRes = new Array(); 
                for(let i = 0; i < lrpp.length; i++){
                    lrpp[i] = lrpp[i].toJSON();
                    if(lrpp[i].restaurant && lrpp[i].restaurant.id){ 
                        retourRes.push(lrpp[i]);
                        crit3.push(lrpp[i].restaurant);
                        crit3Id.push(String(lrpp[i].restaurantId));
                    }
                } 
                lrpp = retourRes; 
                /** intersection entre les tableux des ids */
                var data = [crit1Id, crit2Id, crit3Id];
                var intersection3 = data.reduce((a, b) => a.filter(c => b.includes(c)));  
                var three = new Array();
                for(var x = 0;x < intersection3.length;x++){
                    for(var z = 0;z < crit1.length;z++){
                        if(crit1[z].id === intersection3[x]){
                            three.push(crit1[z]);
                        }
                    }
                } console.log("9")
                /** delete element from crit1 & crit2 & crit3 if exist in intersection between 3 array */
                for(let x = 0; x < intersection3.length; x++){
                    ind = crit1Id.indexOf(intersection3[x]);
                    if(ind > -1){
                        crit1Id.splice(ind,1);
                        crit1.splice(ind,1);
                    }
                    ind = crit2Id.indexOf(intersection3[x]);
                    if(ind > -1){
                        crit2Id.splice(ind,1);
                        crit2.splice(ind,1);
                    }
                    ind = crit3Id.indexOf(intersection3[x]);
                    if(ind > -1){
                        crit3Id.splice(ind,1);
                        crit3.splice(ind,1);
                    }
                } console.log("10")
                /** intersect crit1 & crit2 */
                var two1 = intersection(crit1Id,crit2Id,crit1);
                /** delete element from crit1 & crit2 if used in intersect two1*/
                for(let x = 0; x < two1.length; x++){
                    ind = crit1Id.indexOf(two1[x].id);
                    if(ind > -1){
                        crit1Id.splice(ind,1);
                        crit1.splice(ind,1);
                    }
                    ind = crit2Id.indexOf(two1[x].id);
                    if(ind > -1){
                        crit2Id.splice(ind,1);
                        crit2.splice(ind,1);
                    }
                }  console.log("11")
                /** intersect crit1 & crit3 */
                var two2 = intersection(crit1Id,crit3Id,crit1);
                /** delete element from crit1 & crit3 if used in intersect two2*/
                for(let x = 0; x < two2.length; x++){
                    ind = crit1Id.indexOf(two2[x].id);
                    if(ind > -1){
                        crit1Id.splice(ind,1);
                        crit1.splice(ind,1);
                    }
                    ind = crit3Id.indexOf(two2[x].id);
                    if(ind > -1){
                        crit3Id.splice(ind,1);
                        crit3.splice(ind,1);
                    }
                } console.log("13")
                /** intersect crit3 & crit2 */
                var two3 = intersection(crit3Id,crit2Id,crit3);
                /** delete element from crit3 & crit2 if used in intersect two3*/
                for(let x = 0; x < two3.length; x++){
                    ind = crit2Id.indexOf(two3[x].id);
                    if(ind > -1){
                        crit2Id.splice(ind,1);
                        crit2.splice(ind,1);
                    }
                    ind = crit3Id.indexOf(two3[x].id);
                    if(ind > -1){
                        crit3Id.splice(ind,1);
                        crit3.splice(ind,1);
                    }
                } console.log("14")
                var two = [...two1, ...two2, ...two3];
                var crit  = [...crit1, ...crit2, ...crit3];
                var tab1 = new Array(), tab2 = new Array(), tab3 = new Array(), tab4 = new Array();
                affectation(nbMax, three, tab1, tab2, tab3, tab4);
                affectation(nbMax, two, tab1, tab2, tab3, tab4);
                affectation(nbMax, crit, tab1, tab2, tab3, tab4);
                var result = new Array();
                try{ console.log("15")
                    var price1 = await getElementByPosition (arrPosition, tab1, userFav, location, 1);
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                try{
                    var price2 = await getElementByPosition (arrPosition, tab2, userFav, location, 2);
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                try{
                    var price3 = await getElementByPosition (arrPosition, tab3, userFav, location, 3);
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                try{
                    var price4 = await getElementByPosition (arrPosition, tab4, userFav, location, 4);
                } catch( err ) { Logger.dbLogger.error(err); throw err; }
                if(price1.name){result.push(price1);} console.log("16")
                if(price2.name){result.push(price2);}
                if(price3.name){result.push(price3);}
                if(price4.name){result.push(price4);}
                try{ 
                    var nbFav = await RestFav.count({userId:userId}); 
                } catch( err ) { Logger.dbLogger.error(err); throw err; } console.log("17"); 
                // si n'y a pas de restaurant dans la base, on utilise google 
                if(result.length < 4){ console.log("18") 
                    var listIdFromBase = new Array();
                    for(let a = 0; a < userFav.length; a++){
                        listIdFromBase.push(userFav[a].restaurant.googleId);
                    } console.log("20")
                    if(nearReastaurant.length > 0){ 
                        var resultId = new Array();
                        for(let a = 0; a < result.length; a++){
                            resultId.push(result[a].id); 
                        }
                        for(var d = 0; d < nearReastaurant.length; d++){
                        if((resultId.indexOf(nearReastaurant[d].id) === -1) && (listIdFromBase.indexOf(nearReastaurant[d].id) === -1 )){
                                result.push(nearReastaurant[d]);
                            }
                        }
                        var h1,t1 = new Array(),t2 = new Array(),t3 = new Array(),t4 = new Array(), res = new Array();
                        h1 = Math.floor(nbfrfcu / 3);
                        for(var e = 0; e < result.length; e++){ 
                            switch (result[e].price.tier){
                                case 1: { t1.push(result[e]); break; }
                                case 2: { t2.push(result[e]); break; }
                                case 3: { t3.push(result[e]); break; }
                                case 4: { t4.push(result[e]); break; }
                            }
                        }
                        for(let e = 0; e < h1+1; e++){
                            res.push(t1[e]);
                            t1.splice(e, 1);
                            res.push(t2[e]);
                            t2.splice(e, 1);
                            res.push(t3[e]);
                            t3.splice(e, 1);
                            res.push(t4[e]);
                            t4.splice(e, 1);
                        }
                        resultat = {
                            nbFav: nbFav,
                            restaurants : res
                        } 
                        resultat.total = nbSug;
                        resultat.current = current; 
                        return resultat;
                    }else if(result.length < 4){ 
                        var param = {
                            key: config.googlePlace.apiKey,
                            location: location,
                            radius: 1000,
                            type: "restaurant"
                        };
                        try{
                            var resp = await axios.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json', {params: param}); // .then( async resp =>{ 
                        } catch( err ) { Logger.dbLogger.error(err); throw err; }
                        if((resp.data.status === 'OK')&&(resp.data.results.length > 0)){ console.log("21")
                            for(let a = 0;a < resp.data.results.length; a++){
                                var dataG = resp.data.results[a];
                                if(listIdFromBase.indexOf(dataG.id) === -1 ){
                                    try{
                                        var det = await getRestaurantDetailsFromGoogle(dataG.place_id); 
                                    } catch( err ) { Logger.dbLogger.error(err); throw err; }
                                    if(det){
                                        try{
                                            var restaurant = await SaveOnDB(det);
                                            result.push(restaurant);
                                        }catch( err ) { Logger.dbLogger.error(err); throw err;}
                                    }
                                }
                            }
                            console.log("22");
                            resultat = {
                                nbFav: nbFav,
                                restaurants : result
                            } 
                            resultat.total = nbSug;
                            resultat.current = current;
                            return resultat;
                        }else{ console.log("18")
                            try{
                                result = await Restaurant.find({restaurantId:{nin:listIdFavRst}});
                            } catch( err ) { Logger.dbLogger.error(err); throw err; }
                            t1 = new Array(); t2 = new Array(); t3 = new Array(); t4 = new Array(); res = new Array();
                            h1 = Math.floor(nbfrfcu / 3);
                            for(let e = 0; e < result.length; e++){
                                switch (result[e].price.tier){
                                    case 1: t1.push(result[e]); break;
                                    case 2: t2.push(result[e]); break;
                                    case 3: t3.push(result[e]); break;
                                    case 4: t4.push(result[e]); break;
                                }
                            }
                            for(let e = 0; e < h1+1; e++){
                                res.push(t1[e]);
                                t1.splice(e, 1);
                                res.push(t2[e]);
                                t2.splice(e, 1);
                                res.push(t3[e]);
                                t3.splice(e, 1);
                                res.push(t4[e]);
                                t4.splice(e, 1);
                            }
                            resultat = {
                                nbFav: nbFav,
                                restaurants : res
                            }
                            resultat.total = nbSug;
                            resultat.current = current;
                            return resultat;
                        }
                    }else{  console.log("19")
                        resultat = {
                            nbFav: nbFav,
                            restaurants : result
                        }
                        resultat.total = nbSug;
                        resultat.current = current;
                        return resultat;
                    } 
                }else{
                    resultat = {
                        nbFav: nbFav,
                        restaurants : result 
                    }
                    resultat.total = nbSug;
                    resultat.current = current;
                    return resultat;
                }
            }
        }else{ console.log('2')
            var filter1 = {};  
            if(location){
                filter1.position = {near: location,maxDistance: maxDistance, unit: "kilometers"};
            }
            if(codePays){
                filter1.codePays = codePays;
            } 
            console.log('3')
            try{
                var list = await Restaurant.find({where:filter1, limit: 4,
                    include:{relation: "qualities",scope:{ include: { relation : "quality",scope:{where:{status:1}}}}}});
            }catch(err){Logger.dbLogger.error(err); throw err;}
            console.log('34')
            return list; 
        } 
    };

    async function getRestaurantDetailsFromGoogle(place_id){ 
        var param = {
            key: config.googlePlace.apiKey,
            placeid: place_id
        }; 
        try{
            var det = await axios.get('https://maps.googleapis.com/maps/api/place/details/json', {params: param}); 
            if(det.data.status === 'OK'){
                var data = det.data.result; 
                var t = data.types; 
                if(t && (t.indexOf('restaurant') > -1 || t.indexOf('food') > -1) && t.indexOf('lodging') === -1){  
                    data.location = {
                        lng: data.geometry.location.lng,
                        lat: data.geometry.location.lat
                    }; 
                    
                    // get Country
                    if(data.address_components){
                        var ac = data.address_components; 
                        for(var i=0;i < ac.length;i++){
                            var types = ac[i].types; 
                            if(types.indexOf("country") > -1){
                                data.location.country = ac[i].long_name;
                            }
                            if(types.indexOf("locality") > -1){
                                data.location.state = ac[i].long_name;
                            }
                        }
                    }
                    if(data && data.price_level){
                        data.price = {
                            tier : data.price_level,
                            currency : '$',
                            from: 'google'
                        }
                    }else{
                        var random = Math.floor(Math.random() * 4); 
                        data.price = {
                            tier : random+1,
                            currency : '$',
                            from: 'default'
                        }
                    }
                    data.googleId = data.id; 
                    data.google = true; 
                    det.favorited = false; 
                    return data;
                }else{return null}
            }else{return null}
        }catch( err ) { Logger.dbLogger.error(err); throw err; }
    }

    async function getElementByPosition (ordre, arr, userFav, location, price){  
        if(arr.length > 0){
            if(ordre < arr.length){
                return arr[ordre];  
            }else{
                var random = Math.floor(Math.random() * (arr.length));
                return arr[random];
            }
        }else {
            return {};
        }
    }

    function intersection (aId, bId , a){ 
        var result = new Array();
        var inter = aId.filter(value => -1 !== bId.indexOf(value));
        for(var x = 0;x < inter.length;x++){
            for(var z = 0;z < a.length;z++){
                if(a[z].id === inter[x]){
                    result.push(a[z]);
                }
            }
        }
        return result;
    }
    
    Restaurant.remoteMethod('suggestion',{
        description: 'suggest list restaurant ###', 
        accepts: [  
                    {arg: 'location', type: 'string', required: false, description: 'localisation: lat,lng: 36.81133,10.18715'},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'},
                    {arg: 'codePays', type: 'string', required: false}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/suggestion'} 
    });

    Restaurant.observe('before save', function sanitizedAdress(ctx, next) {
        const data = ctx.instance ? ctx.instance : ctx.data;  
        if(data && data.geometry && data.geometry.location){
            data.position = data.geometry.location;
        }
        next();
    });

    /** NewsFeed 
     * @param object options
     * @param number limit
     * @param number offset
     * @param object location
     * @param string codePays
     * @return object
    */
    Restaurant.NewsFeed = async (options, limit, offset, location, codePays, next) => { 
        const Follow = app.models.Follow, RestFav = app.models.RestFav, GlobalSettings = app.models.GlobalSettings, Profil = app.models.Profil;
        var off = offset || 0, maxDistance = 1;
        var lim = 5, ResmaxDistance, result1, uniqId2, filter1, data, fav, list;
        if(options && options.accessToken && options.accessToken.userId){ 
            var currentId = String(options.accessToken.userId); 
            try{
                var flr = await Follow.find({where:{followerId: currentId, status:2}});  
            }catch(err){Logger.dbLogger.error(err); throw err;} 
            // get maxDistance from DB or get default 1
            try{
                ResmaxDistance = await GlobalSettings.findOne({where:{attribut:"maxDistance"}});
                maxDistance = Number(ResmaxDistance.valeur); 
            } catch(err) { Logger.dbLogger.error(err); throw err;}
            if(flr.length > 0){ 
                var result = new Array(), uniqId = new Array();
                for(var i = 0; i < flr.length; i++){  
                    try{
                        list = await RestFav.find({where:{userId:flr[i].profilId},
                                        include:[
                                            {
                                                relation:"restaurant",
                                                scope:{
                                                    include:{
                                                        relation: "qualities",
                                                        scope:{
                                                            include: {
                                                                relation : "quality",
                                                                scope:{
                                                                    where:{status:1},
                                                                }
                                                            }
                                                        }
                                                    } 
                                                }
                                            },
                                                { relation:"profil"}
                                            ]
                                            });
                    } catch(err) { Logger.dbLogger.error(err); throw err;}                        
                    if(list.length > 0){
                        for(var o = 0;o < list.length;o++){
                            if(uniqId.indexOf(String(list[o].id)) === -1){
                                uniqId.push(String(list[o].id));
                                result.push(list[o]);
                            }
                        }
                    }
                } 
                result.sort(sortByCreatedAt);
                data = Paginator(result, off, lim); 
                /** si l’utilisateur scroll beaucoup et arrive à la fin des news feed de ses contacts, d'ajouter les news feed des utilisateurs avec profil publique et dans la même ville que l’utilisateur */
                if(data.length < lim){ 
                    var filter = {};
                    if(location){
                        filter.location = {near: location,maxDistance: maxDistance,unit: "kilometers"};
                    }
                    result1 = new Array();
                    var uniqId1 = new Array();
                    try{
                        var pp = await Profil.find({statu:1, privateAcc: false}); // pp : publicProfil
                    } catch(err) { Logger.dbLogger.error(err); throw err;} 
                    for(var m = 0;m < pp.length;m++){
                        if(String(pp[m].id) !== String(currentId)){
                            try{
                                list = await RestFav.find({where:{userId:pp[m].id},
                                include:[
                                    {
                                        relation:"restaurant",
                                        scope:{
                                            where:filter,
                                            include:{
                                                relation: "qualities",
                                                scope:{
                                                    include: {
                                                        relation : "quality",
                                                        scope:{
                                                            where:{status:1},
                                                        }
                                                    }
                                                }
                                            } 
                                        }
                                    },
                                    { relation:"profil"}
                                ]
                                    });
                            } catch(err) { Logger.dbLogger.error(err); throw err;} 
                            for(var x = 0;x < list.length;x++){
                                list[x] = list[x].toJSON();
                                if(!list[x].restaurant){
                                    list.splice(x, 1);
                                    x--;
                                    continue;
                                }
                            } 
                            if(list.length > 0){
                                for(let o = 0;o < list.length;o++){
                                    if(uniqId1.indexOf(String(list[o].id)) === -1){
                                        uniqId1.push(String(list[o].id));
                                        result1.push(list[o]);
                                    }
                                }
                            }
                        }
                    }
                    data = data.concat(result1);
                    data = Paginator(data, off, lim);
                }
                for(let i = 0; i < data.length; i++){
                    if(data[i].restaurant){
                        try{
                            fav = await RestFav.find({where:{userId:options.accessToken.userId,restaurantId:data[i].restaurant.id}});
                        }catch(err) { Logger.dbLogger.error(err); throw err; }
                        if(fav.length > 0 ){
                            data[i].restaurant.favorited = true;
                        }else{
                            data[i].restaurant.favorited = false;
                        }
                    }
                }
                return data;
            }else{
                result1 = new Array();
                uniqId2 = new Array();
                filter1 = {};  
                if(location){
                    filter1.location = {near: location,maxDistance: maxDistance,unit: "kilometers"};
                }
                try{
                    list = await RestFav.find({where:{userId:{neq:currentId}},limit:50,order:"createdAt DESC",
                                include:[  
                                {
                                    relation:"restaurant",
                                    scope:{
                                        where:filter1,
                                        include:{
                                            relation: "qualities",
                                            scope:{
                                                include: {
                                                    relation : "quality",
                                                    scope:{
                                                        where:{status:1}
                                                    }
                                                }
                                            }
                                        } 
                                    }
                                },
                                { relation:"profil"}
                                ]
                            }); 
                }catch( err ) { Logger.dbLogger.error(err); throw err;}
                for(let x = 0; x < list.length; x++){
                    list[x] = list[x].toJSON();
                    if(!list[x].restaurant){
                        list.splice(x, 1);
                        x--;
                        continue;
                    }
                } 
                if(list.length > 0){
                    for(let o = 0; o < list.length; o++){
                        if(uniqId2.indexOf(String(list[o].id)) === -1){
                            uniqId2.push(String(list[o].id));
                            result1.push(list[o]);
                        }
                    }
                }
                data = Paginator(result1, off, lim); 
                for(let i = 0; i < data.length; i++){
                    if(data[i].restaurant){
                        try{
                            fav = await RestFav.find({where:{userId:options.accessToken.userId,restaurantId:data[i].restaurantId}});
                        }catch( err ) { Logger.dbLogger.error(err); throw err;}
                        if(data[i].restaurant){
                            if(fav.length > 0 ){
                                data[i].restaurant.favorited = true;
                            }else{
                                data[i].restaurant.favorited = false;
                            }
                        }
                    }
                }
                return data; 
            }
        }else{ 
            try{
                ResmaxDistance = await GlobalSettings.findOne({where:{attribut:"maxDistance"}});
            }catch( err ) { Logger.dbLogger.error(err); throw err;}
            maxDistance = Number(ResmaxDistance.valeur); 
            result1 = new Array();
            uniqId2 = new Array();
            filter1 = {};  
            if(location){
                filter1.location = {near: location,maxDistance: maxDistance,unit: "kilometers"};
            }
            if(codePays){
                filter1.codePays = codePays;
            }
            try{
                list = await RestFav.find({order:"createdAt DESC",
                            include:[  
                                {
                                    relation:"restaurant",
                                    scope:{
                                        "where":filter1,
                                        include:{
                                            relation: "qualities",
                                            scope:{
                                                include: {
                                                    relation : "quality",
                                                    scope:{
                                                        where:{status:1}
                                                    }
                                                }
                                            }
                                        } 
                                    }
                                },
                                { relation:"profil"}
                            ]
                        });
            }catch(err) { Logger.dbLogger.error(err); throw err;}
            for(let x = 0; x < list.length; x++){
                list[x] = list[x].toJSON(); 
                if(!list[x].restaurant){  
                    list.splice(x, 1);
                    x--;
                    continue;
                }
            } 
            if(list.length > 0){
                for(let o = 0; o < list.length; o++){
                    if(uniqId2.indexOf(String(list[o].id)) === -1){
                        uniqId2.push(String(list[o].id));
                        result1.push(list[o]);
                    }
                }
            } 
            data = Paginator(result1, off, lim);  
            for(let i = 0; i < data.length; i++){
                if(data[i].restaurant){
                    data[i].restaurant.favorited = false;
                }
            } 
            return data; 
        }
    };

    function sortByCreatedAt(a,b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
    }

    Restaurant.remoteMethod('NewsFeed',{
        description: 'get list fav restaurant added by followings ###', 
        accepts: [  
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'},
                    {arg: 'limit', type: "number", required: false},
                    {arg: 'offset', type: "number", required: false},
                    {arg: 'location', type: 'string', required: false, description: 'localisation: lat,lng: 36.81133,10.18715'},
                    {arg: 'codePays', type: "string", required: false}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/NewsFeed'} 
    });

    Restaurant.afterRemote('**', async (ctx) => {
        if(ctx.method.name === 'favoriteRestaurant'){
            var list = ctx.result.list;  
            for(var i = 0; i < list.length; i++){ 
                const Promotion = app.models.Promotion, RestQual = app.models.RestQual, Quality = app.models.Quality;
                const date = new Date();
                try{ // get promotion by restaurant 
                    var p = await Promotion.find({where:{restaurantId:list[i].restaurant.id,end:{gt:date}}}); 
                }catch(err){Logger.dbLogger.error(err); throw err;} 
                ctx.result.list[i].hasPromo = true;
                if(p.length < 1){
                    ctx.result.list[i].hasPromo = false;
                }
                try{
                    var quals = await RestQual.find({where:{restaurantId:list[i].restaurant.id},include:"quality"});
                }catch(err){Logger.dbLogger.error(err); throw err;}  
                var uniq = [];
                for(var a = 0;a < quals.length; a++){
                    var t = quals[a].toJSON();
                    if(uniq.indexOf(t.quality.id) === -1){
                        uniq.push(t.quality.id); 
                    }
                } 
                var nbred = [];
                for(var b = 0; b < uniq.length; b++){ 
                    var compt = 0;
                    for(var c = 0; c < quals.length; c++){
                        if(String(uniq[b]) === String(quals[c].qualityId)){ 
                            compt++;
                        }
                    }
                    nbred[b] = compt;
                }
                // tri
                var tri = false;
                while (tri === false) {  
                    tri = true;
                    for(var d = 0; d < nbred.length-1; d++){
                        if(nbred[d] < nbred[d+1]){
                            var aux = nbred[d];
                            nbred[d] =  nbred[d+1];
                            nbred[d+1] = aux;
                            aux = uniq[d];
                            uniq[d] =  uniq[d+1];
                            uniq[d+1] = aux;
                            tri = false;
                        }
                    }
                }
                var rt = uniq.slice(0, 3);
                var res = [];
                for(let t = 0; t < rt.length; t++){ 
                    try{
                        var qual = await Quality.findById(rt[t]);
                    }catch(err){Logger.dbLogger.error(err); throw err;}
                    res.push(qual);
                }
                ctx.result.list[i].quals = res
            } 
            return;
        }else{
            return;
        }
    });

};


