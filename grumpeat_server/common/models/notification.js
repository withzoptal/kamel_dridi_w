'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const config = require('../../server/config.json');
var OneSignal = require('onesignal-node');
const cron = require('cron');
module.exports = function(Notification) {
    var myClient = new OneSignal.Client({
        userAuthKey: config.onesignal.apiKey,
        app: { appAuthKey: config.onesignal.apiKey, appId: config.onesignal.appId }
    });

    /** Send notifiaction to ALL users */
    Notification.SendNotif = (msgEn, msgFr, next) => {
        /** Create a notification to send */
        var firstNotification = new OneSignal.Notification({
            contents: {
                en: msgEn,
                fr: msgFr
            }
        });
        /** set target users */
        firstNotification.postBody["data"] = {"msgEn": msgEn, "msgFr": msgFr}; 
        firstNotification.postBody["included_segments"] = ["Active Users"];
        /** send this notification to All Users except Inactive ones */
        myClient.sendNotification(firstNotification, function (err, httpResponse,data) {
            if (err) {
                next(err)
            } else {
                /** creation d'une notification pour l'historique */
                var content = {
                    type : "FromAdmin",
                    subjectEn:msgEn, 
                    subjectFr:msgFr
                }
                Notification.create({notificationType:"FromAdmin",content:content});
                next();
            }
        });
    };

    Notification.remoteMethod('SendNotif',{
        description: 'Send notif for all ##',
        accepts: [
                    { arg: 'msgEn', type:'string', required: true},
                    { arg: 'msgFr', type: 'string', required: false}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/SendNotif'}
    });

    function sortByCreatedAt(a,b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
    };

    /** return list request follow && followers favorite added */
    Notification.listRequestandNotifs = async (lim, off, userId) => {
        const Follow = app.models.Follow, RestFav = app.models.RestFav, Profil = app.models.Profil;
        var res = {};
        try{
            await Notification.updateAll({"content.userId": userId, status: 1, unread: true},{unread:false})
        }catch(err){Logger.dbLogger.error(err); throw err;}
        // listy request 
        try{
            var fls = await Follow.find({where:{profilId: userId, status: 1},include:'follower'}); 
        }catch(err){Logger.dbLogger.error(err); throw err;}
        res.request = fls; 
        // list notif
        var limit = lim || 10;
        var offset = off || 0;
        try{  // get list followers
            var fl = await Follow.find({where:{followerId: userId,status:2}}); 
        }catch(err){Logger.dbLogger.error(err); throw err;} 
        var  notifByUser = new Array(), aux;
        for(var i = 0;i < fl.length;i++){ 
            aux = await RestFav.find({where:{userId:fl[i].profilId, createdAt:{gte: fl[i].updatedAt}}, include:['restaurant','profil']}); 
            notifByUser = [...aux,...notifByUser]; 
        } 
        res.notifs = notifByUser;
        res.notifs.sort((a, b) => (a.updatedAt < b.updatedAt) ? 1 : -1);
        res.notifs = Paginator (res.notifs, offset, limit);
        try{ 
            var userFav = await RestFav.find({where:{userId:userId}});
        }catch(err){Logger.dbLogger.error(err); throw err;}
        var ufid = []; 
        for(var j = 0;j < userFav.length;j++){
            ufid.push(String(userFav[j].restaurantId));
        } 
        for(var k = 0;k < res.notifs.length;k++){ 
            var str = String(res.notifs[k].restaurantId);
            var ex =  ufid.indexOf(str);
            res.notifs[k] =  res.notifs[k];
            if(ex == -1){ 
                res.notifs[k].restaurant.favorited = false;
            }else{ 
                res.notifs[k].restaurant.favorited = true;
            }
        } 
        /** requestAccepted */
        try{
            var reqAccept = await Notification.find({where:{"content.userId":userId, status:1, "content.type":{neq:"requestFollow"}}});
        }catch(err){Logger.dbLogger.error(err); throw err;}  
        var notiflist = res.notifs; 
        notiflist = notiflist.concat(reqAccept);
        notiflist.sort(sortByCreatedAt);
        var l = new Array();
        res.notifs = notiflist;
        l = res.notifs; 
        var profil;
        for(let i = 0; i < l.length;i++){
            if(l[i].content && l[i].content.userRequested){
                profil = await Profil.findById(String(l[i].content.userRequested));
                if(profil){
                    l[i].acceptedBy = profil;
                }
            } 
            if(l[i].content && l[i].content.userId){
                profil = await Profil.findById(String(l[i].content.userId));
                if(profil){
                    l[i].profil = profil;
                }
            } 
        }  
        return res;
    };

    Notification.remoteMethod('listRequestandNotifs',{
        description: 'list request to follow & update add fav friends ##',
        accepts: [
                    {arg: 'limit', type:'number', required: false},
                    {arg: 'offset', type:'number', required: false},
                    { arg: 'userId', type: 'string', required: true}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/listRequestandNotifs'}
    });

    var i= 0;
    /** periodic send notif when friend have added a new favorite restaurant cronTime: '* * 21 * * 0', */
    const priodicNotif = new cron.CronJob({
        cronTime: '0 0 21 * * 0',
        onTick: function () {
            const Profil = app.models.Profil, Follow = app.models.Follow, RestFav = app.models.RestFav;
            var users = new Array(); 
            // get user where active, connected onSignal, notification active
            Profil.find({where:{statu:1,oneSignalId:{"neq":""},notification:true}},async (err,pr)=>{ 
                if(err){Logger.dbLogger.error(err); console.log(err);} 
                for(var i =0;i < pr.length;i++){
                    // get how user follow
                    var followers = await Follow.find({where:{followerId:pr[i].id, status:2},include:"user"});
                    if(followers.length > 0){
                        var compt = 0;
                        for(var j=0;j < followers.length;j++){
                            followers[j] = followers[j].toJSON();
                            // get  follower how add new fav restaurant
                            var date = new Date(); date.setDate(date.getDate() - 6); 
                            if(followers[j] && followers[j].user && followers[j].user.id ){
                                var favRes = await RestFav.find({where:{userId:followers[j].user.id,createdAt:{"gte":date}},include:"profil"}); 
                                if(favRes.length > 0){
                                    users.push(followers[j].user);
                                }
                            }
                        }
                        if(users.length > 2){
                            var msgEn = users[0].firstName+' '+users[0].lastName+', '+users[0].firstName+' '+users[0].lastName+' and '+(users.length-2)+' other friends have added a new favorite restaurant'
                        }else if (users.length <= 2){
                            var msgEn = users[0].firstName+' '+users[0].lastName+', '+users[0].firstName+' '+users[0].lastName+' have added a new favorite restaurant'
                        }
                        /** get unreaded notification  */
                        var AllNotifByUserUnread = await Notification.find({where:{"content.userId":pr[i].id, status:1, unread: true}});
                        var firstNotification = new OneSignal.Notification({
                            contents: {
                                en: msgEn
                            },
                            include_player_ids : [pr[i].oneSignalId],
                            ios_badgeType: "Increase",
                            ios_badgeCount: AllNotifByUserUnread.length + 1 
                        }); 
                        firstNotification.postBody["data"] = {"msgEn": msgEn, "type": "PriodicNotif"}; 
                        await myClient.sendNotification(firstNotification, function (err, httpResponse,data) { 
                            if (err) {console.log(err)}
                        });
                    }
                    
                }
            });
        },
        start: false
    });

    if (!priodicNotif.running) {
        priodicNotif.start();
    }

    /** pagination */
    function Paginator (arr, page1, per_page){
        var page = page1+1;
        var deb = (page * per_page) - per_page;
        if(deb < 0){deb = 0;}
        var fin = (deb + per_page) ;
        // var tranche = arr.slice(deb,fin);
        return arr.slice(deb,fin);
    }
};

    
