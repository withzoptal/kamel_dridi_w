'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;

module.exports = function(Restfav) {

    /** drag & drop: fav Restaurant permutation rank */
    Restfav.permutationRank = (fav1, fav2, options, next)=>{
        if(options && options.accessToken && options.accessToken.userId){ 
            Restfav.findById(fav1, async (err,f1)=>{ 
                if(err){Logger.dbLogger.error(err); next(err);}
                if(f1){
                    var f2 = await Restfav.findById(fav2);
                    if(f2){
                        var r1 = f1.rank;
                        var r2 = f2.rank;
                        try{
                            await f1.updateAttributes({rank:r2});
                            await f2.updateAttributes({rank:r1});
                        } catch( err ) { Logger.dbLogger.error(err); console.log(err); } ;
                        next();
                    }else{
                        const error = new Error("Id Not Found");
                        error.statusCode = 404;
                        error.code = 'ID_NOT_FOUND'; 
                        next(error);
                    }
                }else{
                    const error = new Error("Id Not Found");
                    error.statusCode = 404;
                    error.code = 'ID_NOT_FOUND'; 
                    next(error);
                }
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Restfav.remoteMethod('permutationRank',{
        description: 'Permutation rank Fav Rest###', 
        accepts: [  
                    {arg: 'fav1', type: 'String', required: true},
                    {arg: 'fav2', type: 'String', required: true},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/permutationRank'} 
    });

    /** add rank */
    // Restfav.observe('before save', function addRank(ctx, next) {
    //     const data = ctx.instance ? ctx.instance : ctx.data;
    //     if(ctx.isNewInstance){ console.log('new instance');
    //         if(ctx && ctx.options && ctx.options.accessToken && ctx.options.accessToken.userId){ 
    //             var userId = ctx.options.accessToken.userId; console.log(userId)
    //             Restfav.find({where:{userId:userId}},(err,res)=>{
    //                 if(err){Logger.dbLogger.error(err); next(err);}
    //                 var newRank = Number(res.length) + 1; console.log(newRank); console.log(typeof newRank)
    //                 data.rank = newRank;
    //                 next();
    //             })
    //         }else{
    //             const error = new Error("Authorization Required");
    //             error.statusCode = 401;
    //             error.code = 'AUTHORIZATION_REQUIRED'; 
    //             next(error);
    //         }
    //     }else{
    //         next();
    //     }
    // });

};
