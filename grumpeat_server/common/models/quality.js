'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;

module.exports = function(Quality) {

    /** get list qualities & gategories */
    Quality.qualitiesCategories = (options, next) => {
        Quality.find({where:{status:1}}, async (err, qualities) => {
            if(err){Logger.dbLogger.error(err); next(err);}
            try{
                var Category = app.models.Category;
                var categories = await Category.find({where:{status:1}});
            }catch(err){Logger.dbLogger.error(err); next(err);}
            var result = {
                qualities: qualities, 
                categories: categories
            };
            next(null, result);
        });
    };

    Quality.remoteMethod('qualitiesCategories',{
        description: 'get list qualities & gategories ##',
        accepts: [
                {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/qualitiesCategories'}
    });

    

};
