'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const path = require('path');
const fs = require('fs');
const Handlebars = require('handlebars');
const config = require('../../server/config.json');
const _ = require('lodash');
const moment = require('moment');

module.exports = function(Order) {
  
    Order.observe('before save', function sendMailToRestaurant(ctx, next) {
        const data = ctx.instance ? ctx.instance : ctx.data;
        const Restaurant = app.models.Restaurant, Promotion = app.models.Promotion; 
        var date = String((new Date()).getTime());
        data.code = date.substr(date.length - 8); 
        if(data.promotionId){
            Promotion.findById(data.promotionId, async (err,promo) => {
                if(err){Logger.dbLogger.error(err); next(err);} 
                var resto =  await Restaurant.findById(promo.restaurantId);
                if(resto.email){
                    next();
                }else{
                    const error = new Error("Restaurant email not found");
                    error.statusCode = 404;
                    error.code = 'RESTAURANT_EMAIL_NOT_FOUND'; 
                    next(error);
                } 
            });
        }else{
            next();
        }
    });

    Order.observe('after save', function sendMailToRestaurant(ctx, next) {
        const data = ctx.instance ? ctx.instance : ctx.data;
        const Restaurant = app.models.Restaurant, Promotion = app.models.Promotion, Profil = app.models.Profil;
        const Email = app.models.Email; 
        var id = String(data.id);
        if(data && data.status === 1 ){
            // data.code = id.substr(id.length - 8);
            Promotion.findById(data.promotionId, async (err,promo) =>{
                if(err){Logger.dbLogger.error(err); next(err);}
                try{
                    var resto =  await Restaurant.findById(promo.restaurantId);
                } catch( err ) { Logger.dbLogger.error(err); next(err); } ;
                try{
                    var profil =  await Profil.findById(data.profilId);
                } catch( err ) { Logger.dbLogger.error(err); next(err); } ;
                const mailSubject = 'Booking - '+profil.firstName+' '+profil.lastName+' table for '+data.nbPersonne+' NB: '+data.code;
                let templatePath, htmlMailCodeValidation, mailTemplate, mailHtml;
                templatePath = path.join(__dirname, '../../server', 'templates', 'newOrdre.html'); 
                htmlMailCodeValidation = fs.readFileSync(templatePath, 'utf8');
                mailTemplate = Handlebars.compile(htmlMailCodeValidation);
                mailHtml = mailTemplate({ 
                    code: data.code, 
                    name: profil.firstName+' '+profil.lastName,
                    date: moment.utc(data.date).format('YYYY-MM-DD'),
                    time: moment.utc(data.timeStart).format('HH:mm') +' - '+ moment.utc(data.timeEnd).format('HH:mm'),
                    nb: data.nbPersonne,
                    tel: data.phone
                });
                Email.send({
                        to: resto.email,
                        from: config.mail.from,
                        subject: mailSubject,
                        html: mailHtml
                    }, function (err) {
                        if (err) {
                            console.log('> error sending code validation email');
                            Logger.dbLogger.error(err); next(err);
                        } else {
                            console.log('> sending code validation email to:', resto.email);
                            next();
                        }
                });
            });
        }else{
            next();
        }
    });

    /** Cancel ordre */
    Order.cancelOrdre = (orderId, options, next) =>{
        if(options && options.accessToken && options.accessToken.userId){
            Order.findById(orderId,{status:1}, (err,ord)=>{
                if(err){Logger.dbLogger.error(err); next(err);} 
                if(ord){
                    ord.updateAttributes({status:0}, async (err,order)=>{
                        if(err){Logger.dbLogger.error(err); next(err);} 
                        const Profil = app.models.Profil, Restaurant = app.models.Restaurant, Promotion =  app.models.Promotion;  
                        const Email = app.models.Email; 
                        
                        try{
                            var profil = await Profil.findById(String(order.profilId)); 
                        } catch( err ) { Logger.dbLogger.error(err); console.log(err); } ; 
                        try{ 
                            var promotion = await Promotion.findById(String(order.promotionId));  
                        } catch( err ) { Logger.dbLogger.error(err);  console.log(err); } ;
                        try{
                            var restaurant = await Restaurant.findById(String(promotion.restaurantId));
                        } catch( err ) { Logger.dbLogger.error(err); console.log(err); } ;
                        const mailSubject = 'Cancelation - '+profil.firstName+' '+profil.lastName+' table for '+order.nbPersonne+' NB: '+order.code;
                        let templatePath, htmlMailCodeValidation, mailTemplate, mailHtml;
                        templatePath = path.join(__dirname, '../../server', 'templates', 'cancelOrdre.html'); 
                        htmlMailCodeValidation = fs.readFileSync(templatePath, 'utf8');
                        mailTemplate = Handlebars.compile(htmlMailCodeValidation);
                        mailHtml = mailTemplate({ 
                            code: order.code, 
                            name: profil.firstName+' '+profil.lastName,
                            date: moment.utc(order.date).format('YYYY-MM-DD'),
                            time: moment.utc(order.timeStart).format('HH:mm') +' - '+ moment.utc(order.timeEnd).format('HH:mm'),
                            nb: order.nbPersonne,
                            tel: order.phone
                        });
                        
                        Email.send({
                                to: restaurant.email,
                                from: config.mail.from,
                                subject: mailSubject,
                                html: mailHtml
                            }, function (err) {
                                if (err) { 
                                    console.log('> error sending code validation email');
                                    Logger.dbLogger.error(err); next(err);
                                } else {
                                    console.log('> sending code validation email to:', resto.email);
                                    next();
                                }
                        });
                    });
                    next();
                }else{
                    const error = new Error("Order Not Found");
                    error.statusCode = 404;
                    error.code = 'ORDER_NOT_FOUND'; 
                    next(error);
                }
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Order.remoteMethod('cancelOrdre',{
        description: 'Cancel order ###', 
        accepts: [  
                    {arg: 'orderId', type: 'string', required: true},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/cancelOrdre'} 
    });

    /** reverse order get */
    Order.observe('access', function defaultOrder(ctx, next) {
        ctx.query.order = ['createdAt DESC'];
        next();
    });
};
