'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;

module.exports = function(Category) {

    /** define rank */
    Category.observe('before save',  function defineRank(ctx, next){
        if(ctx.isNewInstance){
            const data = ctx.instance ? ctx.instance : ctx.data;
            Category.count((err,compt) => {
                if(err){next(err);}
                data.rank = compt;
                next();
            });
        }else{
            next();
        }
    }); 
};
