'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;

module.exports = function(Suggestionordre) {

    /** get suggestion tries information 
     * @param string userId
     * @param object options
     * @return object
    */
    Suggestionordre.getNbSuggestion = (userId, options, next) => {
        const RestFav = app.models.RestFav;
        Suggestionordre.findOne({where:{profilId: userId}}, async (err,sug) => {
            if(err){Logger.dbLogger.error(err); next(err);}
            try{
                var fr = await RestFav.find({where:{userId:userId}});
            }catch(err){Logger.dbLogger.error(err); next(err);}
            if(fr.length < 6){  
                var nbSug = 3; 
            }else if((fr.length < 9) && (fr.length > 5)){
                var nbSug = 6;
            }else if(fr.length > 8){
                var nbSug = 9;
            }
            
            if(sug){ 
                var lastUpdateMid = new Date(sug.updatedAt.setHours(25, 0, 0, 0)); 
                var today = new Date();
                var tempsRestant = Math.trunc((lastUpdateMid - today) / 1000);
                if(sug.nbOrdre >= nbSug){ 
                    if(today > lastUpdateMid){
                        next(null,{current:0,total:nbSug});
                    }else{
                        next(null,{current:sug.nbOrdre,total:nbSug,remainingTime:tempsRestant});
                    }
                }else{
                    next(null,{current:sug.nbOrdre,total:nbSug});
                }
            }else{
                next(null,{current:0,total:nbSug});
            }
        });
    };

    Suggestionordre.remoteMethod('getNbSuggestion',{
        description: 'get suggestion tries information ###', 
        accepts: [  
                    {arg: 'userId', type: 'string', required: true},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/getNbSuggestion'} 
    });
};
