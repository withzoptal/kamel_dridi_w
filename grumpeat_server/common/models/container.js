'use strict';
const app = require('../../server/server');

module.exports = function(Container) {
    /** Renommer l'image' */
    app.dataSources.container.connector.getFilename = function (origFilename, req, res) {
        var origFilename = origFilename.name; 
        var parts = origFilename.split('.'),
        extension = parts[parts.length-1];
        var namePhoto = parts[parts.length-2];
        namePhoto = namePhoto.replace(/[ùûü]/g,"u").replace(/[îï]/g,"i").replace(/[àâä]/g,"a").replace(/[ôö]/g,"o").replace(/[éèêë]/g,"e").replace(/ç/g,"c");
        var newFilename = (new Date()).getTime()+'_'+'grumpeat_'+namePhoto+'.'+extension;
        return newFilename;
    };

};
