'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const _ = require('lodash');
var OneSignal = require('onesignal-node');
const config = require('../../server/config.json');

module.exports = function(Profil) {

    const myClient = new OneSignal.Client({
        userAuthKey: config.onesignal.apiKey,
        app: { appAuthKey: config.onesignal.apiKey, appId: config.onesignal.appId }
    });

    /** pagination */
    function Paginator (arr, page1, per_page){
        var page = page1+1;
        var deb = (page * per_page) - per_page;
        if(deb < 0){deb = 0;}
        var fin = (deb + per_page) ;
        // var tranche = arr.slice(deb,fin);
        return arr.slice(deb,fin);
    }

    /** Change email for user connected from fb */
    Profil.remoteMethod('ChangeEmailFB', {
        description: 'Product autoComplete Search',
        accepts: [
            {arg: 'email', type: 'string', required: true},
            {arg: 'options', type: 'object', http: 'optionsFromRequest'}
        ],
        returns: {type: 'array', root: true},
        http: {verb: 'post', path: '/ChangeEmailFB'}
    });

    Profil.ChangeEmailFB = (email, options, next) => {
        if(options.accessToken.userId){
            Profil.count({email: email},(error,result)=>{
                if (error) {Logger.dbLogger.error(error);return next(error);} 
                if (result !== 0) {
                    const err = new Error('Email already taken');
                    err.statusCode = 422;
                    err.code = 'EMAIL_ALREADY_TAKEN';
                    next(err);
                }  
                else {
                    Profil.findById(options.accessToken.userId, (error,p) =>{
                        if (error) {Logger.dbLogger.error(error);return next(error);} 
                        p.updateAttributes({email:email,validemail:true},(error,np)=>{
                            if (error) {Logger.dbLogger.error(error);return next(error);}
                            next(null,np);
                        })
                    })
                }
            })
        }else{
            const err = new Error('Token required');
            err.statusCode = 403;
            err.code = 'TOKEN_REQUIRED';
            next(err);
        }
    };

    /** Forcer la verification de l'email si ce n'est pas un client  */
    Profil.observe('after save', function validateEmail(ctx, next){
        if(ctx.isNewInstance){
            if(ctx.instance.role){
                Profil.findById(ctx.instance.id, (error, profil) => {  
                    if(error){Logger.dbLogger.error(error); return next(error);}
                    profil.updateAttributes({emailVerified:true});
                    next();
                });
            }else{
                next();
            }
        }else{
            next();
        }
    });

    /**  Assign User Role   */
    Profil.observe('after save', function setRoleMapping(ctx, next) {
        const Role = app.models.Role;
        const RoleMapping = app.models.RoleMapping;
        const isAdmin = ctx.options.isAdmin || false;
        if (ctx.instance) {
            if (ctx.isNewInstance) {
                let userRole = 'client';
                if (ctx.instance.role == 'superadmin') {
                    userRole = 'superadmin';
                }
                if (ctx.instance.role == 'admin') {
                    userRole = 'admin';
                }
                Role.findOne({'where': { 'name': userRole }}, function (error, role) {
                    if (error) { Logger.dbLogger.error(error); return next(error);} 
                    else {
                        role.principals.create({
                            principalType: RoleMapping.USER,
                            principalId: ctx.instance.id
                            }, function (err, roleMapping) {
                                Profil.findById(ctx.instance.id, function (error, profil) {
                                    if(error){Logger.dbLogger.error(error); return next(error);}
                                    profil.updateAttributes({role: userRole});
                                });
                                next();
                        });
                    }
                });
            } else {
                next();
            }
        } else {
            next();
        }
    });

    /** Get User Roles */
    Profil.getRoles = (userId, next) => {
        app.models.Role.getRoles({ 
                principalType: app.models.RoleMapping.USER,
                principalId: userId
            }, {returnOnlyRoleNames: true}, function (roleErr, roles) {
                if (roleErr) {
                    Logger.dbLogger.error(roleErr);
                    return next(roleErr);
                }
                roles = _.filter(roles, function (role) {
                    return typeof role === 'string';
                });
            const userRoles = {
                isSuper: _.includes(roles, 'superadmin'),
                isAdmin: _.includes(roles, 'admin'),
                isClient: _.includes(roles, 'client')
            };
            next(null, userRoles);
        });
    };

    /** tri par nombre d'ami en commun */
    function sortByFriend(a,b) { 
        var x = a.nbFriends;
        var y = b.nbFriends;
        return y - x;
    };

    /** get list users for follow */
    Profil.usersToFollow = (lim, offs, options, next) => {
        if(options && options.accessToken && options.accessToken.userId){
            var offset1 = offs || 0;
            var limit1 = lim || 5; 
            const Follow = app.models.Follow;  
            // ,limit: limit1,offset: offset1 * limit1,   
            Profil.find({
                where:{role:"client",id: {neq: options.accessToken.userId, statu : 1}}
               }, async (err,users)=>{
                if(err){Logger.dbLogger.error(err); next(err);}
                // list d'ami: current user
                try{ 
                    var followers = await Follow.find({where:{followerId: options.accessToken.userId,status:2},include:'user'});
                    var amiId = new Array();
                    for(var i = 0; i < followers.length;i++){
                        followers[i] = followers[i].toJSON(); 
                        amiId.push(String(followers[i].user.id));
                    }
                    var following = await Follow.find({where:{profilId: options.accessToken.userId,status:2},include:'follower'}); 
                    for(var i = 0; i < following.length;i++){
                        following[i] = following[i].toJSON();
                        if(amiId.indexOf(String(following[i].follower.id)) === -1){
                            amiId.push(String(following[i].follower.id));
                        }
                    }
                }catch(err){ Logger.dbLogger.error(err); next(err); }

                for(var i =0; i < users.length;i++){
                    var rel = await Follow.findOne({where:{profilId:users[i].id, followerId:options.accessToken.userId}}); 
                    if(rel){
                        if(rel.status === 2){
                            users[i].followed = 2;
                        }else{
                            users[i].followed = 1;
                        }
                    }else{
                        users[i].followed = 0;
                    }

                    // list d'ami : user from list
                    try{ 
                        var followers = await Follow.find({where:{followerId: users[i].id,status:2},include:'user'}); 
                        var amiId1 = new Array();
                        for(var a = 0; a < followers.length;a++){
                            followers[a] = followers[a].toJSON(); 
                            amiId1.push(String(followers[a].user.id));
                        }
                        var following = await Follow.find({where:{profilId: users[i].id,status:2},include:'follower'}); 
                        for(var b = 0; b < following.length;b++){
                            following[b] = following[b].toJSON();
                            if(amiId1.indexOf(String(following[b].follower.id)) === -1){
                                amiId1.push(String(following[b].follower.id)); 
                            }
                        }
                    }catch(err){ Logger.dbLogger.error(err); next(err); }
                    var arrr = amiId1.filter(value => -1 !== amiId.indexOf(value));
                    users[i].nbFriends = arrr.length;
                }
                users.sort(sortByFriend);
                var data = Paginator (users, offset1, limit1);
                next(null, data);
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Profil.remoteMethod('usersToFollow',{
        description: 'return users list to follow ###', 
        accepts: [  
                    { arg: 'limit', type: 'number', required: false},
                    { arg: 'offset', type: 'number', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/usersToFollow'} 
    });

    /** followed */
    Profil.observe('loaded', function followedFollow(ctx, next){ 
        const data = ctx.instance ? ctx.instance : ctx.data;
        if(ctx.options && ctx.options.accessToken && ctx.options.accessToken.userId){
            const Follow = app.models.Follow;
            // Followed : true or false    je le suivais
            Follow.findOne({where:{followerId: ctx.options.accessToken.userId, profilId: data.id}},(err,rel)=>{
                if(err){Logger.dbLogger.error(err); next(err);}
                if(rel){
                    if(rel.status === 2){
                        data.followed = 2;
                    }else{
                        data.followed = 1;
                    }
                    
                }else{
                    data.followed = 0;
                }
                // Following :true or false   il me suivait
                Follow.findOne({where:{profilId: ctx.options.accessToken.userId, followerId: data.id}},(err,rel)=>{ 
                    if(err){Logger.dbLogger.error(err); next(err);}
                    if(rel){
                        if(rel.status === 2){
                            data.follow = 2;
                        }else{
                            data.follow = 1;
                        }
                        next();
                    }else{
                        data.follow = 0; 
                        next();
                    }
                });
            });
        }else{ 
            next();
        }
    });

    /** return nb of fav rest */
    Profil.observe('loaded', function nbFav(ctx, next){ 
        const data = ctx.instance ? ctx.instance : ctx.data;
        if(data && data.id){
            const RestFav = app.models.RestFav;
            RestFav.find({where:{userId: String(data.id)}},(err,fav)=>{
                if(err){Logger.dbLogger.error(err); next(err);}
                data.favNb = fav.length; 
                next();
            });
        }else{ 
            next();
        }
    });

    
 
    /** search by firstName, lastName, email */
    Profil.search = function (q, limit, offset, options, next) {                
        var limit = limit || 10;
        var offset = offset || 0;
        const Follow = app.models.Follow;
        var qu = q;
        var words = qu.split(' ');
        var patFirst = new RegExp('.*'+words[0]+'.*', "i");
        var patLast = new RegExp('.*'+words[1]+'.*', "i"); 
        q = q.replace(/ /g, "");
        var pattern = new RegExp('.*'+q+'.*', "i");
        // ,
        //         limit: limit,
        //         offset: offset * limit,
        Profil.find({ 
                where: { 
                    or:[
                        {and:[{firstName: {like: patFirst}},{lastName: {like: patLast}}]},
                        {firstName: {like: pattern}},
                        {lastName: {like: pattern}},
                        {username: {like: pattern}},
                        {email: {like: pattern}}],
                    statu: 1,
                    role : 'client'
                }
            }, async function (err, result) {
                if (err) {Logger.dbLogger.error(err); next(err);} 
                // list d'ami: current user
                try{ 
                    var followers = await Follow.find({where:{followerId: options.accessToken.userId,status:2},include:'user'});
                    var amiId = new Array();
                    for(var i = 0; i < followers.length;i++){
                        followers[i] = followers[i].toJSON(); 
                        amiId.push(String(followers[i].user.id));
                    }
                    var following = await Follow.find({where:{profilId: options.accessToken.userId,status:2},include:'follower'}); 
                    for(var i = 0; i < following.length;i++){
                        following[i] = following[i].toJSON();
                        if(amiId.indexOf(String(following[i].follower.id)) === -1){
                            amiId.push(String(following[i].follower.id));
                        }
                    }
                }catch(err){ Logger.dbLogger.error(err); next(err); }

                for(var i = 0; i < result.length; i++){
                    // Followed : 0 1 2     ntaba3 fih
                    try{
                        var rel = await Follow.findOne({where:{followerId: options.accessToken.userId, profilId: result[i].id}});
                    }catch(err){ Logger.dbLogger.error(err); next(err); }
                    if(rel){
                        if(rel.status === 2){
                            result[i].followed = 2;
                        }else{
                            result[i].followed = 1;
                        }
                    }else{
                        result[i].followed = 0;
                    }
                    // Following : 0 1 2   ytaba3 fya
                    try{
                        rel = await Follow.findOne({where:{profilId: options.accessToken.userId, followerId: result[i].id}}); 
                    }catch(err){ Logger.dbLogger.error(err); next(err); }
                    if(rel){
                        if(rel.status === 2){
                            result[i].follow = 1;
                        }else{
                            result[i].follow = 2;
                        }
                    }else{
                        result[i].follow = 0;
                    }
                    // list d'ami : user from list
                    try{ 
                        var followers = await Follow.find({where:{followerId: result[i].id,status:2},include:'user'});
                        var amiId1 = new Array();
                        for(var a = 0; a < followers.length;a++){
                            followers[a] = followers[a].toJSON(); 
                            amiId1.push(String(followers[a].user.id));
                        }
                        var following = await Follow.find({where:{profilId: result[i].id,status:2},include:'follower'}); 
                        for(var b = 0; b < following.length;b++){
                            following[b] = following[b].toJSON();
                            if(amiId1.indexOf(String(following[b].follower.id)) === -1){
                                amiId1.push(String(following[b].follower.id)); 
                            }
                        }
                    }catch(err){ Logger.dbLogger.error(err); next(err); }
                    var arrr = amiId1.filter(value => -1 !== amiId.indexOf(value));
                    result[i].nbFriends = arrr.length;
                }
                result.sort(sortByFriend);
                var data = Paginator (result, offset, limit);
                next(null, data);
        });
    };

    Profil.remoteMethod('search', {
        description: 'Search by fisrtName or lastName or email ###',
        accepts: [
            {arg: 'q', type: 'string', required: true},
            {arg: 'limit', type: 'number', required: false},
            {arg: 'offset', type: 'number', required: false},
            {arg: 'options', type: 'object', http: 'optionsFromRequest'}
        ],
        returns: {type: 'array', root: true},
        http: {verb: 'get', path: '/search'}
    });

    /** on logout: Delete OneSignalId & RegistrationId & Desactive onesignal account */
    Profil.beforeRemote('logout', function (ctx, unused, next) { 
        const accessToken = ctx.args && ctx.args.access_token;
        if (accessToken) {
            app.models.AccessToken.findById(accessToken, (error, accessToken) => { 
                if (error) {next(error);} 
                if (accessToken) {
                    Profil.findById(accessToken.userId, (error, profil) => { 
                        if (error) {next(error);} 
                        if (profil) {
                            var deviceBody = {
                                notification_types: -2
                            };
                            myClient.editDevice(profil.oneSignalId, deviceBody, function (err, httpResponse, data) {
                                if(err){next(err)}
                                profil.updateAttributes({osType: '', oneSignalId: '', registrationId: ''});
                                next();
                            });
                        }
                    });
                }
                else{ 
                    next();
                }
            });
        }else{
            next();
        }
    });

    /** Active oneSignal on login  */
    // Profil.observe('after save', function upTags(ctx, next) { 
    //     const userData = ctx.instance ? ctx.instance : ctx.data;
    //     if(userData.oneSignalId){ 
    //         Profil.findById(userData.id, (err,p) => {
    //             if(err){Logger.dbLogger.error(err); return next(err);}
    //             var deviceBody = {
    //                 notification_types : 1
    //             };
    //             myClient.editDevice(p.oneSignalId, deviceBody, function (err, httpResponse, data) {
    //                 if(err){Logger.dbLogger.error(err); return next(err);}
    //                 next()
    //             });
    //         })
    //     }else{
    //         next()
    //     }
    // });
};
