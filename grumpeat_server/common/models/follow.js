'use strict';
const app = require('../../server/server');
const Logger = require('../../server/logger').default;
const config = require('../../server/config.json');
var OneSignal = require('onesignal-node');
module.exports = function(Follow) {
    var myClient = new OneSignal.Client({
        userAuthKey: config.onesignal.apiKey,
        app: { appAuthKey: config.onesignal.apiKey, appId: config.onesignal.appId }
    });

    /** fix doublure follow */
    Follow.fixDoublure = async (code, options) => {
        if(code === '1445'){
            const Profil = app.models.Profil;
            try{
                var list = await Profil.find();  console.log(list.length);
            }catch(err){Logger.dbLogger.error(err); throw err;}
            for(var i = 0; i < list.length; i++){
                try{
                    var listFollowing = await Follow.find({where:{profilId: String(list[i].id)}});
                    var listFollower = await Follow.find({where:{followerId: String(list[i].id)}});
                }catch(err){Logger.dbLogger.error(err); throw err;}
                for(var j = 0; j < listFollowing.length; j++){
                    var element = listFollowing[j];
                    for(var k = j; k < listFollowing.length; k++){
                        if( (String(element.id) !== String(listFollowing[k].id)) && (String(element.profilId) === String(listFollowing[k].profilId)) && (String(element.followerId) === String(listFollowing[k].followerId)) ){
                            try{ 
                                await Follow.destroyById(listFollowing[k].id);
                            }catch(err){Logger.dbLogger.error(err); throw err;} 
                        }
                    }
                }
                for(let j = 0; j < listFollower.length; j++){
                    let element = listFollower[j];
                    for(let k = j; k < listFollower.length; k++){
                        if( (String(element.id) !== String(listFollower[k].id)) && (String(element.profilId) === String(listFollower[k].profilId)) && (String(element.followerId) === String(listFollower[k].followerId)) ){ console.log('true');
                            try{
                                await Follow.destroyById(listFollower[k].id);
                            }catch(err){Logger.dbLogger.error(err); throw err;}
                        }
                    }
                }
            }
            return {};
        }else{
            const error = new Error("Error code");
            error.statusCode = 401;
            error.code = 'ERROR_CODE'; 
            throw error;
        }
    };

    Follow.remoteMethod('fixDoublure',{
        description: 'fix doublure follow ###', 
        accepts: [  
                    { arg: 'code', type: 'string', required: true},
                    { arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/fixDoublure'} 
    });

    /** unfollowing user (personne qui me suit) */
    Follow.unfollowing = (profilId, next)=>{
        Follow.destroyAll({followerId:profilId,  profilId:options.accessToken.userId},(err,res)=>{
            if(err){Logger.dbLogger.error(err); next(err);}
            next();
        });
    };

    Follow.remoteMethod('unfollowing',{
        description: 'unfollowing user (personne qui me suit) ###', 
        accepts: [  
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'delete', path: '/unfollowing'} 
    });


    /** Liste following qui je les suis*/
    function sortByUserFirstName(a,b) { 
        var x = a.user.firstName;
        var y = b.user.firstName;
        return x.localeCompare(y)
    };

    Follow.listFollowing = (userId, lim, off, options, next)=>{
        var limit = lim || 5;
        var offset = off || 0;
        Follow.find({where:{followerId:userId,status:2},include:"user",limit:limit,offset: offset * limit},async (err,fol)=>{
            if(err){Logger.dbLogger.error(err); next(err);} 
            for(var i =0;i < fol.length;i++){
                fol[i] = fol[i].toJSON(); 
                // Followed : 0 1 2     ntaba3 fih
                var rel = await Follow.findOne({where:{followerId: options.accessToken.userId, profilId: fol[i].profilId}});
                if(rel){  
                    if(rel.status === 1){
                        fol[i].user.followed = 1;
                    }else{
                        fol[i].user.followed = 2;
                    }
                }else{
                    fol[i].user.followed = 0;
                }
                // Following : 0 1 2   ytaba3 fya
                var rel = await Follow.findOne({where:{profilId: options.accessToken.userId, followerId: fol[i].profilId}});  
                if(rel){ 
                    if(rel.status === 1){
                        fol[i].user.follow = 1;
                    }else{
                        fol[i].user.follow = 2;
                    }
                }else{
                    fol[i].user.follow = 0;
                }
            }
            fol.sort(sortByUserFirstName);
            var nbt = await Follow.count({followerId:userId,status:2});
            var result = {
                list: fol,
                nb: nbt
            }
            if(userId !== options.accessToken.userId){ 
                const Profil = app.models.Profil;
                var p = await Profil.findById(userId); 
                if(p.privateAcc){ 
                    var f = await Follow.findOne({where:{profilId:userId,followerId:options.accessToken.userId}}); 
                    if(f){  
                        if(f.status === 2){  
                            next(null,result);
                        }else{  
                            fol = [];
                            result.list = fol;
                            next(null,result);
                        }
                    }else{  
                        next(null,result);
                    }
                }else{  
                    next(null,result);
                }
            }else{ 
                next(null,result);
            }
        });
    };

    Follow.remoteMethod('listFollowing',{
        description: 'following list (how userId follow) ###', 
        accepts: [  
                    { arg: 'userId', type: 'string', required: true},
                    { arg: 'limit', type: 'number', required: false},
                    { arg: 'offset', type: 'number', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/listFollowing'} 
    });

    /** Liste followers qui me suivent */
    function sortByfollowerFirstName(a,b) { 
        var x = a.follower.firstName;
        var y = b.follower.firstName;
        return x.localeCompare(y)
    };

    Follow.listFollowers = (userId, lim, off, options, next)=>{
        var limit = lim || 5;                                           
        var offset = off || 0; 
        Follow.find({where:{profilId:userId,status:2},include:"follower",limit:limit,offset: offset * limit}, async (err,fol)=>{
            if(err){Logger.dbLogger.error(err); next(err);}
            for(var i =0;i < fol.length;i++){ 
                fol[i] = fol[i].toJSON();
                // Followed : 0 1 2     ntaba3 fih
                var rel = await Follow.findOne({where:{followerId: options.accessToken.userId, profilId: fol[i].followerId}}); 
                if(rel){
                    if(rel.status == 1){
                        fol[i].follower.followed = 1;
                    }else{
                        fol[i].follower.followed = 2;
                    }
                }else{
                    fol[i].follower.followed = 0;
                }
                // Following : 0 1 2   ytaba3 fya
                var rel = await Follow.findOne({where:{profilId: options.accessToken.userId, followerId: fol[i].followerId}}); 
                if(rel){
                    if(rel.status == 1){
                        fol[i].follower.follow = 1;
                    }else{
                        fol[i].follower.follow = 2;
                    }
                }else{
                    fol[i].follower.follow = 0;
                }
            }
            fol.sort(sortByfollowerFirstName);
            
            const RestFav = app.models.RestFav;
            var nbt = await Follow.count({profilId:userId,status:2});
            var nbwing = await Follow.count({followerId:userId,status:2});
            var nbf = await RestFav.count({userId:userId});
            var result = {
                list: fol,
                nb: nbt,
                nbFollowing: nbwing,
                nbFav: nbf
            }
            if(userId !== options.accessToken.userId){ 
                const Profil = app.models.Profil;
                var p = await Profil.findById(userId);  
                if(p && p.privateAcc){  
                    var f = await Follow.findOne({where:{profilId:userId,followerId:options.accessToken.userId}}); 
                    if(f){  
                        if(f.status === 2){  
                            next(null,result);
                        }else{  
                            fol = [];
                            result.list = fol;
                            next(null,result);
                        }
                    }else{  
                        next(null,result);
                    }
                }else{  
                    next(null,result);
                }
            }else{ 
                next(null,result);
            }
        });
    };

    Follow.remoteMethod('listFollowers',{
        description: 'followers list (how follow userId) ###', 
        accepts: [  
                    { arg: 'userId', type: 'string', required: true},
                    { arg: 'limit', type: 'number', required: false},
                    { arg: 'offset', type: 'number', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/listFollowers'} 
    });

    /** current user follow other user */
    Follow.followUser = async (profilId, options, next) =>{
        if(options && options.accessToken && options.accessToken.userId){
            const Profil = app.models.Profil, Notification = app.models.Notification;
            var AllNotifByUserUnread;
            try{
                var pr = await Profil.findById(profilId); //, async (err,pr)=>{
                var current =  await Profil.findById(String(options.accessToken.userId));
            }catch(err){ Logger.dbLogger.error(err); throw err;}
            // if already exist relation 
            try{
                var exist = await Follow.findOne({where:{profilId:profilId, followerId:options.accessToken.userId}});
            }catch(err){ Logger.dbLogger.error(err); throw err;}
            if(exist){
                return {status:exist.status};
            }else{
            // si compte privé
                if(pr.privateAcc){
                    try{
                        await Follow.create({profilId:profilId, followerId:options.accessToken.userId, status: 1});
                        /** get unread notification */
                        AllNotifByUserUnread = await Notification.find({where:{"content.userId":pr.id, status:1, unread: true}});
                    }catch(err){Logger.dbLogger.error(err); throw err;}
                    // Create a notification to send
                    var msgEn = current.firstName+" "+current.lastName+" request to follow you !";
                    var msgFr = current.firstName+" "+current.lastName+" souhaite vous suivre !";
                    var firstNotification = new OneSignal.Notification({
                        contents: {
                            en: msgEn,
                            fr: msgFr
                        },  
                        include_player_ids:[pr.oneSignalId],
                        ios_badgeType: "Increase",
                        ios_badgeCount:  AllNotifByUserUnread.length + 1
                    });
                    // set target users
                    firstNotification.postBody["data"] = {"type": "requestFollow", "userId":options.accessToken.userId, "msgEn":msgEn, "msgFr":msgFr};
                    // send this notification to All Users except Inactive ones
                    try{
                        await myClient.sendNotification(firstNotification); //, function (err, httpResponse,data) { 
                    }catch(err) { Logger.dbLogger.error(err);throw err; }  
                    // creation d'une notification pour l'historique
                    var content = {
                        type : "requestFollow",
                        userId : options.accessToken.userId,
                        userRequested: pr.id,
                        subjectEn:msgEn,
                        subjectFr:msgFr
                    }
                    try{
                        await Notification.create({notificationType:"Follow",content:content});
                    }catch(err) { Logger.dbLogger.error(err);throw err; }  
                    return {status:1};
                }else{ 
                    try{
                        await Follow.create({profilId:profilId, followerId:String(options.accessToken.userId)});
                        AllNotifByUserUnread = await Notification.find({where:{"content.userId":pr.id, status:1, unread: true}});
                    }catch(err){Logger.dbLogger.error(err); throw err;}
                    /** Create a notification to send */
                    let msgEn = current.firstName+" "+current.lastName+" start to follow you !";
                    let msgFr = current.firstName+" "+current.lastName+" commence à vous suivre !";
                    let firstNotification = new OneSignal.Notification({
                        contents: {
                            en: msgEn,
                            fr: msgFr
                        },  
                        include_player_ids:[pr.oneSignalId],
                        ios_badgeType: "Increase",
                        ios_badgeCount:  AllNotifByUserUnread.length + 1                 
                    });
                    // set target users
                    firstNotification.postBody["data"] = {"type": "startFollow", "userId":options.accessToken.userId, "msgEn":msgEn, "msgFr":msgFr};
                    // send this notification to All Users except Inactive ones 
                    try{
                        await myClient.sendNotification(firstNotification); //, function (err, httpResponse,data) { 
                    }catch(err) { Logger.dbLogger.error(err); throw err; } // console.log("data",data)
                    // creation d'une notification pour l'historique
                    let content = {
                        type : "startFollow",
                        userId : pr.id,
                        userRequested: options.accessToken.userId,
                        subjectEn:msgEn,
                        subjectFr:msgFr
                    }; 
                    try{
                        await Notification.create({notificationType:"Follow",content:content});
                    }catch(err) { Logger.dbLogger.error(err); throw err; }
                    return {status:2};
                        // });
                }
                // });
            }
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            throw error;
        }
    };

    Follow.remoteMethod('followUser',{
        description: 'current user follow profilId ###', 
        accepts: [  
                    {arg: 'profilId', type: 'string', required: false}, 
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/followUser'} 
    });

    /** current user unfollow other user */
    Follow.unfollowUser = async (profilId, options) => { 
        if(options && options.accessToken && options.accessToken.userId){
            try{
                await Follow.destroyAll({profilId: options.accessToken.userId, followerId: profilId});
            }catch(err){Logger.dbLogger.error(err); throw err;}
            return {};
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            throw error;
        }
    };

    Follow.remoteMethod('unfollowUser',{
        description: 'current user unfollow profilId  ###', 
        accepts: [  
                    {arg: 'profilId', type: 'string', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'delete', path: '/unfollowUser'} 
    });

    /** Accept follow */
    Follow.acceptFollow = (userId, options, next) => {  
        if(options && options.accessToken && options.accessToken.userId){
            var currentUser = String(options.accessToken.userId); 
            Follow.findOne({where:{followerId: userId, profilId: currentUser}},async (err,fl)=>{ 
                if(err){Logger.dbLogger.error(err); next(err);}
                if(fl){
                    fl.updateAttributes({status:2});
                    const Profil = app.models.Profil, Notification = app.models.Notification;
                    // add notification
                    var receiver = await Profil.findById(userId);
                    var sender = await Profil.findById(options.accessToken.userId);
                    /** get unread notification */
                    try{
                        var AllNotifByUserUnread = await Notification.find({where:{"content.userId":receiver.id, status:1, unread: true}});
                    }catch(err){Logger.dbLogger.error(err); next(err);}
                    // Create a notification to send
                    var msgEn = sender.firstName+" "+sender.lastName+" accept your request !";
                    var msgFr = sender.firstName+" "+sender.lastName+" a accepte votre demande !";
                    var firstNotification = new OneSignal.Notification({
                        contents: {
                            en: msgEn,
                            fr: msgFr
                        },  
                        include_player_ids:[receiver.oneSignalId],
                        ios_badgeType: "Increase",
                        ios_badgeCount:  AllNotifByUserUnread.length + 1
                    });
                    // set target users
                    firstNotification.postBody["data"] = {"type": "acceptRequestFollow", "userId":sender.id, "msgEn":msgEn, "msgFr":msgFr};
                    // send this notification to All Users except Inactive ones
                    myClient.sendNotification(firstNotification, function (err, httpResponse,data) {
                        if (err) {  if(err){Logger.dbLogger.error(err);return next(err)}}
                        // creation d'une notification pour l'historique
                        var content = {
                            type : "acceptRequestFollow",
                            userId : receiver.id,
                            userRequested: options.accessToken.userId,
                            subjectEn:msgEn,
                            subjectFr:msgFr
                        }; 
                        Notification.create({notificationType:"Follow",content:content});
                        next(null,{status:1});
                    
                
                    });
                }else{
                    const error = new Error("Follow relation not found");
                    error.statusCode = 40;
                    error.code = 'FOLLOW_RELATION_NOT_FOUND'; 
                    next(error);
                }
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Follow.remoteMethod('acceptFollow',{
        description: 'current user accept follow  ###', 
        accepts: [  
                    {arg: 'userId', type: 'string', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'post', path: '/acceptFollow'} 
    });

    /** get following updates (interface notifications) */
    Follow.followingUpdate = (limit, offset, options, next)=>{
        if(options && options.accessToken && options.accessToken.userId){
            var limit = limit || 10;
            var offset = offset || 0;
            const RestFav = app.models.RestFav;
            Follow.find({where:{followerId: String(options.accessToken.userId)}}, async (err,fls)=>{
                if(err){Logger.dbLogger.error(err); next(err);}
                var following = [];
                for(var i = 0;i < fls.length;i++){
                    following.push(fls[i].profilId);
                }
                var res = await RestFav.find({where:{userId:{inq:following}},include:['restaurant','profil'],limit: limit, offset: offset * limit});
                next(null,res);
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Follow.remoteMethod('followingUpdate',{
        description: 'current user accept follow  ###', 
        accepts: [  
                    {arg: 'limit', type:'number', required: false},
                    {arg: 'offset', type:'number', required: false},
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/followingUpdate'} 
    });

    /** suggest friends */
    function uniqueString(array){ 
        var un = [];
        for(var t =0; t < array.length;t++){
            if(un.indexOf(String(array[t]))=== -1){ 
                un.push(String(array[t]))
            }
        }
        return un;
    }

    function uniqueObjectId(array){ 
        var un = [], tab = [];
        for(var t =0; t < array.length;t++){
            if(un.indexOf(String(array[t].id))=== -1){ 
                un.push(String(array[t].id))
                tab.push(array[t]);
            }
        }
        return tab;
    }

    Follow.suggesToFollow = (options, next) =>{
        if(options && options.accessToken && options.accessToken.userId){
            var userId = String(options.accessToken.userId);
            var friends = [];
            Follow.find({where:{followerId:userId,status:2},fields:'profilId'},async (err,followers) =>{
                if(err){Logger.dbLogger.error(err); next(err);}
                for(var i = 0;i < followers.length;i++){
                    friends.push(followers[i].profilId)
                }
                friends = uniqueString(friends);
                var frFriends = new Array();
                for(var i = 0; i < friends.length;i++){
                    var frFriendsls = await Follow.find({where:{followerId:friends[i]},include:'user'}); 
                    for(var j = 0; j < frFriendsls.length;j++){
                        var f = frFriendsls[j].toJSON();
                        frFriends.push(f.user);
                    }
                }
                var l = 20;
                var result = new Array();
                for(var k = 0; k < frFriends.length;k++){  
                    if(frFriends[k] && frFriends[k].id && (!(String(userId) === String(frFriends[k].id)))){
                        var ami = await Follow.find({where:{profilId:frFriends[k].id, followerId:userId}});
                        if(l > 0){
                            if((ami.length === 0)){
                                result.push(frFriends[k]);
                                l--;
                            }
                        }else{
                            break;
                        }
                    }
                } 
                result = uniqueObjectId(result);
                for(var x = 0;x < result.length;x++){
                    // result[x] = result[x].toJSON(); 
                    try{
                        var flw = await Follow.find({where:{profilId: result[x].id, status:2}}); 
                    }catch(err){Logger.dbLogger.error(err); next(err);}
                    result[x].nbFollowers = flw.length;
                }
                next(null,result);
            });
        }else{
            const error = new Error("Authorization Required");
            error.statusCode = 401;
            error.code = 'AUTHORIZATION_REQUIRED'; 
            next(error);
        }
    };

    Follow.remoteMethod('suggesToFollow',{
        description: 'Suggest profils to follow###', 
        accepts: [  
                    {arg: 'options', type: 'object', http: 'optionsFromRequest'}
                ],
        returns: { type: 'object', root:true},
        http: {verb: 'get', path: '/suggesToFollow'} 
    });
};
