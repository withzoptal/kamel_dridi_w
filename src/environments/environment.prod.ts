export const environment = {
  production: false,
  SERVER_URL:  "http://ec2-54-169-212-59.ap-southeast-1.compute.amazonaws.com/api/",//"http://127.0.0.1:5010/v1/",
  IMG_BASE_URL: "http://ec2-54-169-212-59.ap-southeast-1.compute.amazonaws.com/api/",//"http://127.0.0.1:5010",
  GOOGLE_API_KEY: "AIzaSyDe06nYV9PKC3LHUOhqNADqtO1lIlcoaC0",
  STATUS_CODE: {
    VALID: {
      CODE: 0,
      MESSAGE: 'SUCCESS'
    },
    SUCCESS: {
      CODE: 200,
      MESSAGE: 'SUCCESS'
    },
    FAILURE: {
      CODE: 201,
      MESSAGE: 'ERROR'
    },
    SESSION_EXPIRE: {
      CODE: 203,
      MESSAGE: 'SUCCESS'
    },
    DATA_NOT_FOUND: {
      CODE: 202,
      MESSAGE: 'SUCCESS'
    },
    INTERNEL_SERVER_ERROR: {
      CODE: 500,
      MESSAGE: 'ERROR'
    },
    API_NOT_FOUND: {
      CODE: 404,
      MESSAGE: 'ERROR'
    },
  }
};
