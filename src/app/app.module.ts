import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from "ngx-spinner";
//import { DataTablesModule } from 'angular-datatables';
import { AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { MatTabsModule } from '@angular/material';

//services
import { CanActivateRouteGuard } from "./configs/can-activate-route.guard"

import { NgxPaginationModule} from 'ngx-pagination';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NotifierModule } from "angular-notifier";
//import { CollapseModule, BsDropdownModule, AccordionModule, TabsModule } from "ngx-bootstrap";
import { SweetAlertService } from 'angular-sweetalert-service';
//services
import { Interceptor } from './configs/interceptor';
import { HeaderDirective } from './directives/header.directive';

//Modules
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { OwlModule } from 'ngx-owl-carousel';
import { NgbPaginationModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { ModalModule } from 'ng-bootstrap-modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './view/header/header.component';
import { FooterComponent } from './view/footer/footer.component';
import { HomeComponent } from './view/home/home.component';
import { AboutComponent } from './view/about/about.component';
import { ContactComponent } from './view/contact/contact.component';
import { PagenotfoundComponent } from './view/pagenotfound/pagenotfound.component';
import { LoginComponent } from './view/login/login.component';
import { TermsComponent } from './view/terms/terms.component';
import { PromotionComponent } from './view/promotion/promotion.component';
import { MyAccountComponent } from './view/my-account/my-account.component';
import { NeighbourhoodRestaurantsComponent } from './view/neighbourhood-restaurants/neighbourhood-restaurants.component';
import { PrivacyPolicyComponent } from './view/privacy-policy/privacy-policy.component';
import { UserDirectoryComponent } from './view/user-directory/user-directory.component';
import { ChangePasswordComponent } from './view/change-password/change-password.component';
import { FavoriteRestaurantsComponent } from './view/favorite-restaurants/favorite-restaurants.component';
import { MainSectionComponent } from './view/main-section/main-section.component';
import { SignupComponent } from './view/signup/signup.component';
import { SubSectionComponent } from './view/sub-section/sub-section.component';
import { DetailComponent } from './view/neighbourhood-restaurants/detail/detail.component';
import { ForgotPasswordComponent } from './view/forgot-password/forgot-password.component';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
  LinkedinLoginProvider,
} from "angular-6-social-login";
import { ConfirmComponent } from './view/confirm/confirm.component';
import { AddRestaurantComponent } from './view/add-restaurant/add-restaurant.component';
import { EditProfileComponent } from './view/edit-profile/edit-profile.component';
import { FollowComponent } from './view/follow/follow.component';
import { ViewProfileComponent } from './view/view-profile/view-profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider("1199279237104771")
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("Your-Google-Client-Id")
        },
        {
          id: LinkedinLoginProvider.PROVIDER_ID,
          provider: new LinkedinLoginProvider("1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com")
        },
      ]
  );
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    HeaderDirective,
    AboutComponent,
    ContactComponent,
    PagenotfoundComponent,
    LoginComponent,
    TermsComponent,
    PromotionComponent,
    MyAccountComponent,
    NeighbourhoodRestaurantsComponent,
    PrivacyPolicyComponent,
    UserDirectoryComponent,
    ChangePasswordComponent,
    FavoriteRestaurantsComponent,
    MainSectionComponent,
    SignupComponent,
    SubSectionComponent,
    DetailComponent,
    ForgotPasswordComponent,
    ConfirmComponent,
    AddRestaurantComponent,
    EditProfileComponent,
    FollowComponent,
    ViewProfileComponent,
  ],
  entryComponents:[
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    CKEditorModule,
    NotifierModule,
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    OwlModule,
    SocialLoginModule,
    NgbModule,
    AutoCompleteModule,
    DropDownListModule,
    BrowserAnimationsModule,
    MatTabsModule,
    
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    CanActivateRouteGuard,
    SweetAlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
