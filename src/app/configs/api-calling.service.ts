import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import { NotifierService } from 'angular-notifier';
import { FormGroup, AbstractControl, Validators, ValidationErrors, FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ApiCallingService {
  private readonly notifier: NotifierService;

  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService,
    private router: Router,
    private notifierService: NotifierService,
  ) {
    this.notifier = notifierService;
  }

  public setUserLoggedIn(token: string, data: any): boolean {
    localStorage.setItem('token', token);
    localStorage.setItem('userDetails', JSON.stringify(data));
    return true;
  }
  
  public setUserSessionLoggedIn(token: string, data: any): boolean {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('userDetails', JSON.stringify(data));
    return true;
  }

  public checkUserAuth(): boolean {
    return localStorage.token ? true : false;
  }

  public IMAGE_BASE_URL = environment.IMG_BASE_URL;
  public GOOGLE_API_KEY = environment.GOOGLE_API_KEY;

  public postRequest(apiPath: string, postData: any): any { console.log("+++++++++postData+++++++", postData)
    var promise = new Promise((resolve, reject) => {
      this.spinner.show();
      this.http.post(environment.SERVER_URL + apiPath, postData)
        .subscribe(

          (val) => {  console.log("response val", val)

            this.spinner.hide();
            resolve(handleErrors(val, this.notifier))

          },
          response => {
            console.log("response response", response.status)
         
            this.spinner.hide();
            // console.log("POST call in error", response);
            // this.notifier.notify('error', ' WHOOPS , ' + 'Your session expired !!.. LOGIN AGAIN');

            reject(handleErrors(response, this.notifier));

          },
          () => {
            // console.log("The POST observable is now completed.");
          });
    });

    return promise;
  }

  public getRequest(apiPath: string, getData: any): any {

    var promise = new Promise((resolve, reject) => {
      this.spinner.show();

      this.http.get(environment.SERVER_URL + apiPath, getData)
        .subscribe(

          (val) => {

            this.spinner.hide();
            // resolve(handleError(val))
            if (val['code'] == environment.STATUS_CODE.DATA_NOT_FOUND.CODE) {
              resolve(val);
            } else {
              resolve(handleErrors(val, this.notifier))
            }
          },
          response => { 
            this.spinner.hide();

            // console.log("GET call in error", response);
            reject(handleErrors(response, this.notifier));

          },
          () => {
            // console.log("The GET observable is now completed.");
          });
    });

    return promise;
  }
  
  public deleteRequest(apiPath: string, getData: any): any {

    var promise = new Promise((resolve, reject) => {
      this.spinner.show();
      this.http.delete(environment.SERVER_URL + apiPath, getData)
        .subscribe(

          (val) => {
            this.spinner.hide();
            // resolve(handleError(val))
            if (val['code'] == environment.STATUS_CODE.DATA_NOT_FOUND.CODE) {
              resolve(val);
            } else {
              resolve(handleErrors(val, this.notifier))
            }
          },
          response => { 
            this.spinner.hide();
            // console.log("GET call in error", response);
            reject(handleErrors(response, this.notifier));
          },
          () => {
            // console.log("The GET observable is now completed.");
          });
    });

    return promise;
  }

  public patchRequest(apiPath: string, getData: any): any {

    var promise = new Promise((resolve, reject) => {
      this.spinner.show();
      this.http.patch(environment.SERVER_URL + apiPath, getData)
        .subscribe(

          (val) => {
            this.spinner.hide();
            // resolve(handleError(val))
            if (val['code'] == environment.STATUS_CODE.DATA_NOT_FOUND.CODE) {
              resolve(val);
            } else {
              resolve(handleErrors(val, this.notifier))
            }
          },
          response => { 
            this.spinner.hide();
            // console.log("GET call in error", response);
            reject(handleErrors(response, this.notifier));
          },
          () => {
            // console.log("The GET observable is now completed.");
          });
    });

    return promise;
  }

  


  cannotContainSpace(control: AbstractControl): ValidationErrors | null {

    if ((control.value as string).indexOf(' ') >= 0) {

      return { cannotContainSpace: true }

    } return null;

  }

  onActivateScroll(){
      let scrollToTop = window.setInterval(() => {
        let pos = window.pageYOffset;
        if (pos > 0) {
            window.scrollTo(0, pos - 10); // how far to scroll on each step
        } else {
            window.clearInterval(scrollToTop);
        }
    }, 16);
  }

}

function handleErrors(response, notifiers) {  
   var customErrorMessage;
   if(response && response.error && response.error.error && 
      response.error.error.details && Object.keys(response.error.error.details.messages).length > 0){
      if(
        response.error.error.details.messages 
        && response.error.error.details.messages.email 
        && response.error.error.details.messages.email.indexOf('[') >=-1){

          customErrorMessage = response.error.error.details.messages.email;
      
      }else if(
        response.error.error.details.messages && 
        response.error.error.details.messages.username && 
        response.error.error.details.messages.username.indexOf('[') >= -1){

          customErrorMessage = response.error.error.details.messages.username;
      }
        customErrorMessage = customErrorMessage.toString().replace(/"/g, '');
        customErrorMessage = customErrorMessage.toString().replace('[', '');
        customErrorMessage = customErrorMessage.toString().replace(']', '');
 
        notifiers.notify('error', `Error : ${customErrorMessage}`);
        console.log("+++++ 1 +++++++")
    }else if(response && response.error){ 
    
       if(response.status ==  401 ){
        
        notifiers.notify('error', `Unauthorized : ${'Please Enter correct email/password'}`);

       }else{
          if(response.error.error && response.error.error.message){
            notifiers.notify('error', `Error : ${response.error.error.message}`);
          }else{
            
            notifiers.notify('error', `Error : ${'Something went wrong please try again'}`);

          }
       }

    }else if(response == null || response==''){
     
     
        return true;
     
    }else{
   
        return response;
     

    }
}



function handleError(response, notifiers) {
  if (response['code'] == environment.STATUS_CODE.FAILURE.CODE) {
    // window.alert(`Error : ${response['message']}`);
    notifiers.notify('error', `Error : ${response['message']}`);;
  }
  else if (response['code'] == environment.STATUS_CODE.SESSION_EXPIRE.CODE) {
    notifiers.notify('error', `Error : ${response['message']}`);
    window.location.href = window.location.origin + '/#/login'
    // window.location.hash = "";

  }
  else if (response['code'] == environment.STATUS_CODE.SUCCESS.CODE) {
    return response;
  }
  else if (response['code'] == environment.STATUS_CODE.API_NOT_FOUND.CODE) {
    notifiers.notify('error', `Error: ${response.status} NOT FOUND`);
  }
  else if (response['code'] == environment.STATUS_CODE.DATA_NOT_FOUND.CODE) {
    // window.alert(`Error: ${response.status} NOT FOUND`);

  }
  else {
    notifiers.notify('error', `Error: Technical Error!! Please try again after sometime.`);
    // window.alert(`Error: Technical Error!! Please try again after sometime.`);

  }
  // console.log("POST call successful value returned in body",
  //   response);

}
