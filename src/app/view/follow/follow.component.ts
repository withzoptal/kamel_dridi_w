import { Component, OnInit, AfterViewInit  } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiCallingService } from "../../configs/api-calling.service"
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import Swal from 'sweetalert2/dist/sweetalert2.js'


@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.css']
})
export class FollowComponent implements OnInit {
  private readonly notifier: NotifierService;
  public userDirectory : any = [];
  public IMAGE_BASE_URL;
  public dictionary: any = [];

  // pagination var
  itemsPerPage: number = 20;
  totalRecords: number;
  skip: number = 0;
  p: number = 1;
  private stage  = '';
  total ='';



  constructor(
    private http: HttpClient,
    private router: Router,
    public api: ApiCallingService,
    notifierService: NotifierService,
    ) { this.notifier = notifierService; }


    ngOnInit() {

      this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;
      this.api.onActivateScroll();
     
    }
    searchUsers(){
     
      var api_url = 'Profils/searchUsersToFollow?access_token='+localStorage.getItem('token')+'&query='+$("#search-bar").val()

    console.log("++++api_url++", api_url)
      this.api.getRequest(api_url, {}).then( 
        (res) => { 
          console.log("+++++++++sasa++++++++", res)
          
            if(res == undefined){
              this.userDirectory =[];
            }else{
              this.userDirectory = res;
            }
        },
        (err) => {
        }
      );
  
  
    }
    follow(profileId){

      var api_url ="Follows/followUser?access_token="+localStorage.getItem('token')
      this.api.postRequest(api_url, {profilId: profileId }).then( 
        (res) => {
            Swal.fire(
              'follow!',
              'You have successfully followed.',
              'success'
            )
            this.searchUsers();
         
        },
        (err) => {
        }
      );
         
    }
    unfollow(profileId){
      Swal.fire({
        title: 'Are you sure?',
        text: 'You want to unfollow!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, unfollow it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {
          var api_url ="Follows/unfollowUser?access_token="+localStorage.getItem('token')+'&profilId='+profileId
          this.api.deleteRequest(api_url, {profilId: profileId }).then( 
            (res) => {
               Swal.fire(
                  'Unfollow!',
                  'You have successfully unfollowed.',
                  'success'
                )
              this.searchUsers();
            },
            (err) => {
            }
          );
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            'User is still in following list :)',
            'error'
          )
        }
      })
    

  }
}
