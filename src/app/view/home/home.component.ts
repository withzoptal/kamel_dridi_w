import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { OwlModule } from 'ngx-owl-carousel';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import { NotifierService } from "angular-notifier";
import * as $ from 'jquery';
import { ApiCallingService } from "../../configs/api-calling.service"
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js'
declare let google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  disableSwitching: boolean;
  @ViewChild('tabset', {read: ElementRef}) tabsetEl: ElementRef;
  closeResult: string;
  private readonly notifier: NotifierService;
  public restaurants: any = [];
  public userDetail: any = {};
  public limit =10;
  public IMAGE_BASE_URL;
  public result;
  public locCountryName = "";
  public SeeAll = false;
  show: boolean = true;
 
  constructor(
    private modalService: NgbModal, 
    notifierService: NotifierService,
    public api: ApiCallingService,
    private router: Router,
    ) {  
      this.notifier = notifierService;
      this.userDetail = JSON.parse(localStorage.getItem('userDetails'));
    }
  
  ngOnInit() {

    this.showPosition();

    if(localStorage.getItem('token')){
     this.getRestaturant();
    }else{
      //this.openLogin();
      this.SeeAll = true;

    }
    this.IMAGE_BASE_URL = this.api.IMAGE_BASE_URL;

  }
  openLogin() {
    this.modalService.open(LoginComponent);
  }

  showPosition(){
    localStorage.setItem('latlang', '22.2783076,114.1832507');
    localStorage.setItem('localcodePays', 'HK');
    localStorage.setItem('localcountry', 'Hong Kong');
    $('#country').html("Hong Kong");
      // if(navigator.geolocation) {
      //   navigator.geolocation.getCurrentPosition(this.successCallback, this.errorCallback);
      // } else {
      //   console.log("Sorry, your browser does not support HTML5 geolocation.");
      // }

  }
  successCallback(position) {  

    localStorage.setItem('latlang', position.coords.latitude+','+position.coords.longitude);
    let geocoder = new google.maps.Geocoder();
    let latlang = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    let request = {
      latLng: latlang
    }; 

     if (geocoder) {
          geocoder.geocode( request, function (results, status) { 
            if (status == google.maps.GeocoderStatus.OK) {
              this.locCountryName = results[ results.length - 1].formatted_address;
              localStorage.setItem('localcodePays', results[ results.length - 1].address_components[0].short_name);
              localStorage.setItem('localcountry', this.locCountryName);
              $('#country').html(this.locCountryName);
            }
            else {
            console.log("Geocoding failed: " + status);
            }
        }); 
      } 

  }
  errorCallback(error) {
    if(error.code == 1) {
      console.log("You've decided not to share your position, but it's OK. We won't ask you again.");
    } else if(error.code == 2) {
      console.log("The network is down or the positioning service can't be reached.");
    } else if(error.code == 3) {
      console.log("The attempt timed out before it could get the location data.");
    } else {
      console.log("Geolocation failed due to unknown error.");
    }
}

  getRestaturant(){

    var api_url = 'Restaurants/search?&access_token='+
      localStorage.getItem('token')+'&query='+
      localStorage.getItem('localcountry')+'&location='+
      localStorage.getItem('latlang')+'&limit='+
      this.limit+'&codePays='+
      localStorage.getItem('localcodePays');
    
    this.api.getRequest(api_url, {}).then( 
      (res) => {
          for(var i in res){
            if(res[i].rating){
              const ratings = res[i].rating;
              const starTotal = 5;
              const starPercentage = (ratings / starTotal) * 100;
              res[i].starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
            }else{
              res[i].starPercentageRounded =  '0%';
            }
            if(res[i].reviews){
              res[i].reviewcount = res[i].reviews.length

            }else{
              res[i].reviewcount =  '0';
            }
            if(res[i].photo){
              res[i].picToShow = this.IMAGE_BASE_URL+"/Containers/grumpeat/download/"+res[i].photo;
            }else{
              res[i].picToShow= '../../../assets/img/no-photo.png';
            }
            this.restaurants.push(res[i]);
            this.restaurants = this.shuffle(this.restaurants);
          }
      },
      (err) => {
      }
    );
  }
  shuffle(list) {
    return list.reduce((p, n) => {
      const size = p.length;
      const index = Math.trunc(Math.random() * (size - 1));
      p.splice(index, 0, n);
      return p;
    }, []);
  };
  
}
